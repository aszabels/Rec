/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Functors/FunctorDesc.h"
#include "Functors/IFactory.h"
#include "GaudiAlg/FilterPredicate.h"

namespace {
  using FilterPredicate = Gaudi::Functional::FilterPredicate<bool()>;
} // namespace

/** @class VoidFilter VoidFilter.cpp
 */
struct VoidFilter final : public FilterPredicate {
  // This the type-erased wrapper around the functor we use to make the decision
  using Predicate = Functors::Functor<bool()>;

  using FilterPredicate::FilterPredicate;

  bool operator()() const override {
    auto result = this->m_pred();
    m_cutEff += result;
    return result;
  }

  StatusCode initialize() override {
    auto sc = FilterPredicate::initialize();
    decode();
    return sc;
  }

private:
  // Counter for recording cut retention statistics
  mutable Gaudi::Accumulators::BinomialCounter<> m_cutEff{this, "Cut selection efficiency"};

  // This is the functor cut string
  Gaudi::Property<ThOr::FunctorDesc> m_Cut{
      this, "Cut", {"::Functors::AcceptAll{}", {"Functors/Function.h"}, "ALL"}, [this]( auto& ) {
        if ( this->FSMState() < Gaudi::StateMachine::INITIALIZED ) return;
        this->decode();
      }};

  // This is the type-erased type (roughly std::function) that contains the just-in-time compiled functor
  // I think this has some overhead, but it may be avoidable...TBC
  Predicate m_pred;

  // This is the factory tool that handles the functor cache and/or the just-in-time compilation
  ServiceHandle<Functors::IFactory> m_factory{this, "Factory", "FunctorFactory"};

  void decode() {
    m_factory.retrieve().ignore();
    m_pred = m_factory->get<Predicate>( this, m_Cut );
  }
};

DECLARE_COMPONENT_WITH_ID( VoidFilter, "VoidFilter" )
