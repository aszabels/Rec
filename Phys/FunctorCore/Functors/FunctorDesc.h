/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include <iomanip>
#include <iostream>
#include <string>
#include <vector>

#include "Gaudi/Parsers/Grammars.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/SerializeSTL.h"
#include "GaudiKernel/StatusCode.h"
#include <Gaudi/Parsers/Factory.h>

namespace ThOr {
  using parsing_type = std::tuple<std::string, std::vector<std::string>, std::string>;

  struct FunctorDesc {
    std::string              code;
    std::string              repr;
    std::vector<std::string> headers{};
    FunctorDesc() : code(), repr(), headers() {} // need for grammar
    FunctorDesc( parsing_type const& t )
        : code( std::get<0>( t ) ), repr( std::get<2>( t ) ), headers( std::get<1>( t ) ) {} // need for grammar
    FunctorDesc( std::string const& s, std::vector<std::string> const& vs, std::string const& repr = "" )
        : code( s ), repr( repr ), headers( vs ) {}
    ~FunctorDesc() = default;

    FunctorDesc( FunctorDesc const& ) = default;
    FunctorDesc& operator=( const FunctorDesc& ) = default;

    FunctorDesc( FunctorDesc&& ) = default;

    friend std::ostream& operator<<( std::ostream& o, FunctorDesc const& f ) {
      return GaudiUtils::details::ostream_joiner( o << "\"(" << std::quoted( f.code, '\'' ) << ", "
                                                    << "['",
                                                  f.headers, "', '" )
             << "']"
             << ", " << std::quoted( f.repr, '\'' ) << ")\"";
    }

    friend StatusCode parse_( FunctorDesc& f, std::string const& s ) {
      parsing_type input{};
      auto         sc = Gaudi::Parsers::parse_( input, s );
      if ( not sc ) throw GaudiException( "could not parse functor", "", StatusCode::FAILURE );
      std::tie( f.code, f.headers, f.repr ) = std::move( input );
      return StatusCode::SUCCESS;
    }
  };
} // namespace ThOr

namespace Gaudi::Parsers {
  template <typename Iterator, typename Skipper>
  struct Grammar_<Iterator, ThOr::FunctorDesc, Skipper> {
    typedef TupleGrammar<Iterator, ThOr::parsing_type, 2, Skipper> Grammar;
  };
} // namespace Gaudi::Parsers
