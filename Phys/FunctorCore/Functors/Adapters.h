/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Functors/Function.h"
#include "Functors/Utilities.h"
#include "SelKernel/TrackCombination.h"

/** @file  Adapters.h
 *  @brief Functors that adapt object-wise functors (e.g. PT) into
 *         collection-wise functors (e.g. scalar-sum-of-PT in CombinationCut)
 */
namespace Functors::Adapters {
  template <typename F>
  struct Accumulate : public Function<Accumulate<F>> {
    static_assert( std::is_base_of_v<FunctionBase, F>, "Functors::Adapters::Accumulate must wrap a functor" );
    F m_f;

    Accumulate( F f ) : m_f( std::move( f ) ) {}

    /* Improve error messages. */
    constexpr auto name() const { return "Accumulate( " + detail::get_name( m_f ) + " )"; }

    template <typename Algorithm>
    void bind( Algorithm* alg ) {
      detail::bind( m_f, alg );
    }

    auto prepare() const {
      return [f = detail::prepare( m_f )]( auto const& collection_of_items ) {
        using std::begin;
        using std::end;
        // We need to cope if the container is full of std::reference_wrapper
        // The std::cref( item ).get() construction is ugly, but ends up with T const&
        // regardless of whether item was itself T const& or std::reference_wrapper<T const>
        using item_t = decltype( std::cref( *begin( collection_of_items ) ).get() );
        // Figure out the type of the return value of the functor we are wrapping
        using f_ret_t = std::invoke_result_t<decltype( f ), item_t>;
        // This isn't very elegant, but if f_ret_t is bool, i.e. we are doing
        // something like SUM(ETA > 3), then the type of the sum (int) is not
        // the same as f_ret_t. Deduce the type of the sum...
        using ret_sum_t = std::invoke_result_t<std::plus<>, f_ret_t, f_ret_t>;
        return std::accumulate( begin( collection_of_items ), end( collection_of_items ), ret_sum_t( 0 ),
                                [f]( auto sum, item_t item ) { return sum + f( item ); } );
      };
    }
  };

  template <typename F>
  struct Minimum : public Function<Minimum<F>> {
    static_assert( std::is_base_of_v<FunctionBase, F>, "Functors::Adapters::Minimum must wrap a functor" );
    F m_f;

    Minimum( F f ) : m_f( std::move( f ) ) {}

    /* Improve error messages. */
    constexpr auto name() const { return "Minimum( " + detail::get_name( m_f ) + " )"; }

    template <typename Algorithm>
    void bind( Algorithm* alg ) {
      detail::bind( m_f, alg );
    }

    auto prepare() const {
      return [f = detail::prepare( m_f )]( auto const& collection_of_items ) {
        using std::begin;
        using std::end;
        using std::min;
        // We need to cope if the container is full of std::reference_wrapper
        // The std::cref( item ).get() construction is ugly, but ends up with T const&
        // regardless of whether item was itself T const& or std::reference_wrapper<T const>
        using item_t = decltype( std::cref( *begin( collection_of_items ) ).get() );
        // Figure out the type of the return value of the functor we are wrapping
        using f_ret_t = decltype( f( std::declval<item_t>() ) );
        return std::accumulate( begin( collection_of_items ), end( collection_of_items ),
                                std::numeric_limits<f_ret_t>::max(),
                                [f]( auto min_so_far, item_t item ) { return min( min_so_far, f( item ) ); } );
      };
    }
  };

  template <typename F>
  struct Maximum : public Function<Maximum<F>> {
    static_assert( std::is_base_of_v<FunctionBase, F>, "Functors::Adapters::Maximum must wrap a functor" );
    F m_f;

    Maximum( F f ) : m_f( std::move( f ) ) {}

    /* Improve error messages. */
    constexpr auto name() const { return "Maximum( " + detail::get_name( m_f ) + " )"; }

    template <typename Algorithm>
    void bind( Algorithm* alg ) {
      detail::bind( m_f, alg );
    }

    auto prepare() const {
      return [f = detail::prepare( m_f )]( auto const& collection_of_items ) {
        using std::begin;
        using std::end;
        using std::max;
        // We need to cope if the container is full of std::reference_wrapper
        // The std::cref( item ).get() construction is ugly, but ends up with T const&
        // regardless of whether item was itself T const& or std::reference_wrapper<T const>
        using item_t = decltype( std::cref( *begin( collection_of_items ) ).get() );
        // Figure out the type of the return value of the functor we are wrapping
        using f_ret_t = decltype( f( std::declval<item_t>() ) );
        return std::accumulate( begin( collection_of_items ), end( collection_of_items ),
                                std::numeric_limits<f_ret_t>::min(),
                                [f]( auto max_so_far, item_t item ) { return max( max_so_far, f( item ) ); } );
      };
    }
  };
} // namespace Functors::Adapters

namespace Functors::detail {
  /** Helper to check all members of a parameter pack are the same type. */
  template <typename T, typename... Ts>
  inline constexpr bool same_type_v = std::conjunction_v<std::is_same<T, Ts>...>;

  // TODO implement this without recursion, or give up on this way of trying to
  //      do things...
  template <typename ret_t, typename child_rel_t>
  ret_t follow_relation( child_rel_t const& child_rel ) {
    std::ostringstream oss;
    oss << "Did not find a container with family ID " << child_rel.zipIdentifier();
    throw GaudiException{oss.str(), "Functors::detail::follow_relation", StatusCode::FAILURE};
  }

  template <typename ret_t, typename child_rel_t, typename first_child_container_t, typename... child_containers_t>
  ret_t follow_relation( child_rel_t const& child_rel, first_child_container_t const& child_container,
                         child_containers_t const&... other_containers ) {
    if ( child_container.zipIdentifier() == child_rel.zipIdentifier() ) {
      return child_container[child_rel.index()];
    } else {
      return follow_relation<ret_t>( child_rel, other_containers... );
    }
  }

  template <typename combination_t, typename child_rels_t, typename... child_containers_t, std::size_t... I>
  combination_t make_combination( std::index_sequence<I...>, child_rels_t const& child_rels,
                                  child_containers_t const&... child_containers ) {
    using ret_t = typename combination_t::value_type; // Proxy or reference_wrapper<Track>
    return {{follow_relation<ret_t>( std::get<I>( child_rels ), child_containers... )...}};
  }

  template <typename F, typename... ChildContainers>
  struct CombinationFromComposite {
    CombinationFromComposite( F f ) : m_f{std::move( f )} {}

    /* Improve error messages. */
    constexpr auto name() const { return "CombinationFromComposite( " + get_name( m_f ) + " )"; }

    template <typename Algorithm>
    void bind( Algorithm* alg ) {
      // Set up the dependencies of the functor we explicitly own
      detail::bind( m_f, alg );
    }

    auto prepare( /* event_context */ ) const {
      // The DataDepWrapper we are owned by injects the data dependencies that
      // ChildContainers contains the types of as the first arguments when
      // calling the lambda that we return here.
      return [f = detail::prepare( m_f )]( ChildContainers const&... child_containers, auto const& input ) {
        // This adapter doesn't really make sense with input a variadic
        // parameter pack, so we hardcode that it's a single item.

        // First, get the child relations
        auto const& child_rels = input.child_relations();

        // TODO: figure out what to do when the types are not all the same...
        //       this seems a bit awkward, but it's not only a problem here, we
        //       would have to solve it in the combiner before it's relevant in
        //       this adapter. for now, just assume that there is a common type
        static_assert( same_type_v<typename ChildContainers::value_type...> );

        // Extract the type of the values in the various child_containers...we
        // just checked that these are all the same, so just take the first one
        using child_t = typename std::tuple_element_t<0, std::tuple<ChildContainers...>>::value_type;

        // Figure out the type of the combination object that we want to fill
        // TODO: needs to be more sophisticated if child_rels.size() is not a
        //       compile-time constant
        using child_rels_t   = typename std::decay_t<decltype( child_rels )>;
        using num_children_t = typename std::tuple_size<child_rels_t>;
        using combination_t  = TrackCombination<child_t, num_children_t::value>;

        // Create the combination object
        // Use the containers contained in `child_containers` to find the
        // objects referred to in child_rels, then construct a new combination
        // object using those objects
        auto combination = make_combination<combination_t>( std::make_index_sequence<num_children_t::value>{},
                                                            child_rels, child_containers... );

        // Finally, pass this combination object to our prepared functor
        return f( combination );
      };
    }

  private:
    F m_f;
  };

  /** Helper to go from some functor type to the template (Function, Predicate)
   *  that it derives from. This is useful if we want to make an 'adapted'
   *  functor where the adapter supports predicate operations iff the functor
   *  it wraps/adapts supports those operations.
   *
   *  @todo Figure out the shorthand syntax so e.g. get_base_t<F> 'returns'
   *        Predicate if F is a predicate and Function otherwise. For the
   *        moment we have to write:
   *          get_base_type<is_functor_predicate_v<F>>::template type
   *        at the 'call site'. This is complicated because Function and
   *        Predicate are not plain types, they themselves are templates.
   */
  template <bool B>
  struct get_base_type {
    template <typename A>
    using type = Predicate<A>;
  };

  template <>
  struct get_base_type<false> {
    template <typename A>
    using type = Function<A>;
  };
} // namespace Functors::detail

namespace Functors::Adapters {
  /** Adapter to re-assemble the combination of children that a composite is
   *  formed from. Using the language of 'CombineParticles', that means that
   *  applying ADAPTER(COMBINATION_CUT, ...) as a 'MotherCut', where
   *  COMBINATION_CUT would have been a valid 'CombinationCut'. Because the
   *  composite particle only knows a (family, index) pair about its own
   *  children, the '...' must include a input container for each family. This
   *  also means that you will need to tell the functor the explicit type of
   *  this input container.
   *
   *  The helper function allows us to avoid specifying any more types than is
   *  strictly neccessary.
   */
  template <typename... ChildContainers, typename F, typename... CArgs>
  auto CombinationFromComposite( F f, CArgs&&... args ) {
    // 'F'    is the wrapped functor (COMBINATION_CUT)
    // 'args' is the list of data locations where child data can be found
    // The composite itself will be passed as the argument to the adapter's
    // function call operator.
    return detail::add_data_deps<detail::get_base_type<detail::is_functor_predicate_v<F>>::template type,
                                 ChildContainers...>(
        detail::CombinationFromComposite<F, ChildContainers...>{std::move( f )}, std::forward<CArgs>( args )... );
  }
} // namespace Functors::Adapters
