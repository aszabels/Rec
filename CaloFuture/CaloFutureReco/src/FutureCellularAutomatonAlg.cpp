/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Gaudi/Algorithm.h"
#include "GaudiAlg/FixTESPath.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/Counters.h"
#include "GaudiKernel/SystemOfUnits.h"

#include "Kernel/CaloCellID.h"

#include "DetDesc/ConditionAccessorHolder.h"
#include "DetDesc/IGeometryInfo.h"

#include "CaloDet/DeCalorimeter.h"

#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/ClusterFunctors.h"

#include "Event/CaloCluster.h"
#include "Event/CaloDataFunctor.h"
#include "Event/CaloDigit.h"
#include "Event/CellID.h"

#include "CaloFutureUtils/CaloFutureDataFunctor.h"
#include "CaloFutureUtils/CaloFutureNeighbours.h"
#include "CaloFutureUtils/CellSelector.h"
#include "CaloFutureUtils/ClusterFunctors.h"
#include "CaloKernel/CaloVector.h"

#include <string>
#include <vector>

/** @class FutureCellularAutomatonAlg FutureCellularAutomatonAlg.h
 *
 *
 *  @author Victor Egorychev
 *  @date   2008-04-03
 */
/** @file
 *  Implementation file for class : FutureCellularAutomatonAlg
 *
 *  @date 2008-04-03
 *  @author Victor Egorychev
 */
namespace details {

  using AlgorithmWithCondition = LHCb::DetDesc::ConditionAccessorHolder<FixTESPath<Gaudi::Algorithm>>;

  template <typename... C>
  using usesConditions =
      Gaudi::Functional::Traits::use_<LHCb::DetDesc::useConditionHandleFor<C...>,
                                      Gaudi::Functional::Traits::BaseClass_t<AlgorithmWithCondition>>;
} // namespace details

namespace {
  // ============================================================================
  class CelAutoTaggedCell final {
    // ==========================================================================
  public:
    // ==========================================================================
    enum Tag { DefaultFlag, Clustered, Edge };

    enum FlagState { NotTagged, Tagged };
    // ==========================================================================
  public:
    // ==========================================================================
    // Constructor
    CelAutoTaggedCell() { m_seeds.reserve( 3 ); }
    // ==========================================================================
    // Getters
    const LHCb::CaloDigit*  digit() const { return m_digit; }
    const LHCb::CaloCellID& cellID() const { return digit()->cellID(); }
    double                  e() const { return digit()->e(); }
    bool                    isEdge() const { return ( Tagged == m_status ) && ( Edge == m_tag ); }
    bool                    isClustered() const { return ( Tagged == m_status ) && ( Clustered == m_tag ); }
    const LHCb::CaloCellID& seedForClustered() const { return m_seeds[0]; }
    const std::vector<LHCb::CaloCellID>& seeds() const { return m_seeds; }
    size_t                               numberSeeds() const { return m_seeds.size(); }
    bool                                 isSeed() const { return m_seeds.size() == 1 && cellID() == m_seeds[0]; }
    bool                                 isWithSeed( const LHCb::CaloCellID& seed ) {
      return m_seeds.end() != std::find( m_seeds.begin(), m_seeds.end(), seed );
    }
    Tag       tag() const { return m_tag; }
    FlagState status() const { return m_status; }
    // ==========================================================================
    // Setters
    void setIsSeed() {
      m_tag    = Clustered;
      m_status = Tagged;
      m_seeds.push_back( cellID() );
    }
    // ==========================================================================
    void setEdge() { m_tag = Edge; }
    void setClustered() { m_tag = Clustered; }
    void setStatus() {
      if ( ( Edge == m_tag ) || ( Clustered == m_tag ) ) { m_status = Tagged; }
    }
    void addSeed( const LHCb::CaloCellID& seed ) { m_seeds.push_back( seed ); }
    // ==========================================================================
    // operator
    CelAutoTaggedCell& operator=( const LHCb::CaloDigit* digit ) {
      m_seeds.clear();
      m_seeds.reserve( 3 );
      m_tag    = DefaultFlag;
      m_status = NotTagged;
      m_digit  = digit;
      return *this;
    }
    // ==========================================================================
  private:
    // ==========================================================================
    Tag                    m_tag    = DefaultFlag;
    FlagState              m_status = NotTagged;
    const LHCb::CaloDigit* m_digit  = nullptr;
    // ==========================================================================
    // Ident.seed(s)
    LHCb::CaloCellID::Vector m_seeds;
    // ==========================================================================
  };

  auto isClustered = []( const CelAutoTaggedCell* taggedCell ) { return taggedCell && taggedCell->isClustered(); };
  auto isClusteredOrEdge = []( const CelAutoTaggedCell* taggedCell ) {
    return taggedCell && ( taggedCell->isClustered() || taggedCell->isEdge() );
  };

  auto isWithSeed = []( LHCb::CaloCellID seed ) {
    return [seed]( CelAutoTaggedCell* taggedCell ) { return taggedCell && taggedCell->isWithSeed( seed ); };
  };

  auto setStatus = []( CelAutoTaggedCell* taggedCell ) { taggedCell->setStatus(); };

  /// container to tagged  cells with direct (by CaloCellID key)  access
  using DirVector = CaloVector<CelAutoTaggedCell*>;
  // ============================================================================
  /* Application of rules of tagging on one cell
   *   - No action if no clustered neighbor
   *   - Clustered if only one clustered neighbor
   *   - Edge if more then one clustered neighbor
   */
  // ============================================================================
  void appliRulesTagger( CelAutoTaggedCell* cell, const DirVector& hits, const DeCalorimeter* det, bool releaseBool ) {

    // Find in the neighbors cells tagged before, the clustered neighbors cells
    const LHCb::CaloCellID& cellID               = cell->cellID();
    const CaloNeighbors&    ns                   = det->neighborCells( cellID );
    bool                    hasEdgeNeighbor      = false;
    bool                    hasClusteredNeighbor = false;
    for ( const auto& iN : ns ) {
      const CelAutoTaggedCell* nei = hits[iN];
      if ( !nei ) { continue; }
      //
      if ( nei->isEdge() && releaseBool ) {
        hasEdgeNeighbor = true;
        for ( const auto& id : nei->seeds() ) {
          if ( !cell->isWithSeed( id ) ) cell->addSeed( id );
        }
      }
      //
      if ( !nei->isClustered() ) { continue; }
      hasClusteredNeighbor         = true;
      const LHCb::CaloCellID& seed = nei->seedForClustered();
      if ( cell->isWithSeed( seed ) ) { continue; }
      cell->addSeed( seed );
    }

    // Tag or not the cell

    switch ( cell->numberSeeds() ) {
    case 0:
      if ( !releaseBool ) break;
      if ( hasEdgeNeighbor && !hasClusteredNeighbor ) cell->setEdge(); //
      break;
    case 1:
      cell->setClustered();
      break;
    default:
      cell->setEdge();
      break;
    }
  }
} // namespace

namespace LHCb::Calo {

  class CellularAutomaton : public Gaudi::Functional::Transformer<LHCb::CaloCluster::Container(
                                                                      const DeCalorimeter&, const LHCb::CaloDigits& ),
                                                                  details::usesConditions<DeCalorimeter>> {

  public:
    /// Standard constructor
    CellularAutomaton( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode finalize() override; ///< Algorithm finalization

    LHCb::CaloCluster::Container operator()( const DeCalorimeter&,
                                             const LHCb::CaloDigits& ) const override; ///< Algorithm execution

  private:
    unsigned int clusterize( std::vector<std::unique_ptr<LHCb::CaloCluster>>& clusters, const LHCb::CaloDigits& hits,
                             const DeCalorimeter* detector, const std::vector<LHCb::CaloCellID>& seeds = {},
                             const unsigned int level = 0 ) const;

    bool isLocMax( const LHCb::CaloDigit& digit, const DirVector& hits, const DeCalorimeter* det ) const;

    StatusCode setEXYCluster( LHCb::CaloCluster& cluster, const DeCalorimeter* detector ) const;

    Gaudi::Property<unsigned int> m_neig_level{this, "Level", 0};

    Gaudi::Property<bool>         m_withET{this, "withET", false};
    Gaudi::Property<double>       m_ETcut{this, "ETcut", -10. * Gaudi::Units::GeV};
    Gaudi::Property<std::string>  m_usedE{this, "CellSelectorForEnergy", "3x3"};
    Gaudi::Property<std::string>  m_usedP{this, "CellSelectorForPosition", "3x3"};
    Gaudi::Property<unsigned int> m_passMax{this, "MaxIteration", 10};

    mutable Gaudi::Accumulators::StatCounter<> m_clusters{this, "# clusters"};
    mutable Gaudi::Accumulators::StatCounter<> m_passes{this, "# clusterization passes"};

    mutable Gaudi::Accumulators::StatCounter<> m_clusterEnergy{this, "Cluster energy"};
    mutable Gaudi::Accumulators::StatCounter<> m_negativeSeed{this, "Negative seed energy"};
    mutable Gaudi::Accumulators::StatCounter<> m_clusterSize{this, "Cluster size"};
  };

  // ============================================================================
  // Standard constructor, initializes variables
  // ============================================================================
  CellularAutomaton::CellularAutomaton( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     {KeyValue{"Detector", LHCb::CaloFutureAlgUtils::DeCaloFutureLocation( name )},
                      KeyValue{"InputData", LHCb::CaloFutureAlgUtils::CaloFutureDigitLocation( name )}},
                     KeyValue{"OutputData", LHCb::CaloFutureAlgUtils::CaloFutureClusterLocation( name, "EcalRaw" )} ) {}

  // ============================================================================
  // Main execution
  // ============================================================================
  LHCb::CaloCluster::Container CellularAutomaton::
                               operator()( const DeCalorimeter& calo, const LHCb::CaloDigits& digits ) const ///< Algorithm execution
  {
    // Create the container of clusters
    LHCb::CaloCluster::Container output;
    // update the version number (needed for serialization)
    output.setVersion( 2 );

    // create vector of pointers for CaloCluster
    std::vector<std::unique_ptr<LHCb::CaloCluster>> clusters;

    // clusterization tool which return the vector of pointers for CaloClusters
    unsigned int stepPass;
    if ( m_neig_level > 0 ) {
      stepPass = clusterize( clusters, digits, &calo, {}, m_neig_level );
    } else {
      stepPass = clusterize( clusters, digits, &calo );
    }

    // put to the container of clusters
    for ( auto& clus : clusters ) { output.insert( clus.release() ); }

    // statistics
    m_passes += stepPass;
    m_clusters += output.size();

    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) {
      debug() << "Built " << clusters.size() << " cellular automaton clusters  with " << stepPass << " iterations"
              << endmsg;
      debug() << " ----------------------- Cluster List : " << endmsg;
      for ( const auto& c : clusters ) {
        debug() << " Cluster seed " << c->seed() << " energy " << c->e() << " #entries " << c->entries().size()
                << endmsg;
      }
    }
    return output;
  }
  // ============================================================================
  //  Finalize
  // ============================================================================
  StatusCode CellularAutomaton::finalize() {
    info() << "Built <" << m_clusters.mean() << "> cellular automaton clusters/event  with <" << m_passes.mean()
           << "> iterations (min,max)=(" << m_passes.min() << "," << m_passes.max() << ") on average " << endmsg;
    return Transformer::finalize(); // must be called after all other actions
  }

  // ============================================================================
  bool CellularAutomaton::isLocMax( const LHCb::CaloDigit& digit, const DirVector& hits,
                                    const DeCalorimeter* det ) const {
    double                                                        e = digit.e();
    LHCb::CaloDataFunctor::EnergyTransverse<const DeCalorimeter*> et( det );
    double                                                        eT = ( m_withET ? et( &digit ) : 0. );
    for ( const auto& iN : det->neighborCells( digit.cellID() ) ) {
      const CelAutoTaggedCell* cell = hits[iN];
      if ( !cell ) { continue; }
      const LHCb::CaloDigit* nd = cell->digit();
      if ( !nd ) { continue; }
      if ( nd->e() > e ) { return false; }
      if ( m_withET ) { eT += et( nd ); }
    }
    return !m_withET || eT >= m_ETcut.value();
  }

  // ============================================================================
  StatusCode CellularAutomaton::setEXYCluster( LHCb::CaloCluster& cluster, const DeCalorimeter* detector ) const {
    ///
    double     E, X, Y;
    StatusCode sc =
        LHCb::ClusterFunctors::calculateEXY( cluster.entries().begin(), cluster.entries().end(), detector, E, X, Y );
    ///
    m_clusterEnergy += E;
    if ( sc.isSuccess() ) {
      auto& params                           = cluster.position().parameters();
      params( LHCb::CaloPosition::Index::E ) = E;
      params( LHCb::CaloPosition::Index::X ) = X;
      params( LHCb::CaloPosition::Index::Y ) = Y;
    } else {
      // FIXME: use MsgCounter once available!
      // this->Error( " E,X and Y of cluster could not be evaluated!", sc ).ignore();
    }
    ///
    return sc;
  }

  unsigned int CellularAutomaton::clusterize( std::vector<std::unique_ptr<LHCb::CaloCluster>>& clusters,
                                              const LHCb::CaloDigits& hits, const DeCalorimeter* detector,
                                              const std::vector<LHCb::CaloCellID>& _cell_list,
                                              const unsigned int                   neig_level ) const {

    // cmb: this needs to be done every time in order
    // to allow different detectors in the same algorithm
    // --> to be revisited in a future round
    const CellSelector cellSelectorE{detector, m_usedE.value()};
    const CellSelector cellSelectorP{detector, m_usedP.value()};
    bool               releaseBool = false;
    bool               useData     = false;
    //
    LHCb::CaloCellID::Set out_cells;

    /*

    _cell_list.empty()  && level > 0   ->  All local maxima + neighborhood(level)
    _cell_list.empty()  && level = 0   ->  All data (default)
    !_cell_list.empty() && level > 0   ->  seed lis + neighborhood(level)
    !_cell_list.empty() && level = 0   ->  seed list
    */

    // fill with data if level >0 and no predefined seed list
    std::vector<LHCb::CaloCellID> cell_list;
    if ( _cell_list.empty() && neig_level > 0 ) {
      useData = true;

      cell_list.reserve( hits.size() );
      for ( const auto& i : hits ) {
        const CaloNeighbors& neighbors = detector->neighborCells( i->cellID() );
        if ( std::none_of( neighbors.begin(), neighbors.end(), [&, e = i->e()]( const auto& n ) {
               if ( !detector->valid( n ) ) return false;
               const LHCb::CaloDigit* dig = hits( n );
               return dig && dig->e() > e;
             } ) ) {
          cell_list.push_back( i->cellID() );
        }
      }
    } else {
      cell_list.assign( _cell_list.begin(), _cell_list.end() );
    }

    //  info() << "Cell list " << _cell_list << " level = " << neig_level  << endmsg;

    // if list of "seed" is not empty
    if ( !cell_list.empty() ) {
      out_cells.insert( cell_list.begin(), cell_list.end() );

      /** find all neighbours for the given set of cells for the givel level
       *  @param cells    (UPDATE) list of cells
       *  @param level    (INPUT)  level
       *  @param detector (INPUT) the detector
       *  @return true if neighbours are added
       */
      LHCb::CaloFutureFunctors::neighbours( out_cells, neig_level, detector );
    }

    // z-position of cluster
    LHCb::ClusterFunctors::ZPosition zPosition( detector );

    size_t                         local_size = cell_list.empty() ? hits.size() : out_cells.size();
    const CelAutoTaggedCell        cell0_     = CelAutoTaggedCell();
    std::vector<CelAutoTaggedCell> local_cells( local_size, cell0_ );

    // Create access direct and sequential on the tagged cells
    DirVector taggedCellsDirect{nullptr};
    taggedCellsDirect.reserve( local_size );
    taggedCellsDirect.setSize( 14000 );

    /// container to tagged  cells with sequential access
    std::vector<CelAutoTaggedCell*> taggedCellsSeq;
    taggedCellsSeq.reserve( local_size );

    if ( cell_list.empty() ) {
      // fill with the data
      size_t index = 0;

      for ( auto ihit = hits.begin(); hits.end() != ihit; ++ihit, ++index ) {
        const LHCb::CaloDigit* digit = *ihit;
        if ( !digit ) { continue; } // CONTINUE !!!
        CelAutoTaggedCell& taggedCell = local_cells[index];
        taggedCell                    = digit;

        taggedCellsDirect.addEntry( &taggedCell, digit->cellID() );
        taggedCellsSeq.push_back( &taggedCell );
      }
    } else { // fill for HLT
      size_t index = 0;

      for ( auto icell = out_cells.begin(); out_cells.end() != icell; ++icell, ++index ) {

        const LHCb::CaloDigit* digit = hits( *icell );
        if ( !digit ) { continue; } // CONTINUE !!!

        CelAutoTaggedCell& taggedCell = local_cells[index];
        taggedCell                    = digit;

        taggedCellsDirect.addEntry( &taggedCell, digit->cellID() );
        taggedCellsSeq.push_back( &taggedCell );
      }
    }

    // Find and mark the seeds (local maxima)
    if ( useData ) {
      for ( const auto& seed : cell_list ) taggedCellsDirect[seed]->setIsSeed();
    } else {
      for ( const auto& itTag : taggedCellsSeq ) {
        if ( isLocMax( *itTag->digit(), taggedCellsDirect, detector ) ) { itTag->setIsSeed(); }
      }
    }

    /// Tag the cells which are not seeds
    auto itTagLastSeed = std::stable_partition( taggedCellsSeq.begin(), taggedCellsSeq.end(), isClustered );

    auto         itTagLastClustered = itTagLastSeed;
    auto         itTagFirst         = itTagLastClustered;
    unsigned int nPass              = 0;
    while ( itTagLastClustered != taggedCellsSeq.end() ) {

      // Apply rules tagger for all not tagged cells
      for ( auto itTag = itTagLastClustered; taggedCellsSeq.end() != itTag; ++itTag ) {
        appliRulesTagger( *itTag, taggedCellsDirect, detector, releaseBool );
      }

      // Valid result
      std::for_each( itTagFirst, taggedCellsSeq.end(), setStatus );

      itTagLastClustered = std::stable_partition( itTagFirst, taggedCellsSeq.end(), isClusteredOrEdge );

      // Test if cells are tagged in this pass
      if ( itTagLastClustered == itTagFirst && releaseBool ) {
        const long number = taggedCellsSeq.end() - itTagLastClustered;
        if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
          debug() << " TAGGING NOT FULL - Remain " << std::to_string( number ) << " not clustered cells" << endmsg;
        itTagLastClustered = taggedCellsSeq.end();
      }
      if ( itTagLastClustered == itTagFirst )
        releaseBool = true; // try additional passes releasing appliRulesTagger criteria
      nPass++;
      itTagFirst = itTagLastClustered;
      if ( m_passMax > 0 && nPass >= m_passMax ) break;
    }

    constexpr LHCb::CaloDigitStatus::Status usedForE =
        LHCb::CaloDigitStatus::UseForEnergy | LHCb::CaloDigitStatus::UseForCovariance;
    constexpr LHCb::CaloDigitStatus::Status usedForP =
        LHCb::CaloDigitStatus::UseForPosition | LHCb::CaloDigitStatus::UseForCovariance;
    constexpr LHCb::CaloDigitStatus::Status seed =
        LHCb::CaloDigitStatus::SeedCell | LHCb::CaloDigitStatus::LocalMaximum | usedForP | usedForE;

    itTagLastClustered   = std::stable_partition( itTagLastSeed, taggedCellsSeq.end(), isClustered );
    auto itTagSeed       = taggedCellsSeq.begin();
    auto itTagClustered1 = itTagLastSeed;
    while ( itTagSeed != itTagLastSeed ) {
      const LHCb::CaloDigit* digit = ( *itTagSeed )->digit();

      if ( digit->e() <= 0 ) {
        itTagSeed++;
        m_negativeSeed += digit->e();
        continue; // does not keep cluster with seed.energy <= 0
      }

      LHCb::CaloCellID seedID = ( *itTagSeed )->cellID();
      // Do partitions first
      auto itTagClustered2 = std::stable_partition( itTagClustered1, itTagLastClustered, isWithSeed( seedID ) );
      auto itTagFirstEdge  = itTagLastClustered;
      auto itTagLastEdge   = std::stable_partition( itTagLastClustered, taggedCellsSeq.end(), isWithSeed( seedID ) );

      // Create cluster, reserve, fill
      auto cluster = std::make_unique<LHCb::CaloCluster>();
      cluster->entries().reserve( 1 + ( itTagClustered2 - itTagClustered1 ) + ( itTagLastEdge - itTagFirstEdge ) );

      // set seed
      cluster->entries().emplace_back( digit, seed );
      cluster->setSeed( digit->cellID() );

      // Owned cells
      for ( ; itTagClustered1 != itTagClustered2; ++itTagClustered1 ) {
        LHCb::CaloCellID              cellID = ( *itTagClustered1 )->cellID();
        const LHCb::CaloDigit*        digit  = ( *itTagClustered1 )->digit();
        LHCb::CaloDigitStatus::Status owned  = LHCb::CaloDigitStatus::OwnedCell;
        if ( cellSelectorE( seedID, cellID ) > 0. ) owned |= usedForE;
        if ( cellSelectorP( seedID, cellID ) > 0. ) owned |= usedForP;
        cluster->entries().emplace_back( digit, owned );
      }
      // Shared cells
      for ( ; itTagFirstEdge != itTagLastEdge; ++itTagFirstEdge ) {
        const LHCb::CaloDigit*        digit  = ( *itTagFirstEdge )->digit();
        LHCb::CaloCellID              cellID = ( *itTagFirstEdge )->cellID();
        LHCb::CaloDigitStatus::Status shared = LHCb::CaloDigitStatus::SharedCell;
        if ( cellSelectorE( seedID, cellID ) > 0. ) shared |= usedForE;
        if ( cellSelectorP( seedID, cellID ) > 0. ) shared |= usedForP;
        cluster->entries().emplace_back( digit, shared );
      };

      if ( setEXYCluster( *cluster, detector ).isSuccess() ) {
        cluster->setType( LHCb::CaloCluster::Type::CellularAutomaton );
        cluster->position().setZ( zPosition( cluster.get() ) );
        //  put cluster to the output
        clusters.push_back( std::move( cluster ) );
        m_clusterEnergy += clusters.back()->entries().size();
      }
      itTagClustered1 = itTagClustered2;
      itTagSeed++;
    }

    return nPass;
  }
} // namespace LHCb::Calo
// ============================================================================
// Declaration of the Algorithm Factory
// ============================================================================
DECLARE_COMPONENT_WITH_ID( LHCb::Calo::CellularAutomaton, "FutureCellularAutomatonAlg" )
// =============================================================================
// the END
// =============================================================================
