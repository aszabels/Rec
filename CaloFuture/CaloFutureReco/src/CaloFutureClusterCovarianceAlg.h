/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ===========================================================================
#ifndef CALOFUTURERECO_CALOFUTURECLUSTERCOVARIANCEALG_H
#define CALOFUTURERECO_CALOFUTURECLUSTERCOVARIANCEALG_H 1
/// ===========================================================================
// Include files
// from STL

#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/Counters.h"
#include <string>

#include "Event/CaloCluster.h"

#include "ICaloFutureClusterTool.h"
class FutureSubClusterSelectorTool;

/** @class CaloFutureClusterCovarianceAlg CaloFutureClusterCovarianceAlg.h
 *
 *   Simple algorithm for evaluation of covariance matrix
 *   for CaloFutureCluster object
 *
 *  @author Vanya BElyaev Ivan Belyaev
 *  @date   04/07/2001
 */

class CaloFutureClusterCovarianceAlg
    : public Gaudi::Functional::Transformer<LHCb::CaloCluster::Container( const LHCb::CaloCluster::Container& )> {

public:
  /** Standard constructor
   *  @param   name          algorithm name
   *  @param   pSvcLocator   pointer to Service Locator
   */
  CaloFutureClusterCovarianceAlg( const std::string& name, ISvcLocator* pSvcLocator );

  /** standard initialization method
   *  @see GaudiAlgorithm
   *  @see     Algorithm
   *  @see    IAlgorithm
   *  @return status code
   */
  StatusCode initialize() override;

  /** standard execution method
   *  @see GaudiAlgorithm
   *  @see     Algorithm
   *  @see    IAlgorithm
   *  @return status code
   */
  LHCb::CaloCluster::Container operator()( const LHCb::CaloCluster::Container& ) const override; ///< Algorithm
                                                                                                 ///< execution

protected:
  StatusCode                          cov( LHCb::CaloCluster& c ) const { return ( *m_cov )( c ); }
  StatusCode                          spread( LHCb::CaloCluster& c ) const { return ( *m_spread )( c ); }
  FutureSubClusterSelectorTool const& tagger() const { return *m_tagger; }

private:
  // CHECK if needed for CaloFuture
  // bool m_copy = false; /// copy flag

  // tool used for covariance matrix calculation
  Gaudi::Property<std::string>  m_covType{this, "CovarianceType", "FutureClusterCovarianceMatrixTool"};
  Gaudi::Property<std::string>  m_covName{this, "CovarianceName"};
  ICaloFutureClusterTool*       m_cov    = nullptr; ///< tool
  FutureSubClusterSelectorTool* m_tagger = nullptr;

  // tool used for cluster spread estimation
  Gaudi::Property<std::string> m_spreadType{this, "SpreadType", "FutureClusterSpreadTool"};
  Gaudi::Property<std::string> m_spreadName{this, "SpreadName"};
  ICaloFutureClusterTool*      m_spread = nullptr; ///< tool

  Gaudi::Property<std::string> m_tagName{this, "TaggerName"};

  // following properties are inherited by the selector tool when defined:
  Gaudi::Property<std::vector<std::string>> m_taggerE{this, "EnergyTags"};
  Gaudi::Property<std::vector<std::string>> m_taggerP{this, "PositionTags"};

  // collection of known cluster shapes
  std::map<std::string, std::string>         m_clusterShapes;
  std::map<std::string, std::vector<double>> m_covParams;

  // counter
  mutable Gaudi::Accumulators::Counter<> m_clusters{this, "# clusters"};
};

#endif // CALOFUTURERECO_CALOFUTURECLUSTERCOVARIANCEALG_H
