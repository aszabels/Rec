/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
// local
#include "CaloFutureDigitFilterTool.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloFutureDigitFilterTool
//
// 2010-12-13 : Olivier Deschamps
//-----------------------------------------------------------------------------
namespace Gaudi::Parsers {
  StatusCode parse( std::map<CaloCellCode::CaloIndex, int>& out, const std::string& s ) {
    out.clear();
    std::map<std::string, int> m;
    StatusCode                 sc = parse( m, s );
    if ( sc.isSuccess() ) {
      std::transform( m.begin(), m.end(), std::insert_iterator( out, out.end() ), []( const auto& p ) {
        if ( p.first == "Default" ) return std::pair{Default, p.second};
        auto k = CaloCellCode::caloNum( p.first );
        if ( k < 0 ) throw std::invalid_argument{std::string{"unknown Calo: "} + p.first};
        return std::pair{k, p.second};
      } );
    }
    return sc;
  }
} // namespace Gaudi::Parsers

// Declaration of the Tool Factory
DECLARE_COMPONENT( CaloFutureDigitFilterTool )

const CaloFutureDigitFilterTool::CondMaps CaloFutureDigitFilterTool::m_nullMaps = {};

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
CaloFutureDigitFilterTool::CaloFutureDigitFilterTool( const std::string& type, const std::string& name,
                                                      const IInterface* parent )
    : extends( type, name, parent ) {
  declareProperty( "EcalOffset", m_ecalMaps.offsets );
  declareProperty( "HcalOffset", m_hcalMaps.offsets );
  declareProperty( "PrsOffset", m_prsMaps.offsets );
  declareProperty( "EcalOffsetRMS", m_ecalMaps.offsetsRMS );
  declareProperty( "HcalOffsetRMS", m_hcalMaps.offsetsRMS );
  declareProperty( "PrsOffsetRMS", m_prsMaps.offsetsRMS );
}
//=============================================================================

StatusCode CaloFutureDigitFilterTool::initialize() {
  StatusCode sc = extends::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;       // error printed already by CaloFuture2Dview
  if ( m_vertLoc.empty() ) {
    m_vertLoc = ( m_usePV3D.value() ) ? LHCb::RecVertexLocation::Velo3D : LHCb::RecVertexLocation::Primary;
  }

  // check
  for ( const auto& i : m_maskMap ) {
    if ( ( i.second & CaloCellQuality::OfflineMask ) == 0 )
      Warning( "OfflineMask flag is disabled for " + caloName( i.first ) + " - Are you sure ?", StatusCode::SUCCESS )
          .ignore();
  }
  // subscribe to incident Svc
  incSvc()->addListener( this, IncidentType::BeginEvent );

  return sc;
}

//-----------------------
StatusCode CaloFutureDigitFilterTool::caloUpdate() {
  // loop over all current maps and recreate
  for ( auto& maps : m_offsetMap ) createMaps( maps.first, false );
  const auto offsets = m_offsetMap.find( m_calo );
  setMaps( offsets != m_offsetMap.end() ? *( offsets->second ) : createMaps( m_calo ) );
  return StatusCode::SUCCESS;
}

//-----------------------
const CaloFutureDigitFilterTool::CondMaps& CaloFutureDigitFilterTool::createMaps( DeCalorimeter* calo,
                                                                                  bool           regUpdate ) {
  // Map new set of maps for this calo
  m_offsetMap[calo] = std::make_unique<CondMaps>();
  auto* maps        = m_offsetMap[calo].get();
  // fill the maps from the CaloFuture DetElem
  for ( const auto& c : calo->cellParams() ) {
    const auto id = c.cellID();
    if ( !calo->valid( id ) || id.isPin() ) continue;
    maps->offsets[id]    = c.pileUpOffset();
    maps->offsetsRMS[id] = c.pileUpOffsetRMS();
  }
  // if first time, register callback for future updates
  if ( regUpdate ) { updMgrSvc()->registerCondition( this, calo, &CaloFutureDigitFilterTool::caloUpdate ); }
  // return the new maps
  return *maps;
}

//-----------------------
bool CaloFutureDigitFilterTool::setDet( CaloCellCode::CaloIndex det ) {
  if ( m_calo && m_calo->index() == det ) return true;
  m_calo = getDet<DeCalorimeter>( LHCb::CaloFutureAlgUtils::DeCaloFutureLocation( det ) );
  if ( !m_calo ) return false;
  assert( m_calo->index() == det );
  auto it = m_maskMap.find( det );
  if ( it == m_maskMap.end() ) it = m_maskMap.find( Default );
  if ( m_useCondDB.value() ) {
    m_scalingMethod = m_calo->pileUpSubstractionMethod();
    m_scalingMin    = m_calo->pileUpSubstractionMin();
    m_scalingBin    = m_calo->pileUpSubstractionBin();
    if ( m_scalingMethod < 0 ) {
      setMaps( m_nullMaps );
    } else {
      auto caloOffs = m_offsetMap.find( m_calo );
      setMaps( caloOffs != m_offsetMap.end() ? *( caloOffs->second ) : createMaps( m_calo ) );
    }
  } else {
    setMaps( det == CaloCellCode::CaloIndex::EcalCalo
                 ? m_ecalMaps
                 : det == CaloCellCode::CaloIndex::HcalCalo
                       ? m_hcalMaps
                       : det == CaloCellCode::CaloIndex::PrsCalo ? m_prsMaps : m_nullMaps );
  }
  return true;
}

//-----------------------
double CaloFutureDigitFilterTool::getOffset( LHCb::CaloCellID id, int scale ) const {
  if ( 0 == scale ) return 0.;
  if ( scale <= m_scalingMin ) return 0.;
  const auto& table = offsets();
  const auto  it    = table.find( id );
  if ( it == table.end() ) return 0.;
  const auto& ref = it->second;
  //  overlap probabilty (approximate)
  constexpr double ncells = 6016.;
  const double     aa     = 4 * scale / ncells;
  double           rscale =
      ( aa < 1 ? 0.5 * ncells * ( 1. - std::sqrt( 1. - aa ) ) : ( double( ncells * scale ) / ( ncells - scale ) ) );
  return ref * ( rscale - m_scalingMin ) / m_scalingBin;
}

//-----------------------
int CaloFutureDigitFilterTool::getScale() {
  if ( !m_scale ) { m_scale = ( m_scalingMethod == 1 || m_scalingMethod == 11 ) ? nVertices() : 0; }
  return *m_scale;
}

double CaloFutureDigitFilterTool::offset( LHCb::CaloCellID id ) {
  if ( !m_calo || id.calo() != m_calo->index() ) {
    if ( !setDet( id.calo() ) ) return 0.;
  }
  return getOffset( id, getScale() );
}

//================= Scale accessors

unsigned int CaloFutureDigitFilterTool::nVertices() {
  int nVert = 0;
  if ( !m_usePV3D.value() ) {
    LHCb::RecVertices* verts = getIfExists<LHCb::RecVertices>( evtSvc(), m_vertLoc );
    if ( verts ) return verts->size();
  }
  // try PV3D if explicitely requested or if RecVertices not found
  if ( !m_usePV3D.value() ) m_vertLoc = LHCb::RecVertexLocation::Velo3D;
  LHCb::VertexBases* verts = getIfExists<LHCb::VertexBases>( m_vertLoc );
  if ( verts ) nVert = verts->size();
  return nVert;
}

StatusCode CaloFutureDigitFilterTool::finalize() {
  incSvc()->removeListener( this );
  return extends::finalize(); // must be called after all other actions
}
