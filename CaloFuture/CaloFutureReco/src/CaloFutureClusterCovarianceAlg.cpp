/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//  ===========================================================================
#define CALOFUTURERECO_CALOFUTURECLUSTERCOVARIANCEALG_CPP 1
/// ===========================================================================

#include <vector>

// Include files
#include "CaloFutureClusterCovarianceAlg.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CovarianceEstimator.h"
#include "Event/CaloCluster.h"
#include "FutureSubClusterSelectorTool.h"

// ===========================================================================
/** @file
 *
 *  Implementation file for class CaloFutureClusterCovarianceAlg
 *
 *  @see CaloFutureClusterCovarianceAlg
 *  @see ICaloFutureClusterTool
 *  @see ICaloFutureSubClusterTag
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date 04/07/2001
s */
// ===========================================================================

DECLARE_COMPONENT( CaloFutureClusterCovarianceAlg )

// ============================================================================
/** Standard constructor
 *  @param   name          algorithm name
 *  @param   pSVcLocator   pointer to Service Locator
 */
// ============================================================================
CaloFutureClusterCovarianceAlg::CaloFutureClusterCovarianceAlg( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator,
                   KeyValue{"InputData", LHCb::CaloFutureAlgUtils::CaloFutureClusterLocation( name, "EcalOverlap" )},
                   KeyValue{"OutputData", LHCb::CaloFutureAlgUtils::CaloFutureClusterLocation( name, "Ecal" )} ) {
  // following properties might be inherited by the covariance tool
  declareProperty( "CovarianceParameters", m_covParams ); // KEEP IT UNSET ! INITIAL VALUE WOULD BYPASS DB ACCESS

  std::string caloName = LHCb::CaloFutureAlgUtils::CaloFutureNameFromAlg( name );
  m_covName            = caloName + "CovarTool";
  m_spreadName         = caloName + "SpreadTool";
  m_tagName            = caloName + "ClusterTag";
}

// ===========================================================================
/** standard initialization method
 *  @see GaudiAlgorithm
 *  @see     Algorithm
 *  @see    IAlgorithm
 *  @return status code
 */
// ===========================================================================
StatusCode CaloFutureClusterCovarianceAlg::initialize() {
  // try to initialize base class

  StatusCode sc = Transformer::initialize();
  if ( sc.isFailure() ) { return Error( "Could not initialize the base class Transformer" ); }

  // locate the tagger (inherit from relevant properties)
  m_tagger = tool<FutureSubClusterSelectorTool>( "FutureSubClusterSelectorTool", m_tagName, this );

  // locate the tool for covariance matrix calculations
  m_cov = m_covName.empty() ? tool<ICaloFutureClusterTool>( m_covType, this )
                            : tool<ICaloFutureClusterTool>( m_covType, m_covName, this );

  // locate the tool for cluster spread(2nd moments) estimation
  m_spread = m_spreadName.empty() ? tool<ICaloFutureClusterTool>( m_spreadType, this )
                                  : tool<ICaloFutureClusterTool>( m_spreadType, m_spreadName, this );

  // copy flag
  // CHECK if needed, based on final input/output containers/vectors
  // m_copy = ( !m_inputData.empty() ) && (m_outputData.value() != m_inputData.value() );

  return StatusCode::SUCCESS;
}

// ===========================================================================
LHCb::CaloCluster::Container CaloFutureClusterCovarianceAlg::
                             operator()( const LHCb::CaloCluster::Container& clusters ) const {
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "==> operator" << endmsg;

  // create new container for output clusters
  LHCb::CaloCluster::Container outputClusters;
  // update the version number (needed for serialization)
  outputClusters.setVersion( 2 );
  outputClusters.reserve( clusters.size() );
  // copy clusters to new container
  for ( const auto* cl : clusters ) { outputClusters.insert( new LHCb::CaloCluster( *cl ) ); }

  for ( auto* cl : outputClusters ) {
    // skip nulls
    if ( !cl ) { continue; }

    // == APPLY TAGGER
    StatusCode sc = m_tagger->tag( *( cl ) );
    if ( sc.isFailure() ) {
      Error( "Error from tagger, skip cluster ", sc ).ignore();
      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << cl << endmsg;
      continue;
    }

    // == APPLY COVARIANCE ESTIMATOR
    sc = cov( *cl );
    if ( sc.isFailure() ) {
      Error( "Error from cov,    skip cluster ", sc, 0 ).ignore();
      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << cl << endmsg;
      continue;
    }

    // == APPLY SPREAD ESTIMATOR
    sc = spread( *cl );
    if ( sc.isFailure() ) {
      Error( "Error from spread, skip cluster ", sc, 0 ).ignore();
      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << cl << endmsg;
      continue;
    }
  }

  // == counter
  m_clusters += clusters.size();

  return outputClusters;
}
