/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloFutureTrack2IDAlg.h"

// ============================================================================
/** @class FutureTrack2HcalEAlg FutureTrack2HcalEAlg.cpp
 *  preconfigured instance of class  CaloFutureTrack2IDAlg
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2006-06-15
 */
namespace LHCb::Calo {
  struct Track2HcalEAlg final : public Track2IDAlg {
    Track2HcalEAlg( const std::string& name, ISvcLocator* pSvc ) : Track2IDAlg( name, pSvc ) {
      setProperty( "Digits", LHCb::CaloDigitLocation::Hcal ).ignore();
      using LHCb::CaloFutureAlgUtils::CaloFutureIdLocation;
      setProperty( "Output", CaloFutureIdLocation( "HcalE" ) ).ignore();
      setProperty( "Filter", CaloFutureIdLocation( "InHcal" ) ).ignore();
      setProperty( "Tool", "FutureHcalEnergyForTrack/HcalEFuture" ).ignore();
    }
  };
} // namespace LHCb::Calo
// ============================================================================

DECLARE_COMPONENT_WITH_ID( LHCb::Calo::Track2HcalEAlg, "FutureTrack2HcalEAlg" )

// ============================================================================
