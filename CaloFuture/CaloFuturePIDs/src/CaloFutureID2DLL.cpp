/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloFutureID2DLL.h"
#include "DetDesc/HistoParam.h"
#include "GaudiKernel/ITHistSvc.h"
#include "GaudiUtils/Aida2ROOT.h"
#include <type_traits>

// ============================================================================
/** @file
 *  Implementation file for class CaloFutureID2DLL
 *  @date 2006-06-18
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 */
// ============================================================================
DECLARE_COMPONENT_WITH_ID( LHCb::Calo::ID2DLL, "CaloFutureID2DLL" )
namespace LHCb::Calo {
  // ============================================================================
  /// standard proected constructor
  // ============================================================================

  ID2DLL::ID2DLL( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator, KeyValue{"Input", ""}, KeyValue{"Output", ""} ) {
    // track types
    setProperty( "AcceptedType",
                 Gaudi::Utils::toString<int>( Track::Types::Long, Track::Types::Ttrack, Track::Types::Downstream ) )
        .ignore();
  }

  //==============================================================================
  // Algorithm initialization
  //==============================================================================

  StatusCode ID2DLL::initialize() {
    StatusCode sc = Transformer::initialize();
    if ( sc.isFailure() ) return sc;

    if ( !existDet<DataObject>( detSvc(), m_conditionName ) ) {
      warning() << "Initialise: Condition '" << m_conditionName << "' not found -- switch to reading the DLLs from THS!"
                << endmsg;
      m_useCondDB = false;
    }

    sc = m_useCondDB ? initializeWithCondDB() : initializeWithoutCondDB();
    return sc;
  }

  // ============================================================================

  StatusCode ID2DLL::initializeWithCondDB() {
    info() << "init with CondDB, m_conditionName = " << m_conditionName.value() << endmsg;
    try {
      registerCondition( m_conditionName, m_cond, &ID2DLL::i_updateDLL );
    } catch ( GaudiException& e ) {
      fatal() << e << endmsg;
      return StatusCode::FAILURE;
    }
    StatusCode sc = runUpdate(); // load the conditions
    return sc;
  }

  // ============================================================================

  StatusCode ID2DLL::initializeWithoutCondDB() {
    info() << "init w/o CondDB (read the 2D DLL histograms from a root file "
              "through THS)"
           << endmsg;

    // locate the histogram
    if ( !m_title_lt_ths.empty() ) {
      AIDA::IHistogram2D* aida = get<AIDA::IHistogram2D>( histoSvc(), m_title_lt_ths );
      m_histo_lt               = Gaudi::Utils::Aida2ROOT::aida2root( aida );
    }

    if ( !m_title_dt_ths.empty() ) {
      AIDA::IHistogram2D* aida = get<AIDA::IHistogram2D>( histoSvc(), m_title_dt_ths );
      m_histo_dt               = Gaudi::Utils::Aida2ROOT::aida2root( aida );
    }

    if ( !m_title_tt_ths.empty() ) {
      AIDA::IHistogram2D* aida = get<AIDA::IHistogram2D>( histoSvc(), m_title_tt_ths );
      m_histo_tt               = Gaudi::Utils::Aida2ROOT::aida2root( aida );
    }

    if ( !m_title_vt_ths.empty() ) {
      AIDA::IHistogram2D* aida = get<AIDA::IHistogram2D>( histoSvc(), m_title_vt_ths );
      m_histo_vt               = Gaudi::Utils::Aida2ROOT::aida2root( aida );
    }

    if ( !m_title_ut_ths.empty() ) {
      AIDA::IHistogram2D* aida = get<AIDA::IHistogram2D>( histoSvc(), m_title_ut_ths );
      m_histo_ut               = Gaudi::Utils::Aida2ROOT::aida2root( aida );
    }

    if ( msgLevel( MSG::DEBUG ) )
      debug() << "\nLong:        '" << m_title_lt_ths << "' -> " << (void*)m_histo_lt << "\nDownstream:  '"
              << m_title_dt_ths << "' -> " << (void*)m_histo_dt << "\nTTuricensis: '" << m_title_tt_ths << "' -> "
              << (void*)m_histo_tt << "\nUpstream:    '" << m_title_ut_ths << "' -> " << (void*)m_histo_ut
              << "\nVelo:        '" << m_title_vt_ths << "' -> " << (void*)m_histo_vt << endmsg;

    return StatusCode::SUCCESS;
  }

  // ============================================================================
  /// Algorithm execution
  // ============================================================================

  Table ID2DLL::operator()( const Table& input ) const {

    // create and register the output ocntainer
    const auto links = input.relations();
    auto       table = Table( links.size() );

    // loop over all links
    for ( const auto link : links ) {
      const auto   track = link.from();
      const double value = link.to();

      if ( m_type.value().end() == std::find( m_type.value().begin(), m_type.value().end(), track->type() ) ) {
        continue;
      }

      if ( track->checkFlag( LHCb::Track::Flags::Backward ) ) { continue; }

      // convert value to DLL
      const double DLL = dLL( track->p(), value, track->type() );

      // fill the relation table
      table.i_push( track, (float)DLL ); // NB!: i_push
    };
    /// MANDATORY: i_sort after i_push
    table.i_sort();

    if ( counterStat->isQuiet() ) counter( inputLocation() + "=>" + outputLocation() ) += table.i_relations().size();

    return table;
  }

  // ============================================================================

  StatusCode ID2DLL::i_updateDLL() {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "i_updateDLL() called" << endmsg;
    if ( !m_cond ) return StatusCode::FAILURE;

    try {
      if ( !m_title_lt.empty() ) {
        m_histo_lt = reinterpret_cast<TH2D*>( &m_cond->param<DetDesc::Params::Histo2D>( m_title_lt ) );
      }

      if ( !m_title_dt.empty() ) {
        m_histo_dt = reinterpret_cast<TH2D*>( &m_cond->param<DetDesc::Params::Histo2D>( m_title_dt ) );
      }

      if ( !m_title_tt.empty() ) {
        m_histo_tt = reinterpret_cast<TH2D*>( &m_cond->param<DetDesc::Params::Histo2D>( m_title_tt ) );
      }

      if ( !m_title_ut.empty() ) {
        m_histo_ut = reinterpret_cast<TH2D*>( &m_cond->param<DetDesc::Params::Histo2D>( m_title_ut ) );
      }

      if ( !m_title_vt.empty() ) {
        m_histo_vt = reinterpret_cast<TH2D*>( &m_cond->param<DetDesc::Params::Histo2D>( m_title_vt ) );
      }
    } catch ( GaudiException& exc ) {
      fatal() << "DLL update failed! msg ='" << exc << "'" << endmsg;
      return StatusCode::FAILURE;
    }

    if ( msgLevel( MSG::DEBUG ) )
      debug() << "\nLong:        '" << m_title_lt << "' -> " << (void*)m_histo_lt << "\nDownstream:  '" << m_title_dt
              << "' -> " << (void*)m_histo_dt << "\nTTuricensis: '" << m_title_tt << "' -> " << (void*)m_histo_tt
              << "\nUpstream:    '" << m_title_ut << "' -> " << (void*)m_histo_ut << "\nVelo:        '" << m_title_vt
              << "' -> " << (void*)m_histo_vt << endmsg;

    return StatusCode::SUCCESS;
  }
} // namespace LHCb::Calo
// ============================================================================
