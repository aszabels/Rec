/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef FutureGammaPi0XGBoostTool_H
#define FutureGammaPi0XGBoostTool_H

// Include files
// from Gaudi
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureInterfaces/IFutureGammaPi0SeparationTool.h"
#include "GaudiAlg/GaudiTool.h"

// Math
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/Line.h"

// using xgb in c++

#include "FutureXGBClassifierPhPi0.h"

/** @class FutureGammaPi0XGBoostTool FutureGammaPi0XGBoostTool.h
 *
 *
 *  @author @sayankotor
 *  @date   2018-03-24
 */
namespace LHCb::Calo {
  class GammaPi0XGBoost : public extends<GaudiTool, Interfaces::IGammaPi0Separation> {

  public:
    using extends::extends;

    StatusCode initialize() override;

    std::optional<double>                   isPhoton( const LHCb::CaloHypo& hypo ) override;
    std::optional<double>                   isPhoton( LHCb::span<const double> ) override { return 0; }
    const std::map<Enum::DataType, double>& inputDataMap() override {
      static std::map<Enum::DataType, double> s_map;
      return s_map;
    }

  private:
    bool GetRawEnergy( const LHCb::CaloHypo& hypo, int cluster_type, LHCb::span<double, 25> rawEnergy ) const;
    Gaudi::Property<float> m_minPt{this, "MinPt", 2000.};

    std::array<std::optional<FutureXGBClassifierPhPi0>, 9> m_xgb;

    DataObjectReadHandle<LHCb::CaloDigits> m_digits{this, "DigitLocation", LHCb::CaloDigitLocation::Ecal};

    const DeCalorimeter* m_ecal = nullptr;
  };
} // namespace LHCb::Calo
#endif // FutureGammaPi0XGBoostTool_H
