/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTUREHYPO2CALOFUTURE_H
#define CALOFUTUREHYPO2CALOFUTURE_H 1

// Include files
// from Gaudi
#include "CaloFuture2CaloFuture.h"
#include "CaloFutureInterfaces/ICaloFutureHypo2CaloFuture.h" // Interface
#include "CaloFutureUtils/CellNeighbour.h"
#include "GaudiAlg/GaudiTool.h"

/** @class CaloFutureHypo2CaloFuture CaloFutureHypo2CaloFuture.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2008-09-11
 */
class CaloFutureHypo2CaloFuture : public extends<CaloFuture2CaloFuture, LHCb::Calo::Interfaces::IHypo2Calo> {
public:
  /// Standard constructor
  using extends::extends;

  StatusCode initialize() override;
  // cellIDs
  using CaloFuture2CaloFuture::cellIDs;
  const std::vector<LHCb::CaloCellID>& cellIDs( const LHCb::CaloHypo&   fromHypo,
                                                CaloCellCode::CaloIndex toCaloFuture ) const override;
  const std::vector<LHCb::CaloCellID>& cellIDs( const LHCb::CaloCluster& fromCluster,
                                                CaloCellCode::CaloIndex  toCaloFuture ) const override;
  const std::vector<LHCb::CaloCellID>& cellIDs() const override { return m_cells; };
  // digits
  using CaloFuture2CaloFuture::digits;
  const std::vector<LHCb::CaloDigit*>& digits( const LHCb::CaloCluster& fromCluster,
                                               CaloCellCode::CaloIndex  toCaloFuture ) const override;
  const std::vector<LHCb::CaloDigit*>& digits( const LHCb::CaloHypo&   fromHypo,
                                               CaloCellCode::CaloIndex toCaloFuture ) const override;
  const std::vector<LHCb::CaloDigit*>& digits() const override { return m_digits; };
  // energy
  using CaloFuture2CaloFuture::energy;
  double energy( const LHCb::CaloCluster& fromCluster, CaloCellCode::CaloIndex toCaloFuture ) const override;
  double energy( const LHCb::CaloHypo& fromHypo, CaloCellCode::CaloIndex toCaloFuture ) const override;
  double energy() const override { return m_energy; };
  // multiplicity
  using CaloFuture2CaloFuture::multiplicity;
  int multiplicity( const LHCb::CaloCluster& fromCluster, CaloCellCode::CaloIndex toCaloFuture ) const override;
  int multiplicity( const LHCb::CaloHypo& fromHypo, CaloCellCode::CaloIndex toCaloFuture ) const override;
  int multiplicity() const override { return m_count; };
  using CaloFuture2CaloFuture::setCalos;
  void setCalos( CaloCellCode::CaloIndex fromCaloFuture, CaloCellCode::CaloIndex toCaloFuture ) const override {
    return CaloFuture2CaloFuture::setCalos( fromCaloFuture, toCaloFuture );
  }

private:
  Gaudi::Property<bool>    m_seed{this, "Seed", true};
  Gaudi::Property<bool>    m_neighb{this, "AddNeighbors", true};
  Gaudi::Property<bool>    m_line{this, "PhotonLine", true};
  Gaudi::Property<bool>    m_whole{this, "WholeCluster", false};
  Gaudi::Property<int>     m_status{this, "StatusMask", 0x0};
  Gaudi::Property<float>   m_x{this, "xTolerance", 5. * Gaudi::Units::mm};
  Gaudi::Property<float>   m_y{this, "yTolerance", 5. * Gaudi::Units::mm};
  mutable CellNeighbour    m_neighbour;
  mutable LHCb::CaloCellID m_lineID;
  mutable Gaudi::XYZPoint  m_point;
};
#endif // CALOFUTUREHYPO2CALOFUTURE_H
