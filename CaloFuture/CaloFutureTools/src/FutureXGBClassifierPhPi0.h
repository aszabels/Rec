/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef XGBCLASSIFIER_H
#define XGBCLASSIFIER_H

#include "GaudiKernel/IMessageSvc.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/ServiceHandle.h"
#include "Kernel/STLExtensions.h"
#include "xgboost/c_api.h"
#include <mutex>
#include <string>
#include <vector>

class FutureXGBClassifierPhPi0 {
public:
  FutureXGBClassifierPhPi0();
  FutureXGBClassifierPhPi0( const std::string& path );
  /**    * @brief      Main classification method    *
   * Takes a vector of feature values, and return the corresponding MVA output.
   *    * @param[in]  featureValues  A vector of feature values
   *    * @return     MVA classifier value    */
  double getClassifierValues( LHCb::span<const double> featureValues ) const;

private:
  mutable std::mutex           m_mut;
  mutable std::array<float, 2> m_predictionsCache = {0, 0};
  mutable DMatrixHandle        m_cache_matrix, m_feature_matrix;
  mutable BoosterHandle        m_booster;
};

#endif // XGBCLASSIFIER_H
