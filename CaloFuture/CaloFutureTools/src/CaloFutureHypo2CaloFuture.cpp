/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from Gaudi
#include "GaudiKernel/SystemOfUnits.h"
// from LHCb
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "GaudiKernel/Plane3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/LineTypes.h"
// local
#include "CaloFutureHypo2CaloFuture.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloFutureHypo2CaloFuture
//
// 2008-09-11 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( CaloFutureHypo2CaloFuture )

//=============================================================================
StatusCode CaloFutureHypo2CaloFuture::initialize() {
  StatusCode sc = CaloFuture2CaloFuture::initialize();
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "Initialize CaloFutureHypo2CaloFuture tool " << endmsg;
  m_lineID = LHCb::CaloCellID(); // initialize
  m_point  = Gaudi::XYZPoint();
  return sc;
}

//=============================================================================
const std::vector<LHCb::CaloCellID>& CaloFutureHypo2CaloFuture::cellIDs( const LHCb::CaloHypo&   fromHypo,
                                                                         CaloCellCode::CaloIndex toCalo ) const {

  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
    debug() << "Matching CaloHypo to " << toCalo << " hypo energy = " << fromHypo.e() << endmsg;

  // get the cluster
  const LHCb::CaloCluster* cluster = LHCb::CaloFutureAlgUtils::ClusterFromHypo( &fromHypo );

  if ( !cluster ) {
    Error( "No valid cluster!" ).ignore();
    return m_cells;
  }

  LHCb::CaloCellID seedID   = cluster->seed();
  auto             fromCalo = seedID.calo();
  if ( toCalo != m_toCalo || fromCalo != m_fromCalo ) setCalos( fromCalo, toCalo );
  if ( !m_ok ) return m_cells;
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
    debug() << "Cluster seed " << seedID << " " << m_fromDet->cellCenter( seedID ) << endmsg;

  if ( m_whole ) { return CaloFuture2CaloFuture::cellIDs( *cluster, toCalo ); }

  m_point  = Gaudi::XYZPoint();
  m_lineID = LHCb::CaloCellID();
  if ( m_line && fromHypo.position() != nullptr ) {
    const Gaudi::XYZPoint  ref( fromHypo.position()->x(), fromHypo.position()->y(), fromHypo.position()->z() );
    const Gaudi::XYZVector vec   = ( ref - Gaudi::XYZPoint( 0, 0, 0 ) );
    const Gaudi::Plane3D   plane = m_toPlane;
    Gaudi::Math::XYZLine   line( ref, vec );
    double                 mu;
    Gaudi::Math::intersection<Gaudi::Math::XYZLine, Gaudi::Plane3D>( line, plane, m_point, mu );
    m_lineID = m_toDet->Cell( m_point );
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "Matching cell " << m_lineID << endmsg;
  }
  return cellIDs( *cluster, toCalo );
}

const std::vector<LHCb::CaloCellID>& CaloFutureHypo2CaloFuture::cellIDs( const LHCb::CaloCluster& fromCluster,
                                                                         CaloCellCode::CaloIndex  toCalo ) const {
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << " toCalo " << toCalo << endmsg;
  reset();
  LHCb::CaloCellID seedID   = fromCluster.seed();
  auto             fromCalo = seedID.calo();
  if ( toCalo != m_toCalo || fromCalo != m_fromCalo ) setCalos( fromCalo, toCalo );
  if ( !m_ok ) return m_cells;

  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
    debug() << "-----  cluster energy " << fromCluster.e() << " " << seedID << endmsg;
  m_neighbour.setDet( m_fromDet );

  // get data
  const_cast<CaloFutureHypo2CaloFuture*>( this )->m_digs = getIfExists<LHCb::CaloDigits>( m_toLoc );

  // matching cluster
  const LHCb::CaloCluster::Entries& entries = fromCluster.entries();
  for ( const LHCb::CaloClusterEntry& entry : entries ) {
    LHCb::CaloCellID cellID = entry.digit()->cellID();
    if ( !( m_seed && ( LHCb::CaloDigitStatus::SeedCell & entry.status() ) != 0 ) &&
         !( m_seed && m_neighb && m_neighbour( seedID, cellID ) != 0. ) && !( ( m_status & entry.status() ) != 0 ) &&
         !( m_whole ) )
      continue;
    SmartRef<LHCb::CaloDigit> digit = entry.digit();
    CaloFuture2CaloFuture::cellIDs( digit->cellID(), toCalo, false );
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
      debug() << toCalo << ":  digit is selected in front of the cluster : " << cellID << "/" << seedID << " "
              << m_digits.size() << endmsg;
  }
  // photon line
  if ( m_line ) {
    if ( m_lineID == LHCb::CaloCellID() ) {
      const Gaudi::XYZPoint  ref( fromCluster.position().x(), fromCluster.position().y(), fromCluster.position().z() );
      const Gaudi::XYZVector vec   = ( ref - Gaudi::XYZPoint( 0, 0, 0 ) );
      const Gaudi::Plane3D   plane = m_toPlane;
      Gaudi::Math::XYZLine   line( ref, vec );
      double                 mu;
      Gaudi::Math::intersection<Gaudi::Math::XYZLine, Gaudi::Plane3D>( line, plane, m_point, mu );
      m_lineID = m_toDet->Cell( m_point );
    }
    if ( !( m_lineID == LHCb::CaloCellID() ) ) {
      addCell( m_lineID, toCalo );
      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
        debug() << toCalo << " : digit is selected in the photon line : " << m_lineID << "/" << seedID << " "
                << m_digits.size() << endmsg;
      if ( m_neighb ) {
        const std::vector<LHCb::CaloCellID>& neighbors = m_toDet->neighborCells( m_lineID );
        for ( auto n = neighbors.begin(); n != neighbors.end(); n++ ) {
          double                halfCell   = m_toDet->cellSize( *n ) * 0.5;
          const Gaudi::XYZPoint cellCenter = m_toDet->cellCenter( *n );
          if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
            debug() << *n << " Point : (" << m_point.X() << "," << m_point.Y() << " Cell :  ( " << cellCenter.X() << ","
                    << cellCenter.Y() << " size/2  : " << halfCell << " Tolerance : " << m_x << "/" << m_y << endmsg;
          if ( fabs( m_point.X() - cellCenter.X() ) < ( halfCell + m_x ) &&
               fabs( m_point.Y() - cellCenter.Y() ) < ( halfCell + m_y ) ) {
            addCell( *n, toCalo );
            if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
              debug() << toCalo << " : digit is selected in the photon line neighborhood : " << *n << "/" << seedID
                      << " " << m_digits.size() << endmsg;
          }
        }
      }
    }
  }

  m_lineID = LHCb::CaloCellID(); // reset
  return m_cells;
}

const std::vector<LHCb::CaloDigit*>& CaloFutureHypo2CaloFuture::digits( const LHCb::CaloHypo&   fromHypo,
                                                                        CaloCellCode::CaloIndex toCalo ) const {
  cellIDs( fromHypo, toCalo );
  return m_digits;
}

double CaloFutureHypo2CaloFuture::energy( const LHCb::CaloHypo& fromHypo, CaloCellCode::CaloIndex toCalo ) const {
  cellIDs( fromHypo, toCalo );
  return m_energy;
}
const std::vector<LHCb::CaloDigit*>& CaloFutureHypo2CaloFuture::digits( const LHCb::CaloCluster& fromCluster,
                                                                        CaloCellCode::CaloIndex  toCalo ) const {
  cellIDs( fromCluster, toCalo );
  return m_digits;
}

double CaloFutureHypo2CaloFuture::energy( const LHCb::CaloCluster& fromCluster, CaloCellCode::CaloIndex toCalo ) const {
  cellIDs( fromCluster, toCalo );
  return m_energy;
}

int CaloFutureHypo2CaloFuture::multiplicity( const LHCb::CaloCluster& fromCluster,
                                             CaloCellCode::CaloIndex  toCalo ) const {
  cellIDs( fromCluster, toCalo );
  return m_count;
}
int CaloFutureHypo2CaloFuture::multiplicity( const LHCb::CaloHypo& fromHypo, CaloCellCode::CaloIndex toCalo ) const {
  cellIDs( fromHypo, toCalo );
  return m_count;
}
