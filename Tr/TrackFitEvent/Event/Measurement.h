/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Include files
#include "Event/TrackTypes.h"
#include "Kernel/BrokenLineTrajectory.h"
#include "Kernel/CircleTraj.h"
#include "Kernel/LHCbID.h"
#include "Kernel/LineTraj.h"
#include "Kernel/PolymorphicValue.h"
#include "Kernel/STLExtensions.h"
#include "Kernel/Trajectory.h"
#include "Kernel/meta_enum.h"
#include <variant>

class DetectorElement;

// Velo
#include "Event/VeloCluster.h"
#include "Event/VeloLiteCluster.h"
class DeVeloPhiType;
class DeVeloRType;

// VP
#include "Event/VPLightCluster.h"
class DeVPSensor;

// ST
#include "Event/STCluster.h"
#include "Event/STLiteCluster.h"
class DeSTSector;

// UT
#include "Event/UTHit.h"
class DeUTSector;

// OT
#include "Event/OTLiteTime.h"
class DeOTModule;

// FT
#include "Event/FTLiteCluster.h"
class DeFTMat;

// Muon
class DeMuonChamber;

namespace LHCb {
  /** @class Measurement Measurement.h
   *
   * Measurement defines the functionality required for the
   * Kalman Filter, Alignment and Monitoring
   *
   * @author Jose Hernando, Eduardo Rodrigues
   * created Fri Jan  4 20:26:46 2019
   *
   */
  namespace Enum::Measurement {
    /// enumerator for the type of Measurement
    meta_enum_class( Type, unsigned char, Unknown, VeloR, VeloPhi, VeloLiteR, VeloLitePhi, TT, IT, OT, Muon, TTLite,
                     ITLite, VP, Calo, Origin, FT, UT = 19, UTLite )
  } // namespace Enum::Measurement
  namespace Enum::OT {
    /// Enum for strategy of using drifttime
    meta_enum_class( DriftTimeStrategy, unsigned char, DriftTimeIgnored, FitDistance, FitTime, PreFit, Unknown )
  } // namespace Enum::OT

  namespace details::Measurement {
    template <typename... T>
    struct overloaded : T... {
      using T::operator()...;
    };
    template <typename... T>
    overloaded( T... )->overloaded<T...>;

    template <typename Dir>
    bool isX( const Dir& dir ) {
      return dir.x() > dir.y();
    }

    template <typename T, typename Variant, std::size_t... I>
    constexpr int index_helper( std::index_sequence<I...> ) {
      auto b = std::array{std::is_same_v<T, std::variant_alternative_t<I, Variant>>...};
      for ( int i = 0; i != size( b ); ++i )
        if ( b[i] ) return i;
      return -1;
    }

    template <typename T, typename Variant>
    constexpr int index() {
      constexpr auto idx = index_helper<T, Variant>( std::make_index_sequence<std::variant_size_v<Variant>>() );
      static_assert( idx != -1, "T does not appear in Variant" );
      return idx;
    }

  } // namespace details::Measurement

  class Measurement final {
  public:
    struct FT {
      LHCb::LineTraj<double> trajectory;
      const DeFTMat*         mat = nullptr;
    };
    struct Muon {
      LHCb::LineTraj<double> trajectory;
      const DeMuonChamber*   chamber = nullptr;
    };
    struct OT {
      OTLiteTime ottime;           ///< channel and calibrated time ( 4 + 8 bytes!)
      int        ambiguity    = 0; ///< the ambiguity (+1/-1) of the measurement
      using DriftTimeStrategy = Enum::OT::DriftTimeStrategy;
      DriftTimeStrategy driftTimeStrategy =
          DriftTimeStrategy::FitTime; ///< flag that tells how drifttime was used by projector
      double deltaTimeOfFlight = 0;   ///< time of flight correction

      cxx::PolymorphicValue<Trajectory<double>> trajectory; ///< the trajectory representing the measurement (owner)
      const DeOTModule*                         module = nullptr;

      double timeOfFlight() const;
      void   setTimeOfFlight( double tof );
      double driftTime( double arclen ) const;
      double driftTimeFromY( double globalY ) const;
      double propagationTimeFromY( double globalY ) const;

      struct ValueWithError {
        float val = 0;
        float err = 0;
      };
      ValueWithError driftRadiusWithError( double arclen ) const;
      ValueWithError driftRadiusWithErrorFromY( double globalY ) const;
    };
    struct TT {};
    struct IT {};
    struct ST {
      LHCb::BrokenLineTrajectory trajectory;
      const DeSTSector*          sector = nullptr;
    };
    struct STLite : ST {
      STLiteCluster cluster;
    };
    struct STFull : ST {
      const STCluster* cluster = nullptr;
      unsigned int     size    = -1;
    };
    struct UT {
      LHCb::LineTraj<double> trajectory;
      const DeUTSector*      sector = nullptr;
    };
    struct UTLite : UT {};
    struct Velo {};
    struct VeloLite : Velo {
      VeloLiteCluster cluster;
    };
    struct VeloFull : Velo {
      const VeloCluster* cluster = nullptr;
    };
    struct VeloPhi {
      LHCb::LineTraj<double> trajectory;
      const DeVeloPhiType*   sensor = nullptr;
      double                 resolution( const Gaudi::XYZPoint& p, double errMeasure ) const;
    };
    struct VeloR {
      LHCb::CircleTraj   trajectory;
      const DeVeloRType* sensor = nullptr;
    };

    struct VeloRFull : VeloR, VeloFull {};
    struct VeloRLite : VeloR, VeloLite {};
    struct VeloPhiFull : VeloPhi, VeloFull {};
    struct VeloPhiLite : VeloPhi, VeloLite {};
    struct TTLite : TT, STLite {};
    struct ITLite : IT, STLite {};
    struct TTFull : TT, STFull {};
    struct ITFull : IT, STFull {};
    struct VP {
      /// x or y measurement
      enum class Projection { X = 1, Y = 2 };
      LHCb::LineTraj<double> trajectory;
      const DeVPSensor*      sensor = nullptr;
      Projection             projection() const {
        return details::Measurement::isX( trajectory.direction( 0 ) )
                   ? VP::Projection::Y // measurement is perpendicular to direction
                   : VP::Projection::X;
      }
#if defined( __GNUC__ ) && __GNUC__ < 8
      VP() noexcept {} // first element in variant, make default constructible..
      VP( LHCb::LineTraj<double> traj, const DeVPSensor* s ) noexcept : trajectory{std::move( traj )}, sensor( s ) {}
#endif
    };

  private:
    // Note: for now, use a variant. Alternatively, store this information 'elsewhere,
    //       and put a "pointer" (which could be "container + index" on top of SOA storage)
    //       to 'elsewhere' into the variant instead.
    using SubInfo = std::variant<VP, VeloPhiLite, VeloPhiFull, VeloRLite, VeloRFull, UTLite, TTLite, TTFull, FT, ITLite,
                                 ITFull, OT, Muon>;

    double  m_z;          ///< the z-position of the measurement
    double  m_errMeasure; ///< the measurement error
    LHCbID  m_lhcbID;     ///< the corresponding LHCbID
    SubInfo m_sub;        ///< subdetector specific information

    template <typename SubI, typename = std::enable_if_t<std::is_convertible_v<SubI, SubInfo>>>
    Measurement( LHCbID id, double z, double errMeas, SubI&& subi )
        : m_z{z}, m_errMeasure{errMeas}, m_lhcbID{id}, m_sub{std::forward<SubI>( subi )} {}

  public:
    /// VP constructor
    Measurement( LHCbID id, double z, LHCb::LineTraj<double> traj, double errMeas, const DeVPSensor* elem )
        : Measurement{id, z, errMeas, VP{std::move( traj ), elem}} {}

    // VeloLitePhi constructor
    Measurement( double z, LHCb::LineTraj<double> traj, double errMeas, const DeVeloPhiType* elem,
                 VeloLiteCluster cluster )
        : Measurement{cluster.channelID(), z, errMeas, VeloPhiLite{{std::move( traj ), elem}, {{}, cluster}}} {}

    // VeloFullPhi constructor
    Measurement( double z, LHCb::LineTraj<double> traj, double errMeas, const DeVeloPhiType* elem,
                 const VeloCluster* cluster )
        : Measurement{cluster->channelID(), z, errMeas, VeloPhiFull{{std::move( traj ), elem}, {{}, cluster}}} {}

    // VeloLiteR constructor
    Measurement( double z, LHCb::CircleTraj traj, double errMeas, const DeVeloRType* elem, VeloLiteCluster cluster )
        : Measurement{cluster.channelID(), z, errMeas, VeloRLite{{std::move( traj ), elem}, {{}, cluster}}} {}

    // VeloFullR constructor
    Measurement( double z, LHCb::CircleTraj traj, double errMeas, const DeVeloRType* elem, const VeloCluster* cluster )
        : Measurement{cluster->channelID(), z, errMeas, VeloRFull{{std::move( traj ), elem}, {{}, cluster}}} {}

    /// UTLite constructor
    Measurement( LHCbID id, double z, LHCb::LineTraj<double> traj, double errMeas, const DeUTSector* elem )
        : Measurement{id, z, errMeas, UTLite{{std::move( traj ), elem}}} {}

    /// STLite constructor (IT / TT)
    Measurement( double z, LHCb::BrokenLineTrajectory traj, double errMeas, const DeSTSector* elem,
                 STLiteCluster cluster )
        : Measurement{cluster.channelID(), z, errMeas,
                      cluster.isTT() ? SubInfo{TTLite{{}, {{std::move( traj ), elem}, {cluster}}}}
                                     : SubInfo{ITLite{{}, {{std::move( traj ), elem}, {cluster}}}}} {}

    /// STFull constructor (IT / TT)
    Measurement( double z, LHCb::BrokenLineTrajectory traj, double errMeas, const DeSTSector* elem,
                 const STCluster* cluster, unsigned int clusterSize )
        : Measurement{cluster->channelID(), z, errMeas,
                      cluster->isTT() ? SubInfo{TTFull{{}, {{std::move( traj ), elem}, cluster, clusterSize}}}
                                      : SubInfo{ITFull{{}, {{std::move( traj ), elem}, cluster, clusterSize}}}} {}

    /// FT constructor
    Measurement( LHCbID id, double z, LHCb::LineTraj<double> traj, double errMeas, const DeFTMat* elem )
        : Measurement{id, z, errMeas, FT{std::move( traj ), elem}} {}

    /// OT constructor
    Measurement( OTLiteTime time, std::unique_ptr<Trajectory<double>> traj, const DeOTModule& mod,
                 short int ambiguity = 0 )
        : Measurement{time.channel(), traj->position( 0.5 * ( traj->beginRange() + traj->endRange() ) ).z(), 0.,
                      OT{time, ambiguity, OT::DriftTimeStrategy::FitTime, 0., cxx::PolymorphicValue{std::move( traj )},
                         &mod}} {}

    /// Muon constructor
    Measurement( LHCbID id, double z, LHCb::LineTraj<double> traj, double errMeas, const DeMuonChamber* elem )
        : Measurement{id, z, errMeas, Muon{std::move( traj ), elem}} {}

    // Observers

    /// invoke the callable which can be called for the detector-specific content in this measurement
    template <typename... Callables>
    decltype( auto ) visit( Callables&&... c ) const {
      return std::visit( details::Measurement::overloaded{std::forward<Callables>( c )...}, m_sub );
    }

    /// Get the sub-detector specific information
    template <typename SubI>
    const SubI* getIf() const;

    /// Check whether this Measurements 'is-a' specific 'sub' measurement
    template <typename SubI>
    bool is() const {
      return std::holds_alternative<SubI>( m_sub );
    }

    /// Retrieve the measurement error
    double resolution( const Gaudi::XYZPoint& point, const Gaudi::XYZVector& /*vec*/ ) const {
      return visit( [&]( const auto& v ) {
        if constexpr ( std::is_base_of_v<VeloPhi, std::decay_t<decltype( v )>> ) {
          return v.resolution( point, errMeasure() );
        } else {
          return errMeasure();
        }
      } );
    }

    /// Retrieve the measurement error squared
    double resolution2( const Gaudi::XYZPoint& point, const Gaudi::XYZVector& vec ) const {
      auto r = resolution( point, vec );
      return r * r;
    }

    /// Retrieve the trajectory representing the measurement
    Trajectory<double> const& trajectory() const {
      return visit( []( const OT& ot ) -> Trajectory<double> const& { return *ot.trajectory; },
                    []( const auto& sub ) -> Trajectory<double> const& { return sub.trajectory; } );
    }

    /// Retrieve const  the corresponding LHCbID
    LHCbID lhcbID() const { return m_lhcbID; }

    /// Retrieve const  the z-position of the measurement
    double z() const { return m_z; }

    /// Retrieve const  the measurement error
    double errMeasure() const { return m_errMeasure; }

    /// Retrieve the corresponding detectorElement
    const DetectorElement* detectorElement() const;

    using Type = Enum::Measurement::Type;

    template <typename SubI>
    static constexpr auto index = details::Measurement::index<SubI, SubInfo>();

    /// Retrieve the measurement type
    Type type() const {
      auto idx = m_sub.index();
      ASSUME( idx < std::variant_size_v<SubInfo> );
      switch ( idx ) {
      case index<VP>:
        return Type::VP;
      case index<VeloPhiLite>:
        return Type::VeloLitePhi;
      case index<VeloPhiFull>:
        return Type::VeloPhi;
      case index<VeloRLite>:
        return Type::VeloLiteR;
      case index<VeloRFull>:
        return Type::VeloR;
      case index<UTLite>:
        return Type::UTLite;
      case index<TTLite>:
        return Type::TTLite;
      case index<TTFull>:
        return Type::TT;
      case index<FT>:
        return Type::FT;
      case index<ITLite>:
        return Type::ITLite;
      case index<ITFull>:
        return Type::IT;
      case index<OT>:
        return Type::OT;
      case index<Muon>:
        return Type::Muon;
      default:
        return Type::Unknown;
      }
    };

    /// Modifiers

    template <typename SubI>
    SubI* getIf();

  }; //< class Measurement

  /// implementation of templated member functions requiring specialization

  template <typename SubI>
  inline const SubI* Measurement::getIf() const {
    if constexpr ( std::is_same_v<STFull, SubI> ) {
      const STFull* it = getIf<ITFull>();
      return it ? it : getIf<TTFull>();
    } else if constexpr ( std::is_same_v<STLite, SubI> ) {
      const STLite* it = getIf<ITLite>();
      return it ? it : getIf<TTLite>();
    } else if constexpr ( std::is_same_v<VeloFull, SubI> ) {
      const VeloFull* r = getIf<VeloRFull>();
      return r ? r : getIf<VeloPhiFull>();
    } else if constexpr ( std::is_same_v<VeloLite, SubI> ) {
      const VeloLite* r = getIf<VeloRLite>();
      return r ? r : getIf<VeloPhiLite>();
    } else if constexpr ( std::is_same_v<VeloPhi, SubI> ) {
      const VeloPhi* r = getIf<VeloPhiLite>();
      return r ? r : getIf<VeloPhiFull>();
    } else if constexpr ( std::is_same_v<VeloR, SubI> ) {
      const VeloR* r = getIf<VeloRFull>();
      return r ? r : getIf<VeloRLite>();
    } else if constexpr ( std::is_same_v<ST, SubI> ) {
      const ST* r = getIf<STLite>();
      return r ? r : getIf<STFull>();
    } else if constexpr ( std::is_same_v<UT, SubI> ) {
      return getIf<UTLite>();
    } else {
      return std::get_if<SubI>( &m_sub );
    }
  }

  template <typename SubI>
  inline SubI* Measurement::getIf() {
    if constexpr ( std::is_same_v<STFull, SubI> ) {
      STFull* it = getIf<ITFull>();
      return it ? it : getIf<TTFull>();
    } else if constexpr ( std::is_same_v<STLite, SubI> ) {
      STLite* it = getIf<ITLite>();
      return it ? it : getIf<TTLite>();
    } else if constexpr ( std::is_same_v<VeloFull, SubI> ) {
      VeloFull* r = getIf<VeloRFull>();
      return r ? r : getIf<VeloPhiFull>();
    } else if constexpr ( std::is_same_v<VeloLite, SubI> ) {
      VeloLite* r = getIf<VeloRLite>();
      return r ? r : getIf<VeloPhiLite>();
    } else if constexpr ( std::is_same_v<VeloPhi, SubI> ) {
      VeloPhi* r = getIf<VeloPhiLite>();
      return r ? r : getIf<VeloPhiFull>();
    } else if constexpr ( std::is_same_v<VeloR, SubI> ) {
      VeloR* r = getIf<VeloRFull>();
      return r ? r : getIf<VeloRLite>();
    } else if constexpr ( std::is_same_v<ST, SubI> ) {
      ST* r = getIf<STLite>();
      return r ? r : getIf<STFull>();
    } else if constexpr ( std::is_same_v<UT, SubI> ) {
      return getIf<UTLite>();
    } else {
      return std::get_if<SubI>( &m_sub );
    }
  }

  template <>
  inline bool Measurement::is<Measurement::VeloR>() const {
    return is<VeloRFull>() || is<VeloRLite>();
  }
  template <>
  inline bool Measurement::is<Measurement::VeloPhi>() const {
    return is<VeloPhiFull>() || is<VeloPhiLite>();
  }
  template <>
  inline bool Measurement::is<Measurement::VeloLite>() const {
    return is<VeloPhiLite>() || is<VeloRLite>();
  }
  template <>
  inline bool Measurement::is<Measurement::VeloFull>() const {
    return is<VeloPhiFull>() || is<VeloRFull>();
  }
  template <>
  inline bool Measurement::is<Measurement::Velo>() const {
    return is<VeloLite>() || is<VeloFull>();
  }
  template <>
  inline bool Measurement::is<Measurement::STLite>() const {
    return is<TTLite>() || is<ITLite>();
  }
  template <>
  inline bool Measurement::is<Measurement::STFull>() const {
    return is<TTFull>() || is<ITFull>();
  }
  template <>
  inline bool Measurement::is<Measurement::TT>() const {
    return is<TTFull>() || is<TTLite>();
  }
  template <>
  inline bool Measurement::is<Measurement::IT>() const {
    return is<ITFull>() || is<ITLite>();
  }
  template <>
  inline bool Measurement::is<Measurement::UT>() const {
    return is<UTLite>();
  }

} // namespace LHCb
