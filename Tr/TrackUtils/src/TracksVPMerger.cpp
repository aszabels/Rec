/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include <vector>

// Gaudi
#include "GaudiAlg/Transformer.h"

// LHCb
#include "Event/PrVeloTracks.h"

/**
 * Merge two TracksVP containers into one.
 *
 * @author Arthur Hennequin (CERN, LIP6)
 */

class TracksVPMerger : public Gaudi::Functional::Transformer<LHCb::Pr::Velo::Tracks( const LHCb::Pr::Velo::Tracks&,
                                                                                     const LHCb::Pr::Velo::Tracks& )> {
  using Tracks = LHCb::Pr::Velo::Tracks;

public:
  TracksVPMerger( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer(
            name, pSvcLocator,
            {KeyValue{"TracksLocation1", "Rec/Track/VeloBackward"}, KeyValue{"TracksLocation2", "Rec/Track/Velo"}},
            KeyValue{"OutputTracksLocation", "Rec/Track/VeloMerged"} ) {}

  StatusCode initialize() override {
    StatusCode sc = Transformer::initialize();
    if ( sc.isFailure() ) return sc;
    return StatusCode::SUCCESS;
  };

  Tracks operator()( const Tracks& tracks1, const Tracks& tracks2 ) const override {
    Tracks out;

    m_nbTracksCounter += tracks1.size() + tracks2.size();

    using dType = SIMDWrapper::avx2::types;

    for ( int t = 0; t < tracks1.size(); t += dType::size ) {
      auto loop_mask = dType::loop_mask( t, tracks1.size() );
      out.copy_back<dType>( tracks1, t, loop_mask );
    }

    for ( int t = 0; t < tracks2.size(); t += dType::size ) {
      auto loop_mask = dType::loop_mask( t, tracks2.size() );
      out.copy_back<dType>( tracks2, t, loop_mask );
    }

    return out;
  };

private:
  mutable Gaudi::Accumulators::SummingCounter<> m_nbTracksCounter{this, "Nb of Produced Tracks"};
};

DECLARE_COMPONENT( TracksVPMerger )
