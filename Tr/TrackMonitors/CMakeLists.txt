###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: TrackMonitors
################################################################################
gaudi_subdir(TrackMonitors v2r14)

gaudi_depends_on_subdirs(Det/CaloDet
                         Event/MCEvent
                         Event/GenEvent
                         Event/PhysEvent
                         Event/RecEvent
                         Event/TrackEvent
                         Event/HltEvent
                         Event/LinkerEvent
                         GaudiAlg
                         GaudiUtils
                         Kernel/PartProp
                         ST/STKernel
                         ST/STTELL1Event
                         Tf/PatKernel
                         Tr/TrackFitEvent
                         Tr/TrackInterfaces
                         Tr/TrackKernel
                         Tr/TrackVectorFit)

find_package(GSL)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(TrackMonitors
                 src/*.cpp
                 INCLUDE_DIRS GSL AIDA Tr/TrackInterfaces Tr/TrackKernel Tf/PatKernel
                 LINK_LIBRARIES GSL CaloDetLib MCEvent GenEvent PhysEvent RecEvent TrackEvent HltEvent GaudiAlgLib GaudiUtilsLib PartPropLib LinkerEvent STKernelLib STTELL1Event TrackFitEvent TrackKernel)

gaudi_install_python_modules()
