/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/ToolHandle.h"
#include "TrackStateInitTool.h"

/** @class TrackStateInitAlg TrackStateInitAlg.h
 *
 * Algorithm to initialize all tracks in container.
 * Uses the TrackStateInitTool.
 *
 * @author Pavel Krokovny <krokovny@physi.uni-heidelberg.de>
 * @date   2009-03-02
 */

class TrackStateInitAlg : public GaudiAlgorithm {
public:
  TrackStateInitAlg( const std::string& name, ISvcLocator* pSvcLocator );
  StatusCode initialize() override;
  StatusCode finalize() override;
  StatusCode execute() override;

private:
  Gaudi::Property<bool>        clearStates{this, "ClearStates", true};
  Gaudi::Property<std::string> m_trackLocation{this, "TrackLocation", LHCb::TrackLocation::Default};
  ToolHandle<ITrackStateInit>  m_trackTool{"TrackStateInitTool", this};
};
