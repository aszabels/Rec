/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// from GaudiKernel
#include "GaudiKernel/SystemOfUnits.h"

// Event
#include "Event/Measurement.h"
#include "Event/STCluster.h"
#include "Event/Track.h"
#include "Event/VeloCluster.h"
#include "TrackKernel/TrackFunctors.h"

// local
#include "TrackLikelihood.h"

#include "range/v3/algorithm/count_if.hpp"
#include "range/v3/numeric/accumulate.hpp"
#include <algorithm>

#include "Kernel/LHCbID.h"

#include "gsl/gsl_cdf.h"
#include "gsl/gsl_math.h"
#include "gsl/gsl_randist.h"
#include "gsl/gsl_rng.h"

#include "TrackInterfaces/IHitExpectation.h"
#include "TrackInterfaces/IVeloExpectation.h"

using namespace LHCb;
using namespace Gaudi;

namespace {
  double binomialTerm( const unsigned int found, const unsigned int expected, const double eff ) {

    unsigned int n = expected;
    unsigned int r = found;
    if ( r > n ) r = n;
    double fnum = 1.;
    double fden = 1.;
    for ( unsigned int i = 0; i < n - r; ++i ) {
      fnum *= double( n - i );
      fden *= double( i + 1 );
    }

    return log( fnum / fden ) + r * log( eff ) + ( n - r ) * log( 1 - eff );
  }
} // namespace
DECLARE_COMPONENT( TrackLikelihood )

//=============================================================================
//
//=============================================================================

StatusCode TrackLikelihood::initialize() {
  StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;

  if ( m_useVelo ) m_veloExpectation = tool<IVeloExpectation>( "VeloExpectation" );
  if ( m_useTT ) m_ttExpectation = tool<IHitExpectation>( "TTHitExpectation" );
  if ( m_useUT ) m_utExpectation = tool<IHitExpectation>( "UTHitExpectation" );
  if ( m_useIT ) m_itExpectation = tool<IHitExpectation>( "ITHitExpectation" );
  if ( m_useOT ) m_otExpectation = tool<IHitExpectation>( "OTHitExpectation" );

  return StatusCode::SUCCESS;
}

float TrackLikelihood::operator()( const LHCb::Track& aTrack ) const {
  // chi^2 contribution
  double lik = ( aTrack.probChi2() < 1e-50 ? -150.0 : log( aTrack.probChi2() ) / m_chiWeight );

  if ( m_useVelo && aTrack.hasVelo() ) lik += addVelo( aTrack );
  if ( m_useTT && aTrack.hasTT() ) lik += addTT( aTrack );
  if ( m_useUT && aTrack.hasUT() ) lik += addUT( aTrack );

  if ( aTrack.hasT() ) {
    if ( m_useOT ) lik += addOT( aTrack );
    if ( m_useIT ) lik += addIT( aTrack );
  }

  // return the likelihood value
  return lik;
}

double TrackLikelihood::addVelo( const LHCb::Track& aTrack ) const {

  double lik = 0.0;

  // pick up the LHCbIDs + measurements
  // get the number of expected hits in the velo
  auto nVelo = ranges::accumulate( aTrack.lhcbIDs(), std::array<unsigned, 2>{0u, 0u},
                                   []( std::array<unsigned, 2> N, const LHCbID& id ) {
                                     if ( id.isVelo() ) ++N[id.veloID().isRType()];
                                     return N;
                                   } );

  // get the number of hits
  IVeloExpectation::Info expectedVelo = m_veloExpectation->expectedInfo( aTrack );
  lik += binomialTerm( nVelo[1], expectedVelo.nR, m_veloREff );
  lik += binomialTerm( nVelo[0], expectedVelo.nPhi, m_veloPhiEff );

  // loop and count # with high threshold in R and phi
  unsigned int nHigh = 0;
  unsigned int nTot  = 0;
  for ( const LHCb::Measurement& hit : measurements( aTrack ) ) {
    const auto* veloFull = hit.getIf<LHCb::Measurement::VeloFull>();
    const auto* veloLite = hit.getIf<LHCb::Measurement::VeloLite>();
    if ( veloFull || veloLite ) ++nTot;
    if ( ( veloFull && veloFull->cluster->highThreshold() ) || ( veloLite && veloLite->cluster.highThreshold() ) )
      ++nHigh;
  }

  // add term to lik
  double prob1 = gsl_ran_binomial_pdf( nHigh, m_veloHighEff1, nTot );
  double prob2 = gsl_ran_binomial_pdf( nHigh, m_veloHighEff2, nTot );
  lik += log( std::max( prob1, prob2 ) );

  return lik;
}

double TrackLikelihood::addTT( const LHCb::Track& aTrack ) const {

  double lik = 0.0;

  // get the number of expected hits in TT
  const unsigned int nExpectedTT = m_ttExpectation->nExpected( aTrack );
  if ( nExpectedTT > 2 ) {

    // pick up the LHCbIDs + measurements
    const unsigned int nTTHits = ranges::count_if( aTrack.lhcbIDs(), &LHCb::LHCbID::isTT );
    lik += binomialTerm( nTTHits, nExpectedTT, m_ttEff );

    // spillover information for TT
    // loop and count # with high threshold
    unsigned int nHigh = ranges::count_if( measurements( aTrack ), []( const LHCb::Measurement& m ) {
      const auto* ttmeas = m.getIf<LHCb::Measurement::TTFull>();
      return ttmeas && ttmeas->cluster->highThreshold();
    } );

    double spillprob = gsl_ran_binomial_pdf( nHigh, m_ttHighEff, nTTHits );
    lik += log( spillprob );
  }

  return lik;
}

double TrackLikelihood::addUT( const LHCb::Track& aTrack ) const {

  double lik = 0.0;

  // get the number of expected hits in UT
  const unsigned int nExpectedUT = m_utExpectation->nExpected( aTrack );
  if ( nExpectedUT > 2 ) {

    const unsigned int nUTHits = ranges::count_if( aTrack.lhcbIDs(), &LHCb::LHCbID::isUT );
    lik += binomialTerm( nUTHits, nExpectedUT, m_utEff );

    // spillover information for UT
    // loop and count # with high threshold
    unsigned int nHigh = ranges::count_if( measurements( aTrack ), []( const LHCb::Measurement& ) {
#if 0
      //FIXME:  there are no Measurements build from UTFull clusters, and
      // UTLite doesn't provide access to highThreshold...
      const auto* utfull = m.getIf<LHCb::Measurement::UTFull>();
      return utfull && utfull->cluster->highThreshold();
#else
      assert(false); // FIXME
      return false;
#endif
    } );

    double spillprob = gsl_ran_binomial_pdf( nHigh, m_utHighEff, nUTHits );
    lik += log( spillprob );
  }
  return lik;
}

double TrackLikelihood::addOT( const LHCb::Track& aTrack ) const {

  double lik = 0.0;

  // pick up the LHCbIDs
  auto nOT = ranges::count_if( aTrack.lhcbIDs(), &LHCb::LHCbID::isOT );
  if ( nOT > 0u ) {
    IHitExpectation::Info otInfo = m_otExpectation->expectation( aTrack );
    lik += binomialTerm( nOT, otInfo.nExpected, m_otEff );
    lik += otInfo.likelihood;
  }
  return lik;
}

double TrackLikelihood::addIT( const LHCb::Track& aTrack ) const {

  double lik = 0.0;

  // pick up the LHCbIDs + measurements
  // IT
  auto nIT = ranges::count_if( aTrack.lhcbIDs(), &LHCb::LHCbID::isIT );
  if ( nIT > 0u ) {
    unsigned int nITexpec = m_itExpectation->nExpected( aTrack );
    lik += binomialTerm( nIT, nITexpec, m_itEff );

    // spillover information for IT
    // loop and count # with high threshold
    unsigned int nHigh = ranges::count_if( measurements( aTrack ), []( const LHCb::Measurement& m ) {
      auto* it = m.getIf<LHCb::Measurement::ITFull>();
      return it && it->cluster->highThreshold();
    } );

    double spillprob = gsl_ran_binomial_pdf( nHigh, m_itHighEff, nIT );
    lik += log( spillprob );
  }
  return lik;
}
