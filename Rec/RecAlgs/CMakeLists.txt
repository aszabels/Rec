###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: RecAlgs
################################################################################
gaudi_subdir(RecAlgs v3r4)

gaudi_depends_on_subdirs(Event/DAQEvent
                         Event/EventBase
                         Event/RecEvent
                         GaudiAlg
                         GaudiUtils
                         Kernel/LHCbKernel
                         Rich/RichKernel
                         Rich/RichUtils)

find_package(AIDA)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(RecAlgs
                 src/*.cpp
                 INCLUDE_DIRS AIDA Event/EventBase Rich/RichKernel Rich/RichUtils
                 LINK_LIBRARIES DAQEventLib RecEvent GaudiAlgLib GaudiUtilsLib LHCbKernel RichKernelLib RichUtils)

