###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: RecInterfaces
################################################################################
gaudi_subdir(RecInterfaces v1r4)

gaudi_depends_on_subdirs(Event/PhysEvent
                         Event/RecEvent)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_dictionary(RecInterfaces
                     dict/RecInterfacesDict.h
                     dict/RecInterfacesDict.xml
                     LINK_LIBRARIES PhysEvent RecEvent
                     OPTIONS "-U__MINGW32__")

gaudi_install_headers(RecInterfaces)

