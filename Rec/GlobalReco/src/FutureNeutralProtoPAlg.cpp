/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
#include "FutureNeutralProtoPAlg.h"
// ============================================================================
/** @file
 *  Implementation file for class FutureNeutralProtoPAlg
 *  @date 2006-06-09
 *  @author Olivier Deschamps
 */
// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FutureNeutralProtoPAlg )
// ============================================================================
// Standard constructor, initializes variables
// ============================================================================
FutureNeutralProtoPAlg::FutureNeutralProtoPAlg( const std::string& name, ISvcLocator* pSvcLocator )
    : GaudiAlgorithm( name, pSvcLocator ) {
  // declare the properties
  declareProperty( "HyposLocations", m_hyposLocations );
  declareProperty( "ProtoParticleLocation", m_protoLocation );
  declareProperty( "LightMode", m_light_mode = false,
                   "Use 'light' mode and do not collect all information. Useful for Calibration." );

  using namespace LHCb::CaloHypoLocation;
  m_hyposLocations.push_back( MergedPi0s ); // put it 1st to speed-up photon mass retrieval
  m_hyposLocations.push_back( Photons );
  m_hyposLocations.push_back( SplitPhotons );
  //  m_protoLocation =  LHCb::ProtoParticleLocation::Neutrals ;
  m_protoLocation = LHCb::ProtoParticleLocation::Neutrals;
}
// ============================================================================
// Initialization
// ============================================================================
StatusCode FutureNeutralProtoPAlg::initialize() {
  const StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;                    // error printed already by GaudiAlgorithm
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;

  for ( const auto& loc : m_hyposLocations ) { info() << " Hypothesis loaded from " << loc << endmsg; }

  counterStat = tool<IFutureCounterLevel>( "FutureCounterLevel" );

  if ( lightMode() ) info() << "FutureNeutral protoparticles will be created in 'Light' Mode" << endmsg;

  m_estimator =
      tool<LHCb::Calo::Interfaces::IHypoEstimator>( "CaloFutureHypoEstimator", "CaloFutureHypoEstimator", this );
  auto& h2c = dynamic_cast<IProperty&>( *m_estimator->hypo2Calo() );
  h2c.setProperty( "Seed", "false" ).ignore();
  h2c.setProperty( "PhotonLine", "true" ).ignore();
  h2c.setProperty( "AddNeighbors", "false" ).ignore();

  m_mass.clear();
  m_setMass = false;

  return sc;
}

double FutureNeutralProtoPAlg::getMass( const int cellCode ) const {
  if ( !m_setMass ) {
    Warning( "You should process MergedPi0 protoparticles first to speed up retrieval of photon 'mass'",
             StatusCode::SUCCESS, 1 )
        .ignore();
    // === storing masses
    m_setMass         = true;
    const auto* hypos = getIfExists<LHCb::CaloHypos>( LHCb::CaloHypoLocation::MergedPi0s );
    if ( !hypos ) return 0.;
    for ( const auto& hypo : *hypos ) {
      if ( !hypo ) continue;
      using namespace LHCb::Calo::Enum;
      const int cellCode = m_estimator->data( *hypo, DataType::CellID ).value_or( Default );
      m_mass[cellCode]   = m_estimator->data( *hypo, DataType::HypoM ).value_or( Default );
    }
  }
  const auto it = m_mass.find( cellCode );
  return ( it == m_mass.end() ? 0.0 : it->second );
}

// ============================================================================
// Main execution
// ============================================================================
StatusCode FutureNeutralProtoPAlg::execute() {

  // create and register the output container
  LHCb::ProtoParticles* protos = nullptr;
  if ( !lightMode() && exist<LHCb::ProtoParticles>( m_protoLocation ) ) {
    Warning( "Existing ProtoParticle container at " + m_protoLocation + " found -> Will replace", StatusCode::SUCCESS,
             1 )
        .ignore();
    if ( counterStat->isQuiet() ) counter( "Replaced Proto" ) += 1;
    protos = get<LHCb::ProtoParticles>( m_protoLocation );
    protos->clear();
  } else {
    protos = new LHCb::ProtoParticles();
    put( protos, m_protoLocation );
  }

  if ( !protos ) return Warning( "FutureNeutralProto container points to NULL ", StatusCode::SUCCESS );

  // -- reset mass storage
  m_mass.clear();
  m_setMass = false;
  //------ loop over all caloHypo containers
  for ( const auto& loc : m_hyposLocations ) {

    //==  Load the CaloHypo container
    const auto* hypos = getIfExists<LHCb::CaloHypos>( loc );
    if ( !hypos ) {
      if ( msgLevel( MSG::DEBUG ) ) debug() << "No CaloHypo at '" << loc << "'" << endmsg;
      if ( counterStat->isQuiet() ) counter( "No " + loc + " container" ) += 1;
      continue;
    }

    // no cluster mass storage when no MergedPi0
    if ( LHCb::CaloFutureAlgUtils::toUpper( loc ).find( "MERGED" ) != std::string::npos && hypos->empty() ) {
      m_setMass = true;
    }

    if ( msgLevel( MSG::DEBUG ) ) debug() << "CaloHypo loaded at " << loc << " (# " << hypos->size() << ")" << endmsg;
    int count = 0;

    // == Loop over CaloHypos
    for ( const auto* hypo : *hypos ) {
      if ( !hypo ) { continue; }
      count++;

      // == create and store the corresponding ProtoParticle
      auto* proto = new LHCb::ProtoParticle();
      protos->insert( proto );

      // == link CaloHypo to ProtoP
      using namespace LHCb::Calo::Enum;
      proto->addToCalo( hypo );

      // ===== add data to protoparticle
      if ( !lightMode() ) {
        std::ostringstream type( "" );
        type << hypo->hypothesis();
        const auto hypothesis = type.str();
        // extra data :
        pushData( proto, LHCb::ProtoParticle::additionalInfo::CaloNeutralID, *hypo, DataType::CellID ); // seed cellID
        // retrieve HypoM for photon
        const int cellCode = m_estimator->data( *hypo, DataType::CellID ).value_or( Default );
        if ( hypo->hypothesis() == LHCb::CaloHypo::Hypothesis::Photon ) {
          const auto mass = getMass( cellCode );
          if ( mass > 0. ) {
            proto->addInfo( LHCb::ProtoParticle::additionalInfo::ClusterMass, mass );
            if ( counterStat->isVerbose() ) counter( "ClusterMass for Photon" ) += mass;
          }
        } else if ( hypo->hypothesis() == LHCb::CaloHypo::Hypothesis::Pi0Merged ) {
          pushData( proto, LHCb::ProtoParticle::additionalInfo::ClusterMass, *hypo, DataType::HypoM );
          m_setMass        = true;
          m_mass[cellCode] = m_estimator->data( *hypo, DataType::HypoM ).value_or( Default );
        }
        if ( hypo->hypothesis() != LHCb::CaloHypo::Hypothesis::Pi0Merged ) {
          pushData( proto, LHCb::ProtoParticle::additionalInfo::ClusterAsX, *hypo, DataType::ClusterAsX, 0 );
          pushData( proto, LHCb::ProtoParticle::additionalInfo::ClusterAsY, *hypo, DataType::ClusterAsY, 0 );
        }

        pushData( proto, LHCb::ProtoParticle::additionalInfo::CaloNeutralPrs, *hypo, DataType::HypoPrsE );
        pushData( proto, LHCb::ProtoParticle::additionalInfo::CaloNeutralEcal, *hypo, DataType::ClusterE );

        pushData( proto, LHCb::ProtoParticle::additionalInfo::CaloTrMatch, *hypo, DataType::ClusterMatch,
                  +1.e+06 ); // ** input to neutralID
        pushData( proto, LHCb::ProtoParticle::additionalInfo::ShowerShape, *hypo,
                  DataType::Spread ); // ** input to neutralID  && isPhoton (as Fr2)
        pushData( proto, LHCb::ProtoParticle::additionalInfo::CaloNeutralSpd, *hypo,
                  DataType::HypoSpdM ); // ** input to neutralID
        pushData( proto, LHCb::ProtoParticle::additionalInfo::CaloNeutralHcal2Ecal, *hypo,
                  DataType::Hcal2Ecal ); // ** input to neutralID
        pushData( proto, LHCb::ProtoParticle::additionalInfo::CaloClusterCode, *hypo, DataType::ClusterCode );
        pushData( proto, LHCb::ProtoParticle::additionalInfo::CaloClusterFrac, *hypo, DataType::ClusterFrac, 1 );
        pushData( proto, LHCb::ProtoParticle::additionalInfo::Saturation, *hypo, DataType::Saturation, 0 );

        auto dep = ( m_estimator->data( *hypo, DataType::ToSpdM ) > 0 ) ? -1. : +1.;
        dep *= m_estimator->data( *hypo, DataType::ToPrsE ).value_or( Default );
        proto->addInfo( LHCb::ProtoParticle::additionalInfo::CaloDepositID,
                        dep ); // ** input to neutralID toPrsE=|caloDepositID|

        // DLL-based neutralID (to be obsolete)  :
        pushData( proto, LHCb::ProtoParticle::additionalInfo::PhotonID, *hypo, DataType::NeutralID, -1.,
                  true ); // old DLL-based neutral-ID // FORCE

        // isNotX  inputs :
        pushData( proto, LHCb::ProtoParticle::additionalInfo::CaloNeutralE49, *hypo, DataType::E49 );
        pushData( proto, LHCb::ProtoParticle::additionalInfo::CaloNeutralE19, *hypo,
                  DataType::E19 ); // ** input to neutralID
        pushData( proto, LHCb::ProtoParticle::additionalInfo::CaloPrsNeutralE49, *hypo,
                  DataType::PrsE49 ); // ** input to neutralID
        pushData( proto, LHCb::ProtoParticle::additionalInfo::CaloPrsNeutralE19, *hypo,
                  DataType::PrsE19 ); // ** input to neutralID
        pushData( proto, LHCb::ProtoParticle::additionalInfo::CaloPrsNeutralE4max, *hypo,
                  DataType::PrsE4Max ); // ** input to neutralID
        pushData( proto, LHCb::ProtoParticle::additionalInfo::CaloNeutralPrsM, *hypo,
                  DataType::HypoPrsM ); // ** input to neutralID
        // isNotX output :
        pushData( proto, LHCb::ProtoParticle::additionalInfo::IsNotH, *hypo, DataType::isNotH, -1.,
                  true ); // new NN-based neutral-ID (anti-H) // FORCE
        pushData( proto, LHCb::ProtoParticle::additionalInfo::IsNotE, *hypo, DataType::isNotE, -1.,
                  true ); // new NN-based neutral-ID (anti-E) // FORCE

        // isPhoton inputs (photon & mergedPi0 only)
        if ( hypo->hypothesis() != LHCb::CaloHypo::Hypothesis::PhotonFromMergedPi0 ) {
          pushData( proto, LHCb::ProtoParticle::additionalInfo::CaloShapeFr2r4, *hypo,
                    DataType::isPhotonFr2r4 ); // -- input to isPhoton
          pushData( proto, LHCb::ProtoParticle::additionalInfo::CaloShapeAsym, *hypo,
                    DataType::isPhotonAsym ); // -- input to isPhoton
          pushData( proto, LHCb::ProtoParticle::additionalInfo::CaloShapeKappa, *hypo,
                    DataType::isPhotonKappa ); // -- input to isPhoton
          pushData( proto, LHCb::ProtoParticle::additionalInfo::CaloShapeE1, *hypo,
                    DataType::isPhotonEseed ); // -- input to isPhoton
          pushData( proto, LHCb::ProtoParticle::additionalInfo::CaloShapeE2, *hypo,
                    DataType::isPhotonE2 ); // -- input to isPhoton
          pushData( proto, LHCb::ProtoParticle::additionalInfo::CaloPrsShapeE2, *hypo,
                    DataType::isPhotonPrsE2 ); // -- input to isPhoton
          pushData( proto, LHCb::ProtoParticle::additionalInfo::CaloPrsShapeEmax, *hypo,
                    DataType::isPhotonPrsEmax ); // -- input to isPhoton
          pushData( proto, LHCb::ProtoParticle::additionalInfo::CaloPrsShapeFr2, *hypo,
                    DataType::isPhotonPrsFr2 ); // -- input to isPhoton
          pushData( proto, LHCb::ProtoParticle::additionalInfo::CaloPrsShapeAsym, *hypo,
                    DataType::isPhotonPrsAsym ); // -- input to isPhoton
          pushData( proto, LHCb::ProtoParticle::additionalInfo::CaloPrsM, *hypo,
                    DataType::isPhotonPrsM ); // -- input to isPhoton
          pushData( proto, LHCb::ProtoParticle::additionalInfo::CaloPrsM15, *hypo,
                    DataType::isPhotonPrsM15 ); // -- input to isPhoton
          pushData( proto, LHCb::ProtoParticle::additionalInfo::CaloPrsM30, *hypo,
                    DataType::isPhotonPrsM30 ); // -- input to isPhoton
          pushData( proto, LHCb::ProtoParticle::additionalInfo::CaloPrsM45, *hypo,
                    DataType::isPhotonPrsM45 ); // -- input to isPhoton
          // isPhoton output :
          pushData( proto, LHCb::ProtoParticle::additionalInfo::IsPhoton, *hypo, DataType::isPhoton, +1.,
                    true ); // NN-based neutral-ID (anti-pi0) // FORCE to +1 when missing (i.e. PT < 2 GeV)
          pushData( proto, LHCb::ProtoParticle::additionalInfo::IsPhotonXGB, *hypo, DataType::isPhotonXGB, +1.,
                    true ); // XGBoost-based neutral-ID (anti-pi0) // FORCE to +1 when missing (i.e. PT < 2 GeV)
        }
      } // lightmode
    }   // loop over CaloHypos
    if ( counterStat->isQuiet() ) counter( loc + "=>" + m_protoLocation ) += count;
  } // loop over HyposLocations

  if ( msgLevel( MSG::DEBUG ) ) debug() << "# Future Neutral ProtoParticles created : " << protos->size() << endmsg;
  if ( counterStat->isQuiet() ) counter( "#protos in " + m_protoLocation ) += protos->size();
  return StatusCode::SUCCESS;
}
// ============================================================================
//  Finalize
// ============================================================================

StatusCode FutureNeutralProtoPAlg::finalize() {
  //
  if ( lightMode() ) { info() << "Future Neutral protoparticles have been created in 'Light' Mode" << endmsg; }

  return GaudiAlgorithm::finalize(); // must be called after all other actions
}
