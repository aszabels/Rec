/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/IMessageSvc.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "GaudiKernel/ToStream.h"
// ============================================================================
// GaudiAlg
// ============================================================================
#include "GaudiAlg/GetData.h"
// ============================================================================
// Event
// ============================================================================
#include "Event/Track.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/Algs.h"
#include "LoKi/Services.h"
#include "LoKi/TrSources.h"
#include "LoKi/apply.h"
// ============================================================================
/** @file
 *  Implementation file for various sources
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyav@physics.syr.edu
 *  @date 2006-12-07
 */
// ============================================================================
// constructor from the service, TES location and cuts
// ============================================================================
LoKi::Track::SourceTES::SourceTES( const IDataProviderSvc* svc, const std::string& path )
    : LoKi::AuxFunBase( std::tie( svc, path ) )
    , SourceTES::_Base( svc, path )
    , m_cut( LoKi::BasicFunctors<const LHCb::Track*>::BooleanConstant( true ) ) {}
// ============================================================================
// constructor from the service, TES location and cuts
// ============================================================================
LoKi::Track::SourceTES::SourceTES( const IDataProviderSvc* svc, const std::string& path,
                                   const LoKi::TrackTypes::TrCuts& cuts )
    : LoKi::AuxFunBase( std::tie( svc, path, cuts ) ), SourceTES::_Base( svc, path ), m_cut( cuts ) {}
// ============================================================================
// constructor from the service, TES location and cuts
// ============================================================================
LoKi::Track::SourceTES::SourceTES( const GaudiAlgorithm* svc, const std::string& path, const bool useRootInTES )
    : LoKi::AuxFunBase( std::tie( svc, path, useRootInTES ) )
    , SourceTES::_Base( svc, path, useRootInTES )
    , m_cut( LoKi::BasicFunctors<const LHCb::Track*>::BooleanConstant( true ) ) {}
// ============================================================================
// constructor from the service, TES location and cuts
// ============================================================================
LoKi::Track::SourceTES::SourceTES( const GaudiAlgorithm* svc, const std::string& path,
                                   const LoKi::TrackTypes::TrCuts& cuts, const bool useRootInTES )
    : LoKi::AuxFunBase( std::tie( svc, path, cuts, useRootInTES ) )
    , SourceTES::_Base( svc, path, useRootInTES )
    , m_cut( cuts ) {}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Track::SourceTES* LoKi::Track::SourceTES::clone() const { return new SourceTES( *this ); }
// ============================================================================
// MANDATORY: the only essential method:
// ============================================================================
LoKi::Track::SourceTES::result_type LoKi::Track::SourceTES::operator()() const {
  LHCb::Track::Range       r = get();
  LHCb::Track::ConstVector o;
  o.reserve( r.size() );
  LoKi::apply_filter( r.begin(), r.end(), m_cut.func(), std::back_inserter( o ) );
  return o;
}
// ======================================================================
// get the particles from TES
// ======================================================================
LHCb::Track::Range LoKi::Track::SourceTES::get( const bool exc ) const {
  if ( algorithm() ) {
    // ========================================================================
    typedef LHCb::Track::Range RANGE;
    const bool                 ok = algorithm()->exist<RANGE>( location(), useRootInTES() );
    if ( !ok ) {
      if ( exc ) { Exception( "No valid data is found at location '" + location() + "'" ); }
      Error( "No valid data is found at location '" + location() + "'" );
      return LHCb::Track::Range();
    }
    return algorithm()->get<RANGE>( location(), useRootInTES() );
    // ========================================================================
  } else if ( datasvc() ) {
    // ========================================================================
    Gaudi::Utils::GetData<LHCb::Track::Range> data;
    //
    SmartDataPtr<LHCb::Track::Selection> tracks_1( datasvc(), location() );
    if ( !( !tracks_1 ) ) { return data.make_range( tracks_1 ); }
    //
    SmartDataPtr<LHCb::Track::Container> tracks_2( datasvc(), location() );
    if ( !( !tracks_2 ) ) { return data.make_range( tracks_2 ); }
    //
    if ( exc ) {
      Exception( "No valid data is found at location '" + location() + "'" );
    } else {
      Error( "No valid data is found at location '" + location() + "'" );
    }
    //
    return LHCb::Track::Range();
  }
  Exception( "Invalid configuration!" );
  return LHCb::Track::Range();
}
// ======================================================================
// count the particles from TES
// ======================================================================
std::size_t LoKi::Track::SourceTES::count( const bool exc ) const {
  LHCb::Track::Range r = get( exc );
  return LoKi::Algs::count_if( r.begin(), r.end(), std::cref( m_cut ) );
}
// ============================================================================
// OPTIONAL: the nice printout
// ============================================================================
std::ostream& LoKi::Track::SourceTES::fillStream( std::ostream& o ) const {
  return o << "TrSOURCE(" << Gaudi::Utils::toString( path() ) << "," << m_cut << ")";
}
// ============================================================================

// ============================================================================
// constructor
// ============================================================================
LoKi::Track::TESData::TESData( const GaudiAlgorithm* algorithm, const std::string& location )
    : LoKi::AuxFunBase( std::tie( algorithm, location ) )
    , LoKi::BasicFunctors<const LHCb::Track*>::Source()
    , LoKi::TES::DataHandle<Range_>( algorithm, location )
    , m_cuts( LoKi::BasicFunctors<const LHCb::Track*>::BooleanConstant( true ) ) {}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::TESData::TESData( const GaudiAlgorithm* algorithm, const std::string& location,
                               const LoKi::TrackTypes::TrCuts& cuts )
    : LoKi::AuxFunBase( std::tie( algorithm, location, cuts ) )
    , LoKi::BasicFunctors<const LHCb::Track*>::Source()
    , LoKi::TES::DataHandle<Range_>( algorithm, location )
    , m_cuts( cuts ) {}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Track::TESData* LoKi::Track::TESData::clone() const { return new TESData( *this ); }
// ============================================================================
// MANDATORY: the only essential method:
// ============================================================================
LoKi::Track::TESData::result_type LoKi::Track::TESData::operator()() const {
  const auto                        range = get();
  LoKi::Track::TESData::result_type result;
  result.reserve( range.size() );
  std::copy_if( range.begin(), range.end(), std::back_inserter( result ), std::cref( m_cuts ) );
  return result;
}
// ============================================================================
// OPTIONAL: the nice printout
// ============================================================================
std::ostream& LoKi::Track::TESData::fillStream( std::ostream& o ) const {
  return o << "TrTESDATA('" << location() << "'," << m_cuts << ")";
}

// ============================================================================
// constructor from the service, TES location and cuts
// ============================================================================
LoKi::Track::TESCounter::TESCounter( const LoKi::BasicFunctors<const LHCb::Track*>::Source& s )
    : LoKi::AuxFunBase( std::tie( s ) ), LoKi::Functor<void, double>(), m_source( s ) {}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Track::TESCounter* LoKi::Track::TESCounter::clone() const { return new LoKi::Track::TESCounter( *this ); }
// ============================================================================
// MANDATORY: the only essential method:
// ============================================================================
double LoKi::Track::TESCounter::operator()() const { return m_source().size(); }
// ============================================================================
// OPTIONAL: the nice printout
// ============================================================================
std::ostream& LoKi::Track::TESCounter::fillStream( std::ostream& o ) const { return o << "TrNUM(" << m_source << ")"; }
// ============================================================================

// ============================================================================
// The END
// ============================================================================
