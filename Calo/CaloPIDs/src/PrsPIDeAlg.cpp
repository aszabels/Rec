/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloID2DLL.h"

// ============================================================================
/** @class PrsPIDeAlg  PrsPIDeAlg.cpp
 *  The preconfigured instance of class CaloID2DLL
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-06-18
 */
// ============================================================================

class PrsPIDeAlg final : public CaloID2DLL {
public:
  PrsPIDeAlg( const std::string& name, ISvcLocator* pSvc ) : CaloID2DLL( name, pSvc ) {
    using LHCb::CaloAlgUtils::CaloIdLocation;

    updateHandleLocation( *this, "Input", CaloIdLocation( "PrsE", context() ) );
    updateHandleLocation( *this, "Output", CaloIdLocation( "PrsPIDe", context() ) );

    _setProperty( "nVlong", Gaudi::Utils::toString( 100 * Gaudi::Units::MeV ) );
    _setProperty( "nVdown", Gaudi::Utils::toString( 100 * Gaudi::Units::MeV ) );
    _setProperty( "nVTtrack", Gaudi::Utils::toString( 100 * Gaudi::Units::MeV ) );
    _setProperty( "nMlong", Gaudi::Utils::toString( 100 * Gaudi::Units::GeV ) );
    _setProperty( "nMdown", Gaudi::Utils::toString( 100 * Gaudi::Units::GeV ) );
    _setProperty( "nMTtrack", Gaudi::Utils::toString( 100 * Gaudi::Units::GeV ) );

    _setProperty( "HistogramL", "DLL_Long" );
    _setProperty( "HistogramD", "DLL_Downstream" );
    _setProperty( "HistogramT", "DLL_Ttrack" );
    _setProperty( "ConditionName", "Conditions/ParticleID/Calo/PrsPIDe" );

    _setProperty( "HistogramL_THS", "CaloPIDs/CALO/PRSPIDE/h3" );
    _setProperty( "HistogramD_THS", "CaloPIDs/CALO/PRSPIDE/h5" );
    _setProperty( "HistogramT_THS", "CaloPIDs/CALO/PRSPIDE/h6" );

    _setProperty( "AcceptedType", Gaudi::Utils::toString<int>( LHCb::Track::Types::Long, LHCb::Track::Types::Ttrack,
                                                               LHCb::Track::Types::Downstream ) );
  };
};

// ============================================================================

DECLARE_COMPONENT( PrsPIDeAlg )

// ============================================================================
