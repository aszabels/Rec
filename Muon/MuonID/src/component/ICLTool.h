/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef ICLTOOL_H
#define ICLTOOL_H 1

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

/** @class ICLTool ICLTool.h
 *
 *
 *  @author Jose Angel Hernando Morata
 *  @author Xabier Cid Vidal
 *  @date   2008-07-02
 */
struct ICLTool : extend_interfaces<IAlgTool> {

  // Return the interface ID
  DeclareInterfaceID( ICLTool, 2, 0 );

  virtual double minRange() const = 0;

  virtual double maxRange() const = 0;

  virtual StatusCode cl( double value, double& cls, double& clb, double& clratio, double range = 0.,
                         int region = 0 ) const = 0;
};
#endif // ICLTOOL_H
