/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// local
#include "MuonFastPosTool.h"

//-----------------------------------------------------------------------------
// Implementation file for class : MuonFastPosTool
//
// 2003-04-16 : Alessia Satta
//-----------------------------------------------------------------------------

DECLARE_COMPONENT( MuonFastPosTool )

namespace {
  constexpr unsigned int m_padGridX[5]{24, 48, 48, 12, 12};
  constexpr unsigned int m_padGridY[5]{8, 8, 8, 8, 8};
  constexpr unsigned int m_stripXGridX[20]{24, 24, 24, 24, 48, 48, 48, 48, 48, 48,
                                           48, 48, 12, 12, 12, 12, 12, 12, 12, 12};
  constexpr unsigned int m_stripXGridY[20]{8, 8, 8, 8, 1, 2, 2, 2, 1, 2, 2, 2, 8, 2, 2, 2, 8, 2, 2, 2};
  constexpr unsigned int m_stripYGridX[20]{24, 24, 24, 24, 8, 4, 2, 2, 8, 4, 2, 2, 12, 4, 2, 2, 12, 4, 2, 2};
  constexpr unsigned int m_stripYGridY[20]{8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8};
} // namespace

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
MuonFastPosTool::MuonFastPosTool( const std::string& type, const std::string& name, const IInterface* parent )
    : base_class( type, name, parent ) {
  declareInterface<IMuonFastPosTool>( this );
}
//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
StatusCode MuonFastPosTool::initialize() {

  StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;

  if ( msgLevel( MSG::DEBUG ) ) debug() << "Initialising the muon get info tool" << endmsg;

  m_muonDetector = getDet<DeMuonDetector>( "/dd/Structure/LHCb/DownstreamRegion/Muon" );

  /// get tile tool
  MuonBasicGeometry basegeometry( detSvc(), msgSvc() );
  m_stationNumber = basegeometry.getStations();
  m_regionNumber  = basegeometry.getRegions();

  if ( m_stationNumber == 4 ) {
    m_padSizeX.resize( 16 );
    m_padSizeY.resize( 16 );
    m_stripXSizeX.resize( 16 );
    m_stripXSizeY.resize( 16 );
    m_stripYSizeX.resize( 16 );
    m_stripYSizeY.resize( 16 );
    for ( int i = 0; i < 4; i++ ) {
      m_padGridX_internal[i] = m_padGridX[i + 1];
      m_padGridY_internal[i] = m_padGridY[i + 1];
      for ( int r = 0; r < m_regionNumber; r++ ) {
        m_stripXGridX_internal[i * 4 + r] = m_stripXGridX[i * 4 + 4 + r];
        m_stripXGridY_internal[i * 4 + r] = m_stripXGridY[i * 4 + 4 + r];
        m_stripYGridX_internal[i * 4 + r] = m_stripYGridX[i * 4 + 4 + r];
        m_stripYGridY_internal[i * 4 + r] = m_stripYGridY[i * 4 + 4 + r];
      }
    }
  } else {
    m_padSizeX.resize( 20 );
    m_padSizeY.resize( 20 );
    m_stripXSizeX.resize( 20 );
    m_stripXSizeY.resize( 20 );
    m_stripYSizeX.resize( 20 );
    m_stripYSizeY.resize( 20 );

    for ( int i = 0; i < 5; i++ ) {
      m_padGridX_internal[i] = m_padGridX[i];
      m_padGridY_internal[i] = m_padGridY[i];
      for ( int r = 0; r < m_regionNumber; r++ ) {
        m_stripXGridX_internal[i * 4 + r] = m_stripXGridX[i * 4 + r];
        m_stripXGridY_internal[i * 4 + r] = m_stripXGridY[i * 4 + r];
        m_stripYGridX_internal[i * 4 + r] = m_stripYGridX[i * 4 + r];
        m_stripYGridY_internal[i * 4 + r] = m_stripYGridY[i * 4 + r];
      }
    }
  }

  for ( int i = 0; i < m_stationNumber; i++ ) {

    int channels = 48 * ( m_padGridX_internal[i] * m_padGridY_internal[i] );
    m_posPad[i].resize( channels );

    channels = 12 * ( m_stripXGridX_internal[i * 4] * m_stripXGridY_internal[i * 4] ) +
               12 * ( m_stripXGridX_internal[i * 4 + 1] * m_stripXGridY_internal[i * 4 + 1] ) +
               12 * ( m_stripXGridX_internal[i * 4 + 2] * m_stripXGridY_internal[i * 4 + 2] ) +
               12 * ( m_stripXGridX_internal[i * 4 + 3] * m_stripXGridY_internal[i * 4 + 3] );
    m_posStripX[i].resize( channels );
    channels = 12 * ( m_stripYGridX_internal[i * 4] * m_stripYGridY_internal[i * 4] ) +
               12 * ( m_stripYGridX_internal[i * 4 + 1] * m_stripYGridY_internal[i * 4 + 1] ) +
               12 * ( m_stripYGridX_internal[i * 4 + 2] * m_stripYGridY_internal[i * 4 + 2] ) +
               12 * ( m_stripYGridX_internal[i * 4 + 3] * m_stripYGridY_internal[i * 4 + 3] );
    m_posStripY[i].resize( channels );
  }

  if ( msgLevel( MSG::DEBUG ) )
    debug() << "Initialising the muon get info tool" << m_stationNumber << " " << m_regionNumber << endmsg;
  // fill pad vectors
  LHCb::MuonTileID tile;

  for ( int station = 0; station < m_stationNumber; station++ ) {

    tile.setStation( station );
    tile.setLayout( MuonLayout( m_padGridX_internal[station], m_padGridY_internal[station] ) );
    int index = 0;

    for ( int region = 0; region < m_regionNumber; region++ ) {
      tile.setRegion( region );
      for ( int quarter = 0; quarter < 4; quarter++ ) {
        tile.setQuarter( quarter );
        for ( unsigned y = 0; y < m_padGridY_internal[station]; y++ ) {
          tile.setY( y );
          for ( unsigned x = m_padGridX_internal[station]; x < 2 * m_padGridX_internal[station]; x++ ) {
            tile.setX( x );

            double xp, dx, yp, dy, zp, dz;
            sc = m_muonDetector->Tile2XYZ( tile, xp, dx, yp, dy, zp, dz );
            if ( sc.isFailure() ) return sc;
            ( m_posPad[station] )[index++]   = Gaudi::XYZPoint( xp, yp, zp );
            m_padSizeX[station * 4 + region] = (float)dx;
            m_padSizeY[station * 4 + region] = (float)dy;
          }
        }

        for ( unsigned y = m_padGridY_internal[station]; y < 2 * m_padGridY_internal[station]; y++ ) {
          tile.setY( y );
          for ( unsigned x = 0; x < 2 * m_padGridX_internal[station]; x++ ) {
            tile.setX( x );
            double xp, dx, yp, dy, zp, dz;
            sc = m_muonDetector->Tile2XYZ( tile, xp, dx, yp, dy, zp, dz );
            if ( sc.isFailure() ) return sc;
            ( m_posPad[station] )[index++] = Gaudi::XYZPoint( xp, yp, zp );
          }
        }
      }
    }
  }
  // stripX

  if ( msgLevel( MSG::DEBUG ) ) debug() << "initializing strip x " << endmsg;

  for ( int station = 0; station < m_stationNumber; station++ ) {
    tile.setStation( station );

    int index = 0;
    // int indexInStation=0;

    for ( int region = 0; region < m_regionNumber; region++ ) {
      m_stripXOffset[station * 4 + region] = index;

      tile.setLayout(
          MuonLayout( m_stripXGridX_internal[station * 4 + region], m_stripXGridY_internal[station * 4 + region] ) );
      tile.setRegion( region );
      for ( int quarter = 0; quarter < 4; quarter++ ) {
        tile.setQuarter( quarter );
        for ( unsigned y = 0; y < m_stripXGridY_internal[station * 4 + region]; y++ ) {
          tile.setY( y );
          for ( unsigned x = m_stripXGridX_internal[station * 4 + region];
                x < 2 * m_stripXGridX_internal[station * 4 + region]; x++ ) {
            tile.setX( x );

            double xp, dx, yp, dy, zp, dz;
            sc = m_muonDetector->Tile2XYZ( tile, xp, dx, yp, dy, zp, dz );
            if ( sc.isFailure() ) return sc;
            ( m_posStripX[station] )[index++] = Gaudi::XYZPoint( xp, yp, zp );
            if ( dx > m_stripXSizeX[station * 4 + region] ) m_stripXSizeX[station * 4 + region] = (float)dx;
            if ( dy > m_stripXSizeY[station * 4 + region] ) m_stripXSizeY[station * 4 + region] = (float)dy;
            if ( msgLevel( MSG::DEBUG ) ) {
              if ( region > 1 ) {
                debug() << " station " << station << " " << quarter << " " << tile << " " << dx << " " << dy << endmsg;
                debug() << tile.layout() << endmsg;
              }
            }
          }
        }
        for ( unsigned y = m_stripXGridY_internal[station * 4 + region];
              y < 2 * m_stripXGridY_internal[station * 4 + region]; y++ ) {
          tile.setY( y );
          for ( unsigned x = 0; x < 2 * m_stripXGridX_internal[station * 4 + region]; x++ ) {
            tile.setX( x );
            double xp, dx, yp, dy, zp, dz;
            sc = m_muonDetector->Tile2XYZ( tile, xp, dx, yp, dy, zp, dz );
            if ( sc.isFailure() ) return sc;
            ( m_posStripX[station] )[index++] = Gaudi::XYZPoint( xp, yp, zp );
          }
        }
      }
    }
  }
  // stripY

  if ( msgLevel( MSG::DEBUG ) ) debug() << "initializing strip y " << endmsg;

  for ( int station = 0; station < m_stationNumber; station++ ) {
    tile.setStation( station );

    int index = 0;

    for ( int region = 0; region < m_regionNumber; region++ ) {
      m_stripYOffset[station * 4 + region] = index;

      tile.setLayout(
          MuonLayout( m_stripYGridX_internal[station * 4 + region], m_stripYGridY_internal[station * 4 + region] ) );
      tile.setRegion( region );
      for ( int quarter = 0; quarter < 4; quarter++ ) {
        tile.setQuarter( quarter );
        for ( unsigned y = 0; y < m_stripYGridY_internal[station * 4 + region]; y++ ) {
          tile.setY( y );
          for ( unsigned x = m_stripYGridX_internal[station * 4 + region];
                x < 2 * m_stripYGridX_internal[station * 4 + region]; x++ ) {
            tile.setX( x );

            double xp, dx, yp, dy, zp, dz;
            sc = m_muonDetector->Tile2XYZ( tile, xp, dx, yp, dy, zp, dz );
            if ( sc.isFailure() ) return sc;
            ( m_posStripY[station] )[index++] = Gaudi::XYZPoint( xp, yp, zp );
            if ( dx > m_stripYSizeX[station * 4 + region] ) m_stripYSizeX[station * 4 + region] = (float)dx;
            if ( dy > m_stripYSizeY[station * 4 + region] ) m_stripYSizeY[station * 4 + region] = (float)dy;
            if ( msgLevel( MSG::DEBUG ) ) {
              if ( region > 1 ) {
                debug() << " station " << station << " " << quarter << " " << tile << " " << dx << " " << dy << endmsg;
                debug() << tile.layout() << endmsg;
              }
            }
          }
        }
        for ( unsigned y = m_stripYGridY_internal[station * 4 + region];
              y < 2 * m_stripYGridY_internal[station * 4 + region]; y++ ) {
          tile.setY( y );
          for ( unsigned x = 0; x < 2 * m_stripYGridX_internal[station * 4 + region]; x++ ) {
            tile.setX( x );
            double xp, dx, yp, dy, zp, dz;
            sc = m_muonDetector->Tile2XYZ( tile, xp, dx, yp, dy, zp, dz );
            if ( sc.isFailure() ) return sc;
            ( m_posStripY[station] )[index++] = Gaudi::XYZPoint( xp, yp, zp );
          }
        }
      }
    }
  }

  if ( msgLevel( MSG::DEBUG ) ) {
    for ( int station = 0; station < m_stationNumber; station++ ) {
      for ( int region = 0; region < m_regionNumber; region++ ) {
        debug() << " station " << station << " " << region << " " << m_stripXSizeX[station * 4 + region] << " "
                << m_stripXSizeY[station * 4 + region] << endmsg;
      }
    }
  }

  return StatusCode::SUCCESS;
}

StatusCode MuonFastPosTool::calcTilePos( const LHCb::MuonTileID& tile, double& x, double& deltax, double& y,
                                         double& deltay, double& z, double& /*deltaz*/ ) const {

  int          station    = tile.station();
  int          region     = tile.region();
  int          quarter    = tile.quarter();
  int          perQuarter = 3 * m_padGridX_internal[station] * m_padGridY_internal[station];
  unsigned int xpad       = tile.nX();
  unsigned int ypad       = tile.nY();
  unsigned int index      = ( region * 4 + quarter ) * perQuarter;

  if ( ypad < m_padGridY_internal[station] ) {
    index = index + m_padGridX_internal[station] * ypad + xpad - m_padGridX_internal[station];
  } else {
    index = index + m_padGridX_internal[station] * m_padGridY_internal[station] +
            2 * m_padGridX_internal[station] * ( ypad - m_padGridY_internal[station] ) + xpad;
  }

  const Gaudi::XYZPoint& p = m_posPad[station][index];
  x                        = p.x();
  y                        = p.y();
  z                        = p.z();
  deltax                   = m_padSizeX[station * 4 + region];
  deltay                   = m_padSizeY[station * 4 + region];

  return StatusCode::SUCCESS;
}

StatusCode MuonFastPosTool::calcStripXPos( const LHCb::MuonTileID& tile, double& x, double& deltax, double& y,
                                           double& deltay, double& z, double& /*deltaz*/ ) const {

  int station        = tile.station();
  int region         = tile.region();
  int quarter        = tile.quarter();
  int perQuarter     = 3 * m_stripXGridX_internal[station * 4 + region] * m_stripXGridY_internal[station * 4 + region];
  unsigned int xpad  = tile.nX();
  unsigned int ypad  = tile.nY();
  unsigned int index = m_stripXOffset[station * 4 + region] + quarter * perQuarter;

  if ( ypad < m_stripXGridY_internal[station * 4 + region] ) {
    index = index + m_stripXGridX_internal[station * 4 + region] * ypad + xpad -
            m_stripXGridX_internal[station * 4 + region];
  } else {
    index = index + m_stripXGridX_internal[station * 4 + region] * m_stripXGridY_internal[station * 4 + region] +
            2 * m_stripXGridX_internal[station * 4 + region] * ( ypad - m_stripXGridY_internal[station * 4 + region] ) +
            xpad;
  }

  const Gaudi::XYZPoint& p = m_posStripX[station][index];
  x                        = p.x();
  y                        = p.y();
  z                        = p.z();
  deltax                   = m_stripXSizeX[station * 4 + region];
  deltay                   = m_stripXSizeY[station * 4 + region];

  return StatusCode::SUCCESS;
}

StatusCode MuonFastPosTool::calcStripYPos( const LHCb::MuonTileID& tile, double& x, double& deltax, double& y,
                                           double& deltay, double& z, double& /*deltaz*/ ) const {

  int station        = tile.station();
  int region         = tile.region();
  int quarter        = tile.quarter();
  int perQuarter     = 3 * m_stripYGridX_internal[station * 4 + region] * m_stripYGridY_internal[station * 4 + region];
  unsigned int xpad  = tile.nX();
  unsigned int ypad  = tile.nY();
  unsigned int index = m_stripYOffset[station * 4 + region] + quarter * perQuarter;

  if ( ypad < m_stripYGridY_internal[station * 4 + region] ) {
    index = index + m_stripYGridX_internal[station * 4 + region] * ypad + xpad -
            m_stripYGridX_internal[station * 4 + region];
  } else {
    index = index + m_stripYGridX_internal[station * 4 + region] * m_stripYGridY_internal[station * 4 + region] +
            2 * m_stripYGridX_internal[station * 4 + region] * ( ypad - m_stripYGridY_internal[station * 4 + region] ) +
            xpad;
  }

  const Gaudi::XYZPoint& p = m_posStripY[station][index];
  x                        = p.x();
  y                        = p.y();
  z                        = p.z();
  deltax                   = m_stripYSizeX[station * 4 + region];
  deltay                   = m_stripYSizeY[station * 4 + region];

  return StatusCode::SUCCESS;
}

//=============================================================================
