2018-03-22 Rec v30r0
===

This version uses Lbcom v30r0, LHCb v50r0, Gaudi v30r2 and LCG_93 with ROOT 6.12.06
<p>
This version is released on `master` branch. The previous release on `master` branch  was `Rec v22r1`.

This release includes the merge to `master` of the `future` branch, done in November 2017 - see !800 commit history for details of `future` changes that were included 

This release also contains all the changes that were made in `Rec v23r0` (released on `2018-patches` branch, see corresponding release notes), as well as the additional changes described in this file.


### New features
- Made CommonMuonHitManager public, !834 (@nkazeev)   

- Added implementation of Parameterized Kalman, !721, !850, !905 (@sstemmle)   

- Introduced VPLightcluster, !858, !904 (@sponce)   


### Enhancements
- Template TrackListMerger and export copies for LHCb::Tracks and LHCb::Track::Selection, !958 (@olupton)   

- Faster PrVeloUT, !859, !913 (@decianm)   
  *  Get rid of the tool, do everything directly in `PrVeloUT`  
  *  Cache look-up table for tolerances locally, instead of accessing the tool for every track. Still need to do the same for the Bdl table.  
  *  Avoid using a `std::vector` in the clustering stage, as it just creates unnecessary overhead  
  *  Introduce a minimum momentum cut of 1500MeV. More detailed checks needed for possible efficiency loss  
  *  Speed up hit search by precalculating some quantities.  
  *  Don't split hits in top and bottom anymore, gain was not worth the savings in time.  
  *  Also contains a (prototype) version of the method that creates the output tracks, as most of the time is spent in the method that writes out the `LHCb::Track` in keyed containers. In principle only `x,y,z,tx,ty,q/p` and the `LHCbIDs` are needed from VeloUT

- Try to fix FT zone order, !891 (@mhadji)   

- Improvements for faster decoding in PrFTStoreHit. Reduces CPU consumption by ~40%, !882, !897 (@jvantilb) [LHCBSCIFI-109]   

- replace isLarge with pseudoSize for FTLiteClusters, resolution depends on pseudoSize, !838 (@sesen)   

- Adapt to new templated LHCb traj for TVF, !894, !914 (@mhadji)   
    
- declare MeasProviders as properties, !898 (@sstemmle)   

- Use addSortedForwardStates when inserting new states on TrackEventFitter, !847 (@mhadji)   

- Vectorized updateTransport in TrackVectorFitter, !811 (@dcampora)   
  Closes https://gitlab.cern.ch/lhcb/Rec/issues/2  
 
- Removed reference nodes from TrackVectorFitter, !802 (@dcampora)   
  Closes https://gitlab.cern.ch/lhcb/Rec/issues/4, https://gitlab.cern.ch/lhcb/Rec/issues/6  
    
- changes in MuonID after 12/2017 hackathon, !922 (@rvazquez)   
  - change vdt::fast_exp to std::exp as profiling shows it to be faster  
    - remove the Choleskiy decomposition for matrix inversion in favour of Kramer as profiling shows it to be faster  
    - change when possible double to float  
    - change when possible int to unsigned int

- Fixes for running muon algorithms on upgrade MC, !823 (@olupton)   

- MuonID upgrade to Gaudi functional, !822 (@nkazeev)   

- Rich adapt to decoded data format update, !923 (@jonrob)   

- Various RICH CPU improvements, !907 (@jonrob)   
  - Adapt to move of SIMD mirror data cache to `RichFutureUtils` in `LHCb`.  
  - Use new SIMD global to PD panel coordinate system transformation in the smart ID tool.  
  - Small improvement to the detectable signal algorithm to avoid unnecessary find calls.  
  - CPU improvements to the geometrical efficiency algorithm from converting the TES data type from `map<A,B>` to `vector< pair<A,B> >`.  

- RichRayTracing - Turn off the beam pipe intersection checks with raytracing photons for the CK rings., !889 (@jonrob)   

- RICH - Turn off by default unambiguous photon and beam pipe intersection checks in quartic reconstruction., !888 (@jonrob)

- Reduce secondary mirror iterations to 1 for RICH1 in quartic reconstruction, !886 (@jonrob)   
  
- Reoptimise the RICH photon selection cuts for the Upgrade, !875 (@jonrob)   

- RICH - Minor optimisation, !848 (@jonrob)   
  - Add to reco base class methods to retrieve list of active mass hypotheses excluding the below threshold type.
  - Remove unnecessary normalisation of final photon direction in quartic reconstruction.  
  - Remove some residual aerogel support code.  

- Small update to RICH expected photon signal algorithm, !830 (@jonrob)   
  - Remove the Below Threshold type from the hypo list during initialize() rather than skip each time.  
  - Bail out the hypo loop as soon as a below threshold type is found.  
  - Make the min expected track CK theta angle a configurable radiator property.  
  - Drive by cleanup of RichFutureRecBase.

- Convert RICH CK Estimation photon reconstruction alg to SIMD version., !831 (@jonrob)   

- Use fully SIMD RICH ray tracing API, !795 (@jonrob)   

- Fully SIMD vectorised RICH photon reconstruction, !779, !814, !884 (@jonrob)   

- Backported optimizations in memory allocations from TDR to master, !860 (@sponce)   

- Adapt to Future linker base class, !839 (@sstahl)   

### Bug fixes
- Suppress a maybe unitialised variable warning in StandaloneMuonRec, !971 (@jonrob)   

- Remove default muon state, !966 (@sstahl)   
  The state is not produced by the MuonID, only by some monitoring.

- Remove duplicate MuonIDAlgLite properties, !955 (@olupton)   

- PrVeloUT: Fix sometimes off by one in degrees of freedom, !947 (@graven)   
 
- Fix typo in PatLongLivedTracking parameters and fix some indentations, !941 (@decianm)   

- Fix in-place modification of PrPixelTracking property, !927 (@olupton)   

- Fix undefined reference with -O0 after gaudi/Gaudi!514, !903 (@clemenci)   
  
- Fix TrackListRefiner for VPTrackSelector in Alignment, !825 (@cburr)   

- Fix clang warnings, !819 (@cattanem)   
  - ParameterizedKalman: replace abs by std::abs  
  - CaloMoniDst: remove unused variables

- Fix typo in assertions (std::none_of -> std::all_of), !812 (@chasse)   

- Fix typo in PrMatchNN, !806 (@chasse)   

- Fix trivial clang warnings, !807 (@cattanem)   

- Fix clang compilation warnings, !784 (@cattanem)   
  * PrForwardTool: remove unused member variable  
  * TrackSTEPExtrapolator: Remove unused member variables

- Fix clang warnings in MuonID, !909 (@cattanem)   

- Prevent automatic retrieval of undesired ToolHandles, !791 (@clemenci)   


### Code modernisations and cleanups

- Generalize IMeasurementProvider: prefer span over iterator_range, !940 (@graven)   

- PrVeloUT: implicitly deduce template argument, !943 (@graven)   

- Added method in PrUTMagnetTool/PrTableForFunction to return the table, !855 (@decianm)   

- Do not assume that Track::states() returns a reference to a container, !944 (@graven)   

- Removed Tr::TrackVectorFit::FitTrack, !753 (@dcampora)   

- Adapt TrackBestTrackCreator to automatic ToolHandle retrieval in gaudi/Gaudi!429, !782 (@cattanem)   

- Generalize TrackTraj, !915 (@graven)   
  - avoid mentioning StateContainer, accept span<State* const> instead  
  - prefer offsets over iterators  
  - use assert instead of commented out checks  
  - make static object const

- Modernize TrackFitter, !849 (@graven)   
  - Fully qualify enum values  
  - Prefer explicit enums over ints  
  - Prefer control flow which reduces indentation

- Modernize Track Likelihood, !837 (@graven)   
  - avoid redundant copies & loops  
  - add `ITrackFunctor` in order to be able to deprecate the use of `ITrackManipulator`
  - Make TrackLikelihood inherit from ITrackFunctor instead of ITrackManipulator, and make it const-correct  
  - TrackAddLikelihood now calls ITrackFunctor to obtain values, and then it forwards those value to Track::setLikelihood  
  - Prefer Gaudi::Property

- Generalize ITrackVertexer interface by using span<T> instead of an explicit container<T> type, !917 (@graven)     
  and update the TrackVertexer implementation accordingly

- Modernize TrackCheckerNT, !931 (@graven)   
  - avoid redundant casts  
  - prefer STL algorithms over raw loops  
  - prefer auto  
  - prefer switch/case over if-else-if-else cascades  
  - avoid explicit memory allocation

- Further modernize TrackCaloMatchMonitor, !932 (@graven)   
  - prefer Gaudi::Property  
  - inherit constructor  
  - do not use front and back of ConstNodeRange (to allow ConstNodeRange to become a gsl::span)  
  - use pointer / boost::optional for 'nullable' State instead of an explicit bool flag  
  - prefer emplace_back  
  - prefer std::accumulate over explicit raw loop

- Modernize TrackMonitors, !916 (@graven)   
  - Avoid mentioning LHCb::Track::ConstNodeRange  
  - prefer switch over if-else-if-else-  
  - prefer TupleTool::columns over TupleTool::column

- RichRecUtils - Remove unused code, !854 (@jonrob)   

- RICH Clean up Pixel SIMD algorithm, !828 (@jonrob)   

- Remove old RICH framework + Future scalar algorithms, !818 (@jonrob)   

- Remove unused captures in Rich global pid algorithm, !778 (@jonrob)   

- Modernize Muons, !717 (@graven)   
  - make interfaces inherit from extend_interfaces, and use `DeclareInterfaceID`  
  - avoid trailing return types in interfaces  
  - deprecate interface methods which take a string as argument, provide method which takes an enum instead -- this avoids having to create a string, and then parse it, on every call  
  - prefer inheriting constructors  
  - prefer std::abs over fabs  
  - prefer std::transform over std::for_each  
  - add const  
  - move debug printout out of loop  
  - simplify logic  
  - prefer std::array over std::vector to avoid heap allocation  
  - prefer passing std::array<>& over decayed pointers  
  - prefer true/false over 1/0 assignments for bool  
  - prefer std::array.fill over loops  
  - simplify inside using lambda  
  - prefer range-based for loops  
  - prefer switch/case over if-else-if-else-if-  
  - prefer boost::static_vector over std::vector if there is a known upperlimit on the maximum size  
  - prefer range-based loop over BOOST_FOREACH


- msgLevel already caches the level -- do not cache the cached value, !890 (@graven)   

- Fully qualify enum values, !851, !852, !853, !856, !868 (@graven)   

- Adapt code to lhcb/LHCb!1077, !865 (@graven)   
  - avoid using eventSvc() in MCTrackInfo, prefer make_MCTrackInfo  
  - do not explicitly fetch MCParticles from event store  
  - remove explicit initialize and finalize  
  - prefer Gaudi::Property

- Remove extraneous PUBLIC_HEADERS uncovered by gaudi/Gaudi!615, !933 (@rmatev)   

- Removed use of deprecated factories, !803, !964 (@clemenci)   
  - replaced `DECLARE_*_FACTORY` with `DECLARE_COMPONENT`  
  - made constructors public  
  - removed some unnecessary _disabling_ of copy constructors  
  - removed some trivial destructors  
  See gaudi/Gaudi!420

- Adapted to change in logical comparison between StatusCodes, !885 (@clemenci)   
  see gaudi/Gaudi!514

- Forward compatibility for gaudi/Gaudi!514, !870 (@clemenci)   
  
- Make forward/backward compatible with gaudi/Gaudi!534, !805 (@graven)   

### Monitoring changes
- Remove spurious info message, !864 (@cattanem)   

- Adapt to Future linker base class, !839 (@sstahl)   

- Add TrackResChecker to CheckPatSeq, !798 (@sstemmle)   

### Changes to tests
- Update reference to follow gaudi/Gaudi!542, !863 (@cattanem)   

- Update test reference to follow formatting change from gaudi/Gaudi!426, !804 (@cattanem)   

- LumiAlgs: Tweak lumi2reader reference file to follow gaudi/Gaudi!515, !794 (@cattanem)   
