/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRLINEFITTER_H
#define PRLINEFITTER_H 1

// Include files
#include "PrKernel/PrHit.h"

/** @class PrLineFitter PrLineFitter.h
 *  Simple class to fit a line with coordinates
 *
 *  @author Olivier Callot
 *  @date   2012-08-03
 */
class PrLineFitter final {
public:
  void reset( float z, PrHits* hits ) {
    m_z0   = z;
    m_s0   = 0.f;
    m_sz   = 0.f;
    m_sz2  = 0.f;
    m_sc   = 0.f;
    m_scz  = 0.f;
    m_c0   = 0.f;
    m_tc   = 0.f;
    m_hits = hits;
  }

  void addHit( const ModPrHit& hit ) { addHitInternal( hit ); }

  inline float distance( const ModPrHit& hit ) { return hit.coord - ( m_c0 + ( hit.hit->z() - m_z0 ) * m_tc ); }

  inline float chi2( const ModPrHit& hit ) {
    float d = distance( hit );
    return d * d * hit.hit->w();
  }

  float coordAtRef() const { return m_c0; }
  float slope() const { return m_tc; }

  void solve() {
    float den = ( m_sz * m_sz - m_s0 * m_sz2 );
    m_c0      = ( m_scz * m_sz - m_sc * m_sz2 ) / den;
    m_tc      = ( m_sc * m_sz - m_s0 * m_scz ) / den;
  }

private:
  void addHitInternal( const ModPrHit& hit ) {
    m_hits->push_back( hit.hit );
    float c = hit.coord;
    float w = hit.hit->w();
    float z = hit.hit->z() - m_z0;
    m_s0 += w;
    m_sz += w * z;
    m_sz2 += w * z * z;
    m_sc += w * c;
    m_scz += w * c * z;
  }

  float   m_z0   = 0.;
  PrHits* m_hits = nullptr;
  float   m_c0   = 0.;
  float   m_tc   = 0.;

  float m_s0  = 0.;
  float m_sz  = 0.;
  float m_sz2 = 0.;
  float m_sc  = 0.;
  float m_scz = 0.;
};
#endif // PRLINEFITTER_H
