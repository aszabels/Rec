/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// LHCb
#include "Event/StateParameters.h"
#include "Event/Track.h"
#include "Kernel/VPConstants.h"
#include "PrKernel/PrPixelUtils.h"

// Local
#include "VPClusCache.h"
#include "VeloClusterTracking.h"
#include "fastAtan2.h"

DECLARE_COMPONENT_WITH_ID( LHCb::Pr::Velo::ClusterTracking, "VeloClusterTracking" )

namespace LHCb::Pr::Velo {
  namespace {
    //=============================================================================
    // Internal data structures:
    //=============================================================================
    constexpr int VECTOR_LENGTH = 16;
    constexpr int VECTOR_ALIGN  = VECTOR_LENGTH * sizeof( float );

    constexpr int MAX_CLUSTERS_PER_PLANE = 1024;
    struct PlaneSoA {
      alignas( VECTOR_ALIGN ) float Gx[MAX_CLUSTERS_PER_PLANE];
      alignas( VECTOR_ALIGN ) float Gy[MAX_CLUSTERS_PER_PLANE];
      alignas( VECTOR_ALIGN ) float Gz[MAX_CLUSTERS_PER_PLANE];
      alignas( VECTOR_ALIGN ) float phi[MAX_CLUSTERS_PER_PLANE];
      alignas( VECTOR_ALIGN ) uint32_t cId[MAX_CLUSTERS_PER_PLANE];
      alignas( VECTOR_ALIGN ) uint16_t used[MAX_CLUSTERS_PER_PLANE];
      int n_hits;
      int offset; // start of Plane in global hit array
    };

    constexpr int MAX_HITS = 10000;
    struct HitSoA {
      alignas( VECTOR_ALIGN ) float x[MAX_HITS];
      alignas( VECTOR_ALIGN ) float y[MAX_HITS];
      alignas( VECTOR_ALIGN ) float z[MAX_HITS];
      alignas( VECTOR_ALIGN ) uint32_t cId[MAX_HITS];
      int n_hits;
    };

    constexpr int MAX_TRACKS_PER_EVENT = 2048;
    struct LightTracksSoA {
      alignas( VECTOR_ALIGN ) float x0[MAX_TRACKS_PER_EVENT];
      alignas( VECTOR_ALIGN ) float y0[MAX_TRACKS_PER_EVENT];
      alignas( VECTOR_ALIGN ) float z0[MAX_TRACKS_PER_EVENT];

      alignas( VECTOR_ALIGN ) float x1[MAX_TRACKS_PER_EVENT];
      alignas( VECTOR_ALIGN ) float y1[MAX_TRACKS_PER_EVENT];
      alignas( VECTOR_ALIGN ) float z1[MAX_TRACKS_PER_EVENT];

      alignas( VECTOR_ALIGN ) float x2[MAX_TRACKS_PER_EVENT];
      alignas( VECTOR_ALIGN ) float y2[MAX_TRACKS_PER_EVENT];
      alignas( VECTOR_ALIGN ) float z2[MAX_TRACKS_PER_EVENT];

      alignas( VECTOR_ALIGN ) float phi2[MAX_TRACKS_PER_EVENT];

      alignas( VECTOR_ALIGN ) float sum_scatter[MAX_TRACKS_PER_EVENT];

      alignas( VECTOR_ALIGN ) uint32_t cId[MAX_TRACKS_PER_EVENT * VPInfos::NPlanes];
      alignas( VECTOR_ALIGN ) int32_t n_hits[MAX_TRACKS_PER_EVENT]; // must be 32b
      alignas( VECTOR_ALIGN ) int32_t skipped[MAX_TRACKS_PER_EVENT];
    };

    bool is_used( PlaneSoA* P, int h ) { return ( ( ( (uint32_t*)P->used )[h / 32] ) >> ( h & 31 ) ) & 1; }

    inline void set_used( PlaneSoA* P, int h ) { ( (uint32_t*)P->used )[h / 32] |= 1 << ( h & 31 ); }

    inline void reset_used( PlaneSoA* P ) {
      for ( int i = 0; i <= P->n_hits / 32; i++ ) ( (uint32_t*)P->used )[i] = 0;
    }

#ifdef __AVX2__
#  include "VeloTracking_avx2.h"
#elif __AVX512F__
#  include "VeloTracking_avx512.h"
//#include "VeloTracking_avx256.h" // experimental
#else
#  include "VeloTracking.h"
#endif

    //=============================================================================
    // Union-Find functions used in clustering:
    //=============================================================================
    inline __attribute__( ( always_inline ) ) int find( int* L, int i ) {
      int ai = L[i];
      while ( ai != L[ai] ) ai = L[ai];
      return ai;
    }

    inline __attribute__( ( always_inline ) ) int merge( int* L, int ai, int j ) { // Union
      int aj = find( L, j );
      if ( ai < aj )
        L[aj] = ai;
      else {
        L[ai] = aj;
        ai    = aj;
      }
      return ai;
    }

    //=============================================================================
    // Clustering Algorithm
    //=============================================================================
    inline __attribute__( ( always_inline ) ) void getHits( const uint32_t** VPRawBanks, // List of input Super-Pixels
                                                            const int sensor0, const int sensor1,
                                                            int*      pixel_L,  // Equivalence table
                                                            uint32_t* pixel_SP, // SP candidates for clustering
                                                            uint32_t* pixel_S,  // Output Number of pixel per cluster
                                                            uint32_t* pixel_SX, // Output Sum of pixel x per cluster
                                                            uint32_t* pixel_SY, // Output Sum of pixel y per cluster
                                                            const std::array<std::array<float, 12>, VP::NSensors>& ltgs,
                                                            const std::array<float, VP::NSensorColumns>& m_local_x,
                                                            const std::array<float, VP::NSensorColumns>& m_x_pitch,
                                                            const float m_pixel_size, const uint32_t m_maxClusterSize,
                                                            PlaneSoA* P, int* perms, PlaneSoA* Pout, Hits& hits ) {
      int n_hits = 0; // Number of clusters after filtering
      for ( int s = sensor0; s < sensor1; s++ ) {
        const uint32_t*              bank    = VPRawBanks[s];
        const uint32_t               nsp     = *bank++;
        const std::array<float, 12>& ltg     = ltgs[s];
        int                          pixel_N = 0; // Pixels to cluster count
        int                          labels  = 0; // Total number of generated clusters

        for ( unsigned int i = 0; i < nsp; ++i ) {
          const uint32_t sp_word = *bank++;
          uint8_t        sp      = SP_getPixels( sp_word );

          if ( 0 == sp ) continue; // protect against zero super pixels.

          const auto sp_row = SP_getRow( sp_word );
          const auto sp_col = SP_getCol( sp_word );

          // This SP is isolated, skip clustering :
          if ( SP_isIsolated( sp_word ) ) {
            uint8_t mask = s_SPMasks[sp];

            auto n_kx_ky = s_SPn_kx_ky[sp & mask];
            auto n       = SPn_kx_ky_getN( n_kx_ky );  // number of pixel in this sp
            auto kx      = SPn_kx_ky_getKx( n_kx_ky ); // sum of x in this sp
            auto ky      = SPn_kx_ky_getKy( n_kx_ky ); // sum of y in this sp

            pixel_SX[labels] = sp_col * n * 2 + kx;
            pixel_SY[labels] = sp_row * n * 4 + ky;
            pixel_S[labels]  = n;
            labels++;

            if ( mask != 0xFF ) { // Add 2nd cluster
              n_kx_ky = s_SPn_kx_ky[sp & ( ~mask )];
              n       = SPn_kx_ky_getN( n_kx_ky );  // number of pixel in this sp
              kx      = SPn_kx_ky_getKx( n_kx_ky ); // sum of x in this sp
              ky      = SPn_kx_ky_getKy( n_kx_ky ); // sum of y in this sp

              pixel_SX[labels] = sp_col * n * 2 + kx;
              pixel_SY[labels] = sp_row * n * 4 + ky;
              pixel_S[labels]  = n;
              labels++;
            }

            continue;
          }

          // This one is not isolated, add it to clustering :
          uint32_t mask     = 0x7FFFFF00 | s_SPMasks[sp];
          pixel_SP[pixel_N] = sp_word & mask;
          pixel_L[pixel_N]  = pixel_N;
          pixel_N++;

          if ( mask != 0x7FFFFFFF ) {                      // Add 2nd cluster
            pixel_SP[pixel_N] = sp_word & ( mask ^ 0xFF ); // ~ of low 8 bits
            pixel_L[pixel_N]  = pixel_N;
            pixel_N++;
          }
        } // loop over super pixels in raw bank

        // SparseCCL
        // (This version assume SP are ordered by col then row)
        int start_j = 0;
        for ( int i = 0; i < pixel_N; i++ ) { // Pixel Scan
          uint32_t sp_word_i, sp_word_j;
          uint32_t ai;
          uint8_t  spi, spj;
          int      x_i, x_j, y_i, y_j;

          sp_word_i = pixel_SP[i];
          spi       = SP_getPixels( sp_word_i );
          y_i       = SP_getRow( sp_word_i );
          x_i       = SP_getCol( sp_word_i );

          pixel_L[i] = i;
          ai         = i;

          for ( int j = start_j; j < i; j++ ) {
            sp_word_j = pixel_SP[j];
            spj       = SP_getPixels( sp_word_j );
            y_j       = SP_getRow( sp_word_j );
            x_j       = SP_getCol( sp_word_j );
            if ( is_adjacent_8C_SP( x_i, y_i, x_j, y_j, spi, spj ) ) {
              ai = merge( pixel_L, ai, j );
            } else if ( x_j + 1 < x_i )
              start_j++;
          }
        }

        for ( int i = 0; i < pixel_N; i++ ) { // Transitive Closure + CCA
          uint32_t sp_word = pixel_SP[i];
          uint8_t  sp      = SP_getPixels( sp_word );
          uint32_t y_i     = SP_getRow( sp_word );
          uint32_t x_i     = SP_getCol( sp_word );

          auto n_kx_ky = s_SPn_kx_ky[sp];
          auto n       = SPn_kx_ky_getN( n_kx_ky );                // number of pixel in this sp
          auto kx      = x_i * n * 2 + SPn_kx_ky_getKx( n_kx_ky ); // sum of x in this sp
          auto ky      = y_i * n * 4 + SPn_kx_ky_getKy( n_kx_ky ); // sum of y in this sp

          uint32_t l;
          if ( pixel_L[i] == i ) {
            l           = labels++; // new label
            pixel_SX[l] = kx;
            pixel_SY[l] = ky;
            pixel_S[l]  = n;
          } else {
            l = pixel_L[pixel_L[i]]; // transitive closure
            pixel_SX[l] += kx;
            pixel_SY[l] += ky;
            pixel_S[l] += n;
          }
          pixel_L[i] = l;
        }

        for ( int i = 0; i < labels; i++ ) {
          uint32_t n = pixel_S[i];

          if ( n <= m_maxClusterSize ) {
            uint32_t x = pixel_SX[i];
            uint32_t y = pixel_SY[i];

            // if the pixel is smaller than the max cluster size, store it for the tracking
            const uint32_t cx = x / n;
            const uint32_t cy = y / n;

            // store target (3D point for tracking)
            const float fx      = x / static_cast<float>( n ) - cx;
            const float fy      = y / static_cast<float>( n );
            const float local_x = m_local_x[cx] + fx * m_x_pitch[cx];
            const float local_y = ( 0.5f + fy ) * m_pixel_size;

            const float gx = ( ltg[0] * local_x + ltg[1] * local_y + ltg[9] );
            const float gy = ( ltg[3] * local_x + ltg[4] * local_y + ltg[10] );
            const float gz = ( ltg[6] * local_x + ltg[7] * local_y + ltg[11] );

            P->Gx[n_hits]  = gx;
            P->Gy[n_hits]  = gy;
            P->Gz[n_hits]  = gz;
            P->cId[n_hits] = VPChannelID( s, cx, cy );
            n_hits++;
          }
        }
      } // Loop over sensors

      P->n_hits = n_hits;
      VeloTracking::sort_by_phi( P, Pout, perms, n_hits ); // Compute and sort by phi

      Pout->n_hits = n_hits;

      // Copy to global array
      Pout->offset = hits.size();
      for ( int i = 0; i < n_hits; i++ ) {
        auto pos = Vec3<SIMDWrapper::scalar::float_v>( Pout->Gx[i], Pout->Gy[i], Pout->Gz[i] );
        hits.store_pos( hits.size() + i, pos );
        hits.store_ChannelId( hits.size() + i, SIMDWrapper::scalar::int_v( Pout->cId[i] ) );
      }
      hits.size() += n_hits;
    }
  } // namespace

  //=============================================================================
  // Standard constructor, initializes variables
  //=============================================================================
  ClusterTracking::ClusterTracking( const std::string& name, ISvcLocator* pSvcLocator )
      : MultiTransformer( name, pSvcLocator, KeyValue{"RawEventLocation", RawEventLocation::Default},
                          {KeyValue{"HitsLocation", "Raw/VP/Hits"},
                           KeyValue{"TracksBackwardLocation", "Rec/Track/VeloBackward"},
                           KeyValue{"TracksLocation", "Rec/Track/Velo"}} ) {}

  //=============================================================================
  // Initialization
  //=============================================================================
  StatusCode ClusterTracking::initialize() {
    StatusCode sc = MultiTransformer::initialize();
    if ( sc.isFailure() ) return sc;

    m_vp = getDet<DeVP>( DeVPLocation::Default );
    // Make sure we are up-to-date on populated VELO stations
    registerCondition( m_vp->sensors().front()->geometry(), &ClusterTracking::rebuildGeometry );

    sc = updMgrSvc()->update( this );
    if ( !sc.isSuccess() ) return Error( "Failed to update station structure." );

    return StatusCode::SUCCESS;
  }

  //============================================================================
  // Rebuild the geometry (in case something changes in the Velo during the run)
  //============================================================================
  StatusCode ClusterTracking::rebuildGeometry() {
    // Delete the existing modules.
    m_modules.clear();
    m_firstModule = 999;
    m_lastModule  = 0;

    int        previousLeft  = -1;
    int        previousRight = -1;
    const auto sensors       = m_vp->sensors();

    // we copy the data from the geometry object which holds doubles intor a local float array
    std::copy( sensors.front()->xLocal().begin(), sensors.front()->xLocal().end(), m_local_x.begin() );
    std::copy( sensors.front()->xPitch().begin(), sensors.front()->xPitch().end(), m_x_pitch.begin() );
    m_pixel_size = sensors.front()->pixelSize( LHCb::VPChannelID( 0, 0, 0, 0 ) ).second;

    for ( unsigned i = 0; i < VPInfos::NSensors; i++ ) {
      // TODO:
      // if (!sensor->isReadOut()) continue;
      auto sensor = sensors[i];

      // get the local to global transformation matrix and
      // store it in a flat float array of sixe 12.
      Gaudi::Rotation3D     ltg_rot;
      Gaudi::TranslationXYZ ltg_trans;
      sensor->geometry()->toGlobalMatrix().GetDecomposition( ltg_rot, ltg_trans );
      auto& tr = m_ltg[sensor->sensorNumber()];
      ltg_rot.GetComponents( tr.data() ); // writes to [ ltg[0], ltg[8] ]
      tr[9]  = ltg_trans.X();
      tr[10] = ltg_trans.Y();
      tr[11] = ltg_trans.Z();

      // Get the number of the module this sensor is on.
      const unsigned int number = sensor->module();
      if ( number < m_modules.size() ) {
        // Check if this module has already been setup.
        if ( m_modules[number] ) continue;
      } else {
        m_modules.resize( number + 1, 0 );
      }

      // Create a new module and add it to the list.
      m_module_pool.emplace_back( number, sensor->isRight() );
      PrPixelModule* module = &m_module_pool.back();
      module->setZ( sensor->z() );
      if ( sensor->isRight() ) {
        module->setPrevious( previousRight );
        previousRight = number;
      } else {
        module->setPrevious( previousLeft );
        previousLeft = number;
      }
      m_modules[number] = module;
      if ( m_firstModule > number ) m_firstModule = number;
      if ( m_lastModule < number ) m_lastModule = number;
    }

    // the module pool might have been resized -- make sure
    // all module pointers are valid.
    for ( unsigned int i = 0; i < m_module_pool.size(); ++i ) {
      PrPixelModule* module       = &m_module_pool[i];
      m_modules[module->number()] = module;
    }

    return StatusCode::SUCCESS;
  }

  //=============================================================================
  // Main execution
  //=============================================================================
  std::tuple<Hits, Tracks, Tracks> ClusterTracking::operator()( const RawEvent& rawEvent ) const {

    auto result                                 = std::tuple<Hits, Tracks, Tracks>{};
    auto& [hits, tracksBackward, tracksForward] = result;

    const auto& tBanks = rawEvent.banks( RawBank::VP );
    if ( tBanks.empty() ) return result;

    const unsigned int version = tBanks[0]->version();
    if ( version != 2 ) {
      warning() << "Unsupported raw bank version (" << version << ")" << endmsg;
      return result;
    }

    const uint32_t* VPRawBanks[VPInfos::NSensors];

    // Copy rawbanks pointers to protect against unordered data
    for ( auto iterBank = tBanks.begin(); iterBank != tBanks.end(); iterBank++ ) {
      const uint32_t sensor = ( *iterBank )->sourceID();
      VPRawBanks[sensor]    = ( *iterBank )->data();
    }

    // Clustering buffers
    const int                        MAX_CLUSTERS_PER_SENSOR = 1024;    // Max number of SP per bank
    uint32_t                         pixel_SP[MAX_CLUSTERS_PER_SENSOR]; // SP to process
    int                              pixel_L[MAX_CLUSTERS_PER_SENSOR];  // Label equivalences
    alignas( VECTOR_ALIGN ) uint32_t pixel_SX[MAX_CLUSTERS_PER_SENSOR]; // x sum of clusters' pixels
    alignas( VECTOR_ALIGN ) uint32_t pixel_SY[MAX_CLUSTERS_PER_SENSOR]; // y sum of clusters' pixels
    alignas( VECTOR_ALIGN ) uint32_t pixel_S[MAX_CLUSTERS_PER_SENSOR];  // number of clusters' pixels

    alignas( VECTOR_ALIGN ) int perms[MAX_CLUSTERS_PER_PLANE]; // permutations for sort by phi

    // Tracking buffers
    PlaneSoA  raw_P0, raw_P1, raw_P2, tmp_P;
    PlaneSoA *P0 = &raw_P0, *P1 = &raw_P1, *P2 = &raw_P2;

    LightTracksSoA  t_candidates, t_forwarded, tracks;
    LightTracksSoA *tracks_candidates = &t_candidates, *tracks_forwarded = &t_forwarded;

    int n_tracks_output = 0;

    // Do tracking per plane backward
    {
      for ( int i = 0; i < MAX_CLUSTERS_PER_PLANE; i++ ) {
        P0->used[i] = 0;
        P1->used[i] = 0;
        P2->used[i] = 0;
      }

      int n_tracks_seeded    = 0;
      int n_tracks_forwarded = 0;

      const int i0 = VPInfos::NPlanes - 1, i1 = VPInfos::NPlanes - 2, i2 = VPInfos::NPlanes - 3;

      const int sensor0 = i0 * VPInfos::NSensorsPerPlane;
      const int sensor1 = i1 * VPInfos::NSensorsPerPlane;
      const int sensor2 = i2 * VPInfos::NSensorsPerPlane;

      getHits( VPRawBanks, sensor0, sensor0 + VPInfos::NSensorsPerPlane, pixel_L, pixel_SP, pixel_S, pixel_SX, pixel_SY,
               m_ltg, m_local_x, m_x_pitch, m_pixel_size, m_maxClusterSize, &tmp_P, perms, P0, hits );

      getHits( VPRawBanks, sensor1, sensor1 + VPInfos::NSensorsPerPlane, pixel_L, pixel_SP, pixel_S, pixel_SX, pixel_SY,
               m_ltg, m_local_x, m_x_pitch, m_pixel_size, m_maxClusterSize, &tmp_P, perms, P1, hits );

      getHits( VPRawBanks, sensor2, sensor2 + VPInfos::NSensorsPerPlane, pixel_L, pixel_SP, pixel_S, pixel_SX, pixel_SY,
               m_ltg, m_local_x, m_x_pitch, m_pixel_size, m_maxClusterSize, &tmp_P, perms, P2, hits );

      n_tracks_seeded = VeloTracking::TrackSeeding( P0, P1, P2, tracks_candidates, n_tracks_forwarded );

      PlaneSoA* tmp = P0;
      P0            = P1;
      P1            = P2;
      P2            = tmp;
      reset_used( P2 );

      for ( int p = VPInfos::NPlanes - 4; p >= 0; p-- ) {
        const int sensor = p * VPInfos::NSensorsPerPlane;
        getHits( VPRawBanks, sensor, sensor + VPInfos::NSensorsPerPlane, pixel_L, pixel_SP, pixel_S, pixel_SX, pixel_SY,
                 m_ltg, m_local_x, m_x_pitch, m_pixel_size, m_maxClusterSize, &tmp_P, perms, P2, hits );

        int n_candidates   = n_tracks_seeded + n_tracks_forwarded;
        n_tracks_forwarded = 0;
        VeloTracking::TrackForwarding( tracks_candidates, n_candidates, P2, tracks_forwarded, n_tracks_forwarded,
                                       &tracks, n_tracks_output );

        LightTracksSoA* tmp_track = tracks_candidates;
        tracks_candidates         = tracks_forwarded;
        tracks_forwarded          = tmp_track;

        n_tracks_seeded = VeloTracking::TrackSeeding( P0, P1, P2, tracks_candidates, n_tracks_forwarded );

        PlaneSoA* tmp = P0;
        P0            = P1;
        P1            = P2;
        P2            = tmp;
        reset_used( P2 );
      }

      VeloTracking::copy_remaining( tracks_candidates, n_tracks_seeded + n_tracks_forwarded, &tracks, n_tracks_output );
    } // plane backward

    // Make LHCb tracks
    using simd = SIMDWrapper::avx2::types;
    using F    = simd::float_v;
    using Vec  = Vec3<F>;
    for ( int t = 0; t < n_tracks_output; t += simd::size ) {
      auto loop_mask = simd::loop_mask( t, n_tracks_output );

      // Simple fit
      auto p1 = Vec( &tracks.x0[t], &tracks.y0[t], &tracks.z0[t] );
      auto p2 = Vec( &tracks.x2[t], &tracks.y2[t], &tracks.z2[t] );
      auto d  = p1 - p2;

      auto tx = d.x / d.z;
      auto ty = d.y / d.z;
      auto x0 = p2.x - p2.z * tx;
      auto y0 = p2.y - p2.z * ty;

      F z_beam = 0.5f * ( p1.z + p2.z );
      F denom  = tx * tx + ty * ty;
      z_beam   = select( denom < 0.001f * 0.001f, z_beam, -( x0 * tx + y0 * ty ) / denom );

      auto backwards = ( z_beam > p2.z ) && loop_mask;

      const float err  = ( sqrt( 12 ) / 0.055 ) * 0.45;
      const float err2 = err * err;

      auto s   = err2 * 2;
      auto sz  = err2 * ( p1.z + p2.z );
      auto sz2 = err2 * ( p1.z * p1.z + p2.z * p2.z );

      auto m00 = s;
      auto m10 = sz - z_beam * s;
      auto m11 = sz2 + ( z_beam * s - 2.f * sz ) * z_beam;
      auto det = 1.f / ( m11 * m00 - m10 * m10 );

      auto cov = Vec( m11 * det, -m10 * det, m00 * det );

      // State closest to beam
      auto beam_pos = Vec( x0 + z_beam * tx, y0 + z_beam * ty, z_beam );
      auto beam_dir = Vec( tx, ty, 1.f );

      auto n_hits   = simd::int_v( &tracks.n_hits[t] );
      int  max_hits = n_hits.hmax( loop_mask );

      // Store backward tracks
      int i = tracksBackward.size();

      tracksBackward.compressstore_nHits( i, backwards, n_hits );
      tracksBackward.compressstore_statePos( i, 0, backwards, beam_pos );
      tracksBackward.compressstore_stateDir( i, 0, backwards, beam_dir );
      tracksBackward.compressstore_stateCovX( i, 0, backwards, cov );
      tracksBackward.compressstore_stateCovY( i, 0, backwards, cov );

      for ( int h = 0; h < max_hits; h++ ) {
        tracksBackward.compressstore_hit( i, h, backwards,
                                          simd::int_v( (int*)&tracks.cId[h * MAX_TRACKS_PER_EVENT + t] ) );
      }

      tracksBackward.size() += simd::popcount( backwards );

      // Store forward tracks
      auto forwards = ( !backwards ) && loop_mask;
      i             = tracksForward.size();

      tracksForward.compressstore_nHits( i, forwards, n_hits );
      tracksForward.compressstore_statePos( i, 0, forwards, beam_pos );
      tracksForward.compressstore_stateDir( i, 0, forwards, beam_dir );
      tracksForward.compressstore_stateCovX( i, 0, forwards, cov );
      tracksForward.compressstore_stateCovY( i, 0, forwards, cov );

      auto end_pos =
          Vec( x0 + StateParameters::ZEndVelo * tx, y0 + StateParameters::ZEndVelo * ty, StateParameters::ZEndVelo );

      tracksForward.compressstore_statePos( i, 1, forwards, end_pos );
      tracksForward.compressstore_stateDir( i, 1, forwards, beam_dir );
      tracksForward.compressstore_stateCovX( i, 1, forwards, cov );
      tracksForward.compressstore_stateCovY( i, 1, forwards, cov );

      for ( int h = 0; h < max_hits; h++ ) {
        tracksForward.compressstore_hit( i, h, forwards,
                                         simd::int_v( (int*)&tracks.cId[h * MAX_TRACKS_PER_EVENT + t] ) );
      }

      tracksForward.size() += simd::popcount( forwards );
    }

    m_nbClustersCounter += hits.size();
    m_nbTracksCounter += n_tracks_output;

    return result;
  }
} // namespace LHCb::Pr::Velo
