/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// LHCb
#include "Event/StateParameters.h"
#include "Event/Track.h"
#include "Kernel/VPConstants.h"
#include "VPKernel/PixelUtils.h"
// Local
#include "VSPClus.h"
#include "fastAtan2.h"
#include <VPDet/VPDetPaths.h>
#include <iomanip>

DECLARE_COMPONENT_WITH_ID( LHCb::Pr::Velo::VSPClus, "VSPClus" )
namespace LHCb::Pr::Velo {

  namespace {
    using namespace Pixel;
    /// Maximum allowed cluster size (no effect when running on lite clusters).
    constexpr unsigned int s_maxClusterSize = 999999999;
    struct SensorCache {
      // representing the transformed pitch vectors
      float xx, xy, yx, yy;
      // coordinates (xi, yi) of pixel in lower left corner of chip
      std::array<float, VeloInfo::Numbers::NChipCorners> ChipCorners;
      // sensor z position
      float z;
    };
    struct SPCache {
      std::array<float, 4> fxy;
      unsigned char        pattern;
      unsigned char        nx1;
      unsigned char        nx2;
      unsigned char        ny1;
      unsigned char        ny2;
    };

    // 0 1 2 3
    // 4 5 6 7

    auto make_linkNS() {
      std::array<uint8_t, 256> linkNS{};
      // Build LUT for North-South connection test
      for ( int data = 0; data < 256; data++ ) {
        std::bitset<8> sp( data );
        linkNS[data] = ( sp[4] && ( sp[0] || sp[1] ) ) || ( sp[5] && ( sp[0] || sp[1] || sp[2] ) ) ||
                       ( sp[6] && ( sp[1] || sp[2] || sp[3] ) ) || ( sp[7] && ( sp[2] || sp[3] ) );
      }
      return linkNS;
    }

    static const std::array<uint8_t, 256> _linkNS = make_linkNS();

    //=========================================================================
    // Cache Super Pixel cluster patterns.
    //=========================================================================
    std::array<SPCache, 256> cacheSPPatterns() {
      std::array<SPCache, 256> SPCaches{};
      // create a cache for all super pixel cluster patterns.
      // this is an unoptimized 8-way flood fill on the 8 pixels
      // in the super pixel.
      // no point in optimizing as this is called once in
      // initialize() and only takes about 20 us.

      // define deltas to 8-connectivity neighbours
      const int dx[] = {-1, 0, 1, -1, 0, 1, -1, 1};
      const int dy[] = {-1, -1, -1, 1, 1, 1, 0, 0};

      // clustering buffer for isolated superpixels.
      unsigned char sp_buffer[8];

      // SP index buffer and its size for single SP clustering
      unsigned char sp_idx[8];
      unsigned char sp_idx_size = 0;

      // stack and stack pointer for single SP clustering
      unsigned char sp_stack[8];
      unsigned char sp_stack_ptr = 0;

      // loop over all possible SP patterns
      for ( unsigned int sp = 0; sp < 256; ++sp ) {
        sp_idx_size = 0;
        for ( unsigned int shift = 0; shift < 8; ++shift ) {
          const unsigned char p = sp & ( 1 << shift );
          sp_buffer[shift]      = p;
          if ( p ) { sp_idx[sp_idx_size++] = shift; }
        }

        // loop over pixels in this SP and use them as
        // cluster seeds.
        // note that there are at most two clusters
        // in a single super pixel!
        unsigned char clu_idx = 0;
        for ( unsigned int ip = 0; ip < sp_idx_size; ++ip ) {
          unsigned char idx = sp_idx[ip];

          if ( 0 == sp_buffer[idx] ) { continue; } // pixel is used

          sp_stack_ptr             = 0;
          sp_stack[sp_stack_ptr++] = idx;
          sp_buffer[idx]           = 0;
          unsigned char x          = 0;
          unsigned char y          = 0;
          unsigned char n          = 0;

          while ( sp_stack_ptr ) {
            idx                     = sp_stack[--sp_stack_ptr];
            const unsigned char row = idx % 4;
            const unsigned char col = idx / 4;
            x += col;
            y += row;
            ++n;

            for ( unsigned int ni = 0; ni < 8; ++ni ) {
              const char ncol = col + dx[ni];
              if ( ncol < 0 || ncol > 1 ) continue;
              const char nrow = row + dy[ni];
              if ( nrow < 0 || nrow > 3 ) continue;
              const unsigned char nidx = ( ncol << 2 ) | nrow;
              if ( 0 == sp_buffer[nidx] ) continue;
              sp_stack[sp_stack_ptr++] = nidx;
              sp_buffer[nidx]          = 0;
            }
          }

          const uint32_t cx = x / n;
          const uint32_t cy = y / n;
          const float    fx = x / static_cast<float>( n ) - cx;
          const float    fy = y / static_cast<float>( n ) - cy;

          // store the centroid pixel
          SPCaches[sp].pattern |= ( ( cx << 2 ) | cy ) << 4 * clu_idx;

          // set the two cluster flag if this is the second cluster
          SPCaches[sp].pattern |= clu_idx << 3;

          // set the pixel fractions
          SPCaches[sp].fxy[2 * clu_idx]     = fx;
          SPCaches[sp].fxy[2 * clu_idx + 1] = fy;

          // increment cluster count. note that this can only become 0 or 1!
          ++clu_idx;
        }
      }
      return SPCaches;
    }
    // SP pattern buffers for clustering, cached once.
    // There are 256 patterns and there can be at most two
    // distinct clusters in an SP.
    static const std::array<SPCache, 256> s_SPCaches = cacheSPPatterns();

    //=============================================================================
    // Link Horizontal : test if there is a link between two horizontal adjacent SP
    // 0 1 2 3 | 0 1 2 3
    // 4 5 6 7 | 4 5 6 7
    //=============================================================================
    constexpr auto linkH( uint8_t l, uint8_t r ) { return ( l & 0x88 ) && ( r & 0x11 ); }

    //=============================================================================
    // Link Diagonal Forward : test if there is a link between two SW-NE adjacent
    // SP
    //         | 0 1 2 3
    //         | 4 5 6 7
    // --------|--------
    // 0 1 2 3 |
    // 4 5 6 7 |
    //=============================================================================
    constexpr auto linkDF( uint8_t sw, uint8_t ne ) { return ( sw & 0x08 ) && ( ne & 0x10 ); }

    //=============================================================================
    // Link Diagonal Backward : test if there is a link between two NW-SE adjacent
    // SP
    // 0 1 2 3 |
    // 4 5 6 7 |
    // --------|--------
    //         | 0 1 2 3
    //         | 4 5 6 7
    //=============================================================================
    constexpr auto linkDB( uint8_t nw, uint8_t se ) { return ( nw & 0x80 ) && ( se & 0x01 ); }

    //=============================================================================
    // Link Vertical : test if there is a link between two vertical adjacent SP
    // 0 1 2 3
    // 4 5 6 7
    // -------
    // 0 1 2 3
    // 4 5 6 7
    //=============================================================================
    constexpr auto linkV( uint8_t b, uint8_t t ) { return _linkNS[( b & 0xF0 ) | ( t & 0x0F )]; }

  } // namespace
  //=============================================================================
  // Standard constructor, initializes variables
  //=============================================================================
  VSPClus::VSPClus( const std::string& name, ISvcLocator* pSvcLocator )
      : MultiTransformer( name, pSvcLocator, KeyValue{"RawEventLocation", RawEventLocation::Default},
                          {KeyValue{"ClusterLocation", VPClusterLocation::Light},
                           KeyValue{"ClusterOffsets", VPClusterLocation::Offsets}} ) {}

  //=============================================================================
  // Initialization
  //=============================================================================
  StatusCode VSPClus::initialize() {

    StatusCode sc = MultiTransformer::initialize();
    if ( sc.isFailure() ) return sc;

    // declare the condition derivation
    const bool withAlignment = exist<Condition>( detSvc(), LHCb::Det::VP::module_align );
    VPGeometry::registerDerivation( conditionDerivationMgr(), m_vp.key(), withAlignment );

    return sc;
  }
  //=============================================================================
  // Main execution
  //=============================================================================
  std::tuple<std::vector<VPLightCluster>, std::array<unsigned, VeloInfo::Numbers::NOffsets>> VSPClus::
                                                                                             operator()( const EventContext& ctx, const RawEvent& rawEvent ) const {
    const auto& vp = m_vp.get( getConditionContext( ctx ) );

    const auto& tBanks = rawEvent.banks( RawBank::VP );
    if ( tBanks.empty() ) return {};

    const unsigned int version = ( *tBanks.begin() )->version();
    if ( version != 2 ) {
      warning() << "Unsupported raw bank version (" << version << ")" << endmsg;
      return {};
    }
    // WARNING:
    // This is a rather long function. Please refrain from breaking this
    // up into smaller functions as this will severely impact the
    // timing performance. And yes, this has been measured. Just don't.

    // Clustering buffers
    std::array<uint8_t, VP::NPixelsPerSensor / 8> buffer{};
    std::array<uint8_t, VP::NPixelsPerSensor / 8> use{};
    std::vector<uint32_t>                         pixel_idx;
    std::vector<uint32_t>                         stack;

    // reserve a minimal stack
    stack.reserve( 64 );

    // Since the pool is local, to first preallocate the pool, then count hits per module,
    // and then preallocate per module and move hits might not be faster than adding
    // directly to the PixelModuleHits (which would require more allocations, but
    // not many if we start with a sensible default)
    std::vector<VPLightCluster>                       pool;
    std::array<unsigned, VeloInfo::Numbers::NOffsets> offsets{};
    const unsigned int                                startSize = 10000U;
    pool.reserve( startSize );

    uint32_t module_offset = 0;
    uint32_t module_offset_prev = 0;

    // Loop over VP RawBanks
    for ( const auto& tBank : tBanks ) {

      const unsigned int sensor = tBank->sourceID();
      const unsigned int module = 1 + ( sensor / VP::NSensorsPerModule );
      if ( m_modulesToSkipMask[module - 1] ) { continue; }
      // reset and then fill the super pixel buffer for a sensor
      // memset(m_sp_buffer,0,256*256*3*sizeof(unsigned char));
      // the memset is too slow here. the occupancy is so low that
      // resetting a few elements is *much* faster.
      const unsigned int nrc = pixel_idx.size();
      for ( unsigned int irc = 0; irc < nrc; ++irc ) { buffer[pixel_idx[irc]] = 0; }
      pixel_idx.clear();

      const auto& ltg = vp.m_ltg[sensor];

      const uint32_t* bank = tBank->data();
      const uint32_t  nsp = *bank++;

      if ( 4 * ( nsp + 1 ) != static_cast<uint32_t>( tBank->size() ) ) {
        error() << "inconsistent bank size: " << nsp << " pixels, expected size: " << 4 * nsp + 4
                << " actual size: " << tBank->size() << endmsg;
      }

      for ( unsigned int i = 0; i < nsp; ++i ) {
        const uint32_t sp_word = *bank++;
        uint8_t        sp = sp_word & 0xFFU;

        if ( 0 == sp ) continue; // protect against zero super pixels.

        const uint32_t sp_addr = ( sp_word & 0x007FFF00U ) >> 8;
        const uint32_t sp_row = sp_addr & 0x3FU;
        const uint32_t sp_col = ( sp_addr >> 6 );
        const uint32_t no_sp_neighbours = sp_word & 0x80000000U;

        // if a super pixel is isolated the clustering boils
        // down to a simple pattern look up.
        // don't do this if we run in offline mode where we want to record all
        // contributing channels; in that scenario a few more us are negligible
        // compared to the complication of keeping track of all contributing
        // channel IDs.
        if ( no_sp_neighbours ) {
          const auto&    spcache = s_SPCaches[sp];
          const uint32_t idx = spcache.pattern;

          // there is always at least one cluster in the super
          // pixel. look up the pattern and add it.
          const uint32_t row = idx & 0x03U;
          const uint32_t col = ( idx >> 2 ) & 1;
          const uint32_t cx = sp_col * 2 + col;
          const uint32_t cy = sp_row * 4 + row;
          const uint32_t chip = cx / CHIP_COLUMNS;
          const uint32_t ccol = cx % CHIP_COLUMNS;

          VPChannelID cid( sensor, chip, ccol, cy );

          const float fx = spcache.fxy[0];
          const float fy = spcache.fxy[1];
          const float local_x = vp.m_local_x[cx] + fx * vp.m_x_pitch[cx];
          const float local_y = ( cy + 0.5f + fy ) * vp.m_pixel_size;

          const float gx = ( ltg[0] * local_x + ltg[1] * local_y + ltg[9] );
          const float gy = ( ltg[3] * local_x + ltg[4] * local_y + ltg[10] );
          const float gz = ( ltg[6] * local_x + ltg[7] * local_y + ltg[11] );

          VPLightCluster vplc( 1, 1, gx, gy, gz, cid );
          pool.push_back( vplc );
          module_offset++;

          // if there is a second cluster for this pattern
          // add it as well.
          if ( idx & 8 ) {
            const uint32_t row = ( idx >> 4 ) & 3;
            const uint32_t col2 = ( idx >> 6 ) & 1;
            const uint32_t cy = sp_row * 4 + row;

            // to get rid of the div use old ccol
            const uint32_t ccol2 = ccol + ( col2 - col );

            VPChannelID cid( sensor, chip, ccol2, cy );

            const float fx = spcache.fxy[2];
            const float fy = spcache.fxy[3];
            const float local_x = vp.m_local_x[cx] + fx * vp.m_x_pitch[cx];
            const float local_y = ( cy + 0.5f + fy ) * vp.m_pixel_size;

            const float gx = ( ltg[0] * local_x + ltg[1] * local_y + ltg[9] );
            const float gy = ( ltg[3] * local_x + ltg[4] * local_y + ltg[10] );
            const float gz = ( ltg[6] * local_x + ltg[7] * local_y + ltg[11] );

            pool.emplace_back( 1, 1, gx, gy, gz, cid );
            module_offset++;
          }
          continue; // move on to next super pixel
        }

        // Add the SP to the image buffer
        const uint32_t idx = ( sp_row * 384 ) + sp_col;
        buffer[idx] = sp;
        use[idx]    = 1;
        pixel_idx.push_back( idx );

      } // loop over super pixels in raw bank

      // the sensor buffer is filled, perform the clustering on
      // clusters that span several super pixels.
      const unsigned int nidx = pixel_idx.size();
      for ( unsigned int irc = 0; irc < nidx; ++irc ) {

        const uint32_t idx = pixel_idx[irc];

        if ( !use[idx] ) continue; // pixel is used in another cluster

        // 8-way row scan optimized seeded flood fill from here.
        stack.clear();

        // mark seed as used
        use[idx] = 0;

        // initialize sums
        unsigned int x = 0;
        unsigned int y = 0;
        unsigned int n = 0;

        // push seed on stack
        stack.push_back( idx );

        // as long as the stack is not exhausted:
        // - pop the stack and add popped pixel to cluster
        // - scan the row to left and right, adding set pixels
        //   to the cluster and push set pixels above and below
        //   on the stack (and delete both from the pixel buffer).
        while ( !stack.empty() ) {

          // pop pixel from stack and add it to cluster
          const uint32_t idx = stack.back();
          stack.pop_back();
          const uint32_t row = idx / 384;
          const uint32_t col = idx % 384;

          const uint8_t data = buffer[idx];

          auto&    spcache = s_SPCaches[data];
          uint32_t pat = spcache.pattern;
          uint32_t r = pat & 0x03U;
          uint32_t c = ( pat >> 2 ) & 1;

          x += col * 2 + c;
          y += row * 4 + r;
          ++n;

          // check up and down
          uint32_t u_idx = idx + 384;
          if ( row < ( VP::NRows / 4 - 1 ) && use[u_idx] ) {
            if ( linkH( data, buffer[u_idx] ) ) {
              use[u_idx] = false;
              stack.push_back( u_idx );
            }
          }
          uint32_t d_idx = idx - 384;
          if ( row > 0 && use[d_idx] ) {
            if ( linkH( buffer[d_idx], data ) ) {
              use[d_idx] = false;
              stack.push_back( d_idx );
            }
          }

          // scan row to the right
          uint8_t  cur_data = data;
          uint32_t nidx = idx;
          for ( unsigned int c = col + 1; c < VP::NSensorColumns / 2; ++c ) {
            nidx++;
            u_idx++;
            d_idx++;

            // check up and down (diagonal)
            if ( row < ( VP::NRows / 4 - 1 ) && use[u_idx] ) {
              if ( linkDF( cur_data, buffer[u_idx] ) ) {
                use[u_idx] = false;
                stack.push_back( u_idx );
              }
            }
            if ( row > 0 && use[d_idx] ) {
              if ( linkDB( buffer[d_idx], cur_data ) ) {
                use[d_idx] = false;
                stack.push_back( d_idx );
              }
            }

            // add set pixel to cluster or stop scanning
            if ( use[nidx] && linkV( cur_data, buffer[nidx] ) ) {
              use[nidx] = false;
              cur_data = buffer[nidx];

              // check up and down
              if ( row < ( VP::NRows / 4 - 1 ) && use[u_idx] ) {
                if ( linkH( cur_data, buffer[u_idx] ) ) {
                  use[u_idx] = false;
                  stack.push_back( u_idx );
                }
              }
              if ( row > 0 && use[d_idx] ) {
                if ( linkH( buffer[d_idx], cur_data ) ) {
                  use[d_idx] = false;
                  stack.push_back( d_idx );
                }
              }

              auto&    spcache = s_SPCaches[cur_data];
              uint32_t pat = spcache.pattern;
              uint32_t rr = pat & 0x03U;
              uint32_t cc = ( pat >> 2 ) & 1;

              x += c * 2 + cc;
              y += row * 4 + rr;
              ++n;
            } else {
              break;
            }
          }

          // scan row to the left
          cur_data = data;
          u_idx = idx + 384;
          nidx  = idx;
          d_idx = idx - 384;
          for ( int c = col - 1; c >= 0; --c ) {
            nidx--;
            u_idx--;
            d_idx--;

            // check up and down (diagonal)
            if ( row < ( VP::NRows / 4 - 1 ) && use[u_idx] ) {
              if ( linkDB( cur_data, buffer[u_idx] ) ) {
                use[u_idx] = false;
                stack.push_back( u_idx );
              }
            }
            if ( row > 0 && use[d_idx] ) {
              if ( linkDF( buffer[d_idx], cur_data ) ) {
                use[d_idx] = false;
                stack.push_back( d_idx );
              }
            }

            // add set pixel to cluster or stop scanning
            if ( use[nidx] && linkV( buffer[nidx], cur_data ) ) {
              use[nidx] = false;
              cur_data = buffer[nidx];

              // check up and down
              if ( row < ( VP::NRows / 4 - 1 ) && use[u_idx] ) {
                if ( linkH( cur_data, buffer[u_idx] ) ) {
                  use[u_idx] = false;
                  stack.push_back( u_idx );
                }
              }
              if ( row > 0 && use[d_idx] ) {
                if ( linkH( buffer[d_idx], cur_data ) ) {
                  use[d_idx] = false;
                  stack.push_back( d_idx );
                }
              }

              auto&    spcache = s_SPCaches[cur_data];
              uint32_t pat = spcache.pattern;
              uint32_t rr = pat & 0x03U;
              uint32_t cc = ( pat >> 2 ) & 1;

              x += c * 2 + cc;
              y += row * 4 + rr;
              ++n;
            } else {
              break;
            }
          }
        } // while the stack is not empty

        // we are done with this cluster, calculate
        // centroid pixel coordinate and fractions.
        if ( n <= s_maxClusterSize ) {
          // if the pixel is smaller than the max cluster size, store it for the tracking
          const unsigned int cx = x / n;
          const unsigned int cy = y / n;

          const uint32_t chip = cx / CHIP_COLUMNS;
          const uint32_t ccol = cx % CHIP_COLUMNS;

          // store target (3D point for tracking)
          VPChannelID cid( sensor, chip, ccol, cy );

          const float fx = x / static_cast<float>( n ) - cx;
          const float fy = y / static_cast<float>( n ) - cy;
          const float local_x = vp.m_local_x[cx] + fx * vp.m_x_pitch[cx];
          const float local_y = ( cy + 0.5f + fy ) * vp.m_pixel_size;

          const float gx = ( ltg[0] * local_x + ltg[1] * local_y + ltg[9] );
          const float gy = ( ltg[3] * local_x + ltg[4] * local_y + ltg[10] );
          const float gz = ( ltg[6] * local_x + ltg[7] * local_y + ltg[11] );

          VPLightCluster vplc( 1, 1, gx, gy, gz, cid );
          pool.push_back( vplc );
          module_offset++;
        }
      } // loop over all potential seed pixels

      if ( sensor % VP::NSensorsPerModule == VP::NSensorsPerModule - 1 ) {

        // sorting in phi for even modules
        auto cmp_phi_for_odd_modules = []( const VPLightCluster& a, const VPLightCluster& b ) {
          return ( a.y() < 0.f && b.y() > 0.f ) ||
                 // same y side even and odd modules, check y1/x1 < y2/x2
                 ( ( a.y() * b.y() ) > 0.f && ( a.y() * b.x() < b.y() * a.x() ) );
        };

        // sorting in phi for odd modules
        auto cmp_phi_for_even_modules = []( const VPLightCluster& a, const VPLightCluster& b ) {
          return ( a.y() > 0.f && b.y() < 0.f ) ||
                 // same y side even and odd modules, check y1/x1 < y2/x2
                 ( ( a.y() * b.y() ) > 0.f && ( a.y() * b.x() < b.y() * a.x() ) );
        };

        auto sort_module = [&pool, &module_offset, module_offset_prev]( auto cmp ) {
          std::sort( pool.begin() + module_offset_prev, pool.begin() + module_offset, cmp );
        };

        if ( ( module - 1 ) % 2 == 1 ) {
          sort_module( cmp_phi_for_odd_modules );
        } else {
          sort_module( cmp_phi_for_even_modules );
        }
        module_offset_prev = module_offset;
        offsets[module] = module_offset;
      }
    } // loop over all banks

    m_nbClustersCounter += pool.size();
    return std::make_tuple( std::move( pool ), std::move( offsets ) );
  }
} // namespace LHCb::Pr::Velo
