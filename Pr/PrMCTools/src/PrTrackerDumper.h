

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRTRACKERDUMPER_H
#define PRTRACKERDUMPER_H 1

#include <cstring>
#include <fstream>
#include <string>

// Include files
#include "Event/MCParticle.h"
#include "Event/MCTrackInfo.h"
#include "Event/MCVertex.h"
#include "Event/ODIN.h"
#include "Event/VPLightCluster.h"
#include "GaudiAlg/Consumer.h"
#include "Linker/LinkerWithKey.h"
#include "PrKernel/PrFTHitHandler.h"
#include "PrKernel/UTHitHandler.h"
#include "PrKernel/UTHitInfo.h"

/** @class PrTrackerDumper PrTrackerDumper.h
 *  TupleTool storing all VPClusters position on tracks (dummy track for noise ones)
 *
 *  @author Renato Quagliani
 *  @date   2017-11-06
 */
/*

*/

class PrTrackerDumper
    : public Gaudi::Functional::Consumer<void( const LHCb::MCParticles&, const std::vector<LHCb::VPLightCluster>&,
                                               const PrFTHitHandler<PrHit>&, const UT::HitHandler&, const LHCb::ODIN&,
                                               const LHCb::LinksByKey& )> {
public:
  /// Standard constructor
  PrTrackerDumper( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;

  void write_MCP_info_to_binary_file(
      const int key, const int pid, const float p, const float pt, const float eta, const float phi, const float ovtx_x,
      const float ovtx_y, const float ovtx_z, const bool isLong, const bool isDown, const bool hasVelo,
      const bool hasUT, const bool hasSciFi, const bool fromBeautyDecay, const bool fromCharmDecay,
      const bool fromStrangeDecay, const int mother_key, const int DecayOriginMother_key,
      const int DecayOriginMother_pid, const float DecayOriginMother_pt, const float DecayOriginMother_tau,
      const float charge, const std::vector<unsigned int> Velo_lhcbID, const std::vector<unsigned int> UT_lhcbID,
      const std::vector<unsigned int> SciFi_lhcbID, const unsigned int nPrim, std::ofstream& outfile ) const;

  void operator()( const LHCb::MCParticles& MCParticles, const std::vector<LHCb::VPLightCluster>& VPClusters,
                   const PrFTHitHandler<PrHit>& ftHits, const UT::HitHandler& utHits, const LHCb::ODIN& odin,
                   const LHCb::LinksByKey& links ) const override;

  int                   mcVertexType( const LHCb::MCParticle& particle ) const;
  const LHCb::MCVertex* findMCOriginVertex( const LHCb::MCParticle& particle,
                                            const double            decaylengthtolerance = 1.e-3 ) const;

private:
  Gaudi::Property<std::string> m_outputDirectory{this, "OutputDirectory", "TrackerDumper"};
  Gaudi::Property<std::string> m_MCOutputDirectory{this, "MCOutputDirectory", "MC_info/tracks"};
  Gaudi::Property<bool>        m_writeBinary{this, "DumpToBinary", false};
  Gaudi::Property<bool>        m_writeROOT{this, "DumpToROOT", true};
};
#endif // PRTRACKERDUMPER_H
