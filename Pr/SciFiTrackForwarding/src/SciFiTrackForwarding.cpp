/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/StateParameters.h"
#include "Event/Track_v2.h"
#include "GaudiAlg/Transformer.h"
#include "Kernel/ILHCbMagnetSvc.h"
#include "PrKernel/PrFTZoneHandler.h"
#include "PrKernel/PrSelection.h"
#include "SciFiTrackForwardingHits.h"
#include "vdt/log.h"
#include <limits>
#include <string>
#include <tuple>

#include "Event/PrForwardTracks.h"
#include "Event/PrUpstreamTracks.h"

/** @class SciFiTrackForwarding SciFiTrackForwarding.cpp
 *
 *  \brief Alternative algorithm for the HLT1 Forward tracking of the upgrade
 *
 *   The general approach is to find a hit in each x-layer in station 3
 *   which is used to form a track seed from which one can forward this track
 *   into the other stations.
 *
 *   For further information see presentations in
 *   - https://indico.cern.ch/event/810764/
 *   - https://indico.cern.ch/event/786084/
 *
 *   Most important FIXMEs:
 * - Whenever a lighter eventmodel is used, it will be a good idea to vectorize the linear and binary search for further
 *   speedups
 *
 * - The currently employed linear fit leaves a couple things to be desired. Thus this should at some point be replaced
 *   by a better solution. Mainly we don't get a real y-slope or offset since we only fit x-hits, but it's resolution
 *   for high momentum tracks could be better which leads to a momentum resolution of up to ~1.8% while something closer
 *   to ~1% should be possible
 */

///////////////////////////////////////////////////////////////////////////////
// anonymous namespace for helper functions local to this compilation unit
///////////////////////////////////////////////////////////////////////////////
namespace {
  using TracksUT = LHCb::Pr::Upstream::Tracks;
  using TracksFT = LHCb::Pr::Forward::Tracks;

  // average z-position of the kink position inside the magnet (tuned on MC)
  float constexpr zPosKinkMagnet = 5282.5f;

  // constants for extrapolation polynomials from x hit in S3L0
  // to the corresponding x hit in other stations and layers
  double constexpr ExtFac_alpha = 1.4706824654297932e-5;
  double constexpr ExtFac_beta  = -3.152273942420754e-09;
  double constexpr ExtFac_gamma = -0.0003351012155394656;

  // constants for the extrapolation polynomial from Velo to SciFi, used to determine searchwindows and minPT cut border
  std::array<float, 8> constexpr toSciFiExtParams{4824.31956565f,  426.26974766f,   7071.08408876f, 12080.38364257f,
                                                  14077.79607408f, 13909.31561208f, 9315.34184959f, 3209.49021545f};

  // constants for the y-correction polynomial
  std::array<float, 8> constexpr deltaYParams{3.78837f, 73.1636f, 7353.89f,  -6347.68f,
                                              20270.3f, 3721.02f, -46038.2f, 230943.f};

  // parameters used for the momentum determination at the very end
  std::array<float, 8> constexpr MomentumParams{1239.4073749458162, 486.05664058906814, 6.7158701518424815,
                                                632.7283787142547,  2358.5758035677504, -9256.27946160669,
                                                241.4601040854867,  42.04859549174048};

  // Parameters for the linear discriminant which we use to reject ghosts
  std::array<float, 8> LDAParams{5.35770606, 1.22786675, -1.17615119, 2.41396581,
                                 2.09624748, 1.7029565,  -2.69818528, 1.88471171};

  int constexpr maxint = std::numeric_limits<int>::max();

  // internal helper class to keep track of our best candidate
  struct SciFiTrackForwardingCand {
    // array used to store indices of hits
    std::array<int, 12> ids{maxint, maxint, maxint, maxint, maxint, maxint};
    float               PQ{0.f};        // Momentum times charge of candidate
    float               finaltx{0};     // x-slope at the first x-hit in station 3 after linear fit
    float               newx0{0};       // x position of x-hit in S3 after linear fit
    float               quality{-4.5f}; // the default value of quality acts as cut in the later selection of candidates
    int                 numHits{0};     // number of hits this candidate holds
  };

  // Simple binary search
  // FIXME the std binary search is pretty much as bad as it gets lets replace this as soon as makelhcbtracks is gone
  [[using gnu: pure]] int inline getUpperBoundBinary( std::vector<float> const& vec, int const start, int const end,
                                                      float const val ) noexcept {
    return std::distance( vec.begin(), std::upper_bound( vec.begin() + start, vec.begin() + end, val ) );
  }

  // Simple linear search
  // since my vector container is padded by sentinels I can get rid of a check in the loop and simply advance until
  // condition is met
  [[using gnu: pure]] int inline getUpperBoundLinear( std::vector<float> const& vec, int start,
                                                      float const val ) noexcept {
    // FIXME unfortunately gcc doesn't want to vectorize this for me, let's get back to that at some point
    // at the very least we can probably do better by unrolling this
    for ( ;; ) {
      if ( vec[start] > val ) return start;
      ++start;
    }
  }

  // shared implementation between linear and binary hit search
  [[using gnu: pure]] std::tuple<int, float> __Impl_SearchLayerForHit( std::vector<float> const& hits,
                                                                       int const upperIdx, float const prediction ) {
    if ( ( hits[upperIdx] - prediction ) < ( prediction - hits[upperIdx - 1] ) ) {
      return {upperIdx, ( hits[upperIdx] - prediction )};
    } else {
      return {upperIdx - 1, ( prediction - hits[upperIdx - 1] )};
    }
  }

  [[using gnu: pure]] std::tuple<int, float> __Impl_SearchLayerForHitABS( std::vector<float> const& hits,
                                                                          int const upperIdx, float const prediction ) {
    if ( std::abs( hits[upperIdx] - prediction ) < std::abs( hits[upperIdx - 1] - prediction ) ) {
      return {upperIdx, std::abs( hits[upperIdx] - prediction )};
    } else {
      return {upperIdx - 1, std::abs( hits[upperIdx - 1] - prediction )};
    }
  }

  // given the hits of a station are in the vector hits+StartOffset ---- hits+EndOffset
  // this will find the closest x-hit to the x-prediction using binary search
  [[using gnu: pure]] std::tuple<int, float> SearchLayerForHitBinary( std::vector<float> const& hits,
                                                                      int const StartOffset, int const EndOffset,
                                                                      float const prediction ) {
    int const upperIdx = getUpperBoundBinary( hits, StartOffset, EndOffset, prediction );
    return __Impl_SearchLayerForHit( hits, upperIdx, prediction );
  }

  // same as above, although we use a linear search, EndOffset is not needed as the sentinel value ends the range
  [[using gnu: pure]] std::tuple<int, float> SearchLayerForHitLinear( std::vector<float> const& hits,
                                                                      int const StartOffset, float const prediction ) {
    int const upperIdx = getUpperBoundLinear( hits, StartOffset, prediction );
    return __Impl_SearchLayerForHit( hits, upperIdx, prediction );
  }

  [[using gnu: pure]] std::tuple<int, float>
  SearchLayerForHitLinearABS( std::vector<float> const& hits, int const StartOffset, float const prediction ) {
    int const upperIdx = getUpperBoundLinear( hits, StartOffset, prediction );
    return __Impl_SearchLayerForHitABS( hits, upperIdx, prediction );
  }

  // helper function to fill a vector with some information later used in the linear fit
  // only to reduce code duplication and readability later on
  [[using gnu: always_inline]] void inline fillFitVec( std::array<float, 24>& fitvec, int& fitcnt, float const x,
                                                       float const zdelta, float const curve, float const residual ) {
    fitvec[fitcnt++] = x;
    fitvec[fitcnt++] = zdelta;
    fitvec[fitcnt++] = curve;
    fitvec[fitcnt++] = residual;
  }

} // namespace

///////////////////////////////////////////////////////////////////////////////
//
//  Fast Upgrade Forward Tracking Algorithm
//
//  The general approach is to find a hit in each x-layer in station 3
//  which is used to form a track seed from which one can forward this track
//  into the other stations.
//
//  For further information see presentations in
//  - https://indico.cern.ch/event/810764/
//  - https://indico.cern.ch/event/786084/
//
//
///////////////////////////////////////////////////////////////////////////////
class SciFiTrackForwarding
    : public Gaudi::Functional::Transformer<TracksFT( SciFiTrackForwardingHits const&, TracksUT const& )> {

public:
  SciFiTrackForwarding( std::string const& name, ISvcLocator* pSvcLocator )
      : Transformer(
            name, pSvcLocator,
            {KeyValue{"HitsLocation", "Rec/SciFiTrackForwarding/Hits"}, KeyValue{"InputTracks", "Rec/Track/UT"}},
            KeyValue{"Output", "Rec/Track/FT"} ) {}

  TracksFT operator()( SciFiTrackForwardingHits const&, TracksUT const& ) const override;

  StatusCode initialize() override {
    auto sc = Transformer::initialize();
    if ( sc.isFailure() ) return sc;

    m_magFieldSvc = svc<ILHCbMagnetSvc>( "MagneticFieldSvc", true );
    registerCondition<SciFiTrackForwarding>( PrFTInfo::FTZonesLocation, m_zoneHandler );

    // get the layer z-positions and slopes to cache them for later usage
    for ( int i{0}; i < 12; ++i ) {
      // the first 12 values are the bottom layers
      m_LayerZPos[i] = m_zoneHandler->zone( 2 * i ).z();
      m_dZdYFiber[i] = m_zoneHandler->zone( 2 * i ).dzDy();

      // the last 12 values are the top layers
      m_LayerZPos[12 + i] = m_zoneHandler->zone( 2 * i + 1 ).z();
      m_dZdYFiber[12 + i] = m_zoneHandler->zone( 2 * i + 1 ).dzDy();
    }

    // get the uv layer fiber slopes to cache them for later usage
    for ( int i{0}; i < 6; ++i ) {
      // the first 6 values are the bottom uv layers
      m_dXdYFiber[i] = m_zoneHandler->zone( PrFTInfo::stereoZones[2 * i] ).dxDy();
      // the last 6 values are the top uv layers
      m_dXdYFiber[6 + i] = m_zoneHandler->zone( PrFTInfo::stereoZones[2 * i + 1] ).dxDy();
    }

    // calculat some coefficients for the extrapolation into other stations and put them in a cache
    int cnt = 0;
    for ( int i : {0, 3, 4, 7} ) {
      m_ExtFacCache[cnt]        = ExtFac_alpha * std::pow( m_LayerZPos[i] - m_LayerZPos[8], 2 );
      m_ExtFacCache[12 + cnt++] = ExtFac_alpha * std::pow( m_LayerZPos[12 + i] - m_LayerZPos[20], 2 );

      m_ExtFacCache[cnt]        = ExtFac_beta * std::pow( m_LayerZPos[i] - m_LayerZPos[8], 3 );
      m_ExtFacCache[12 + cnt++] = ExtFac_beta * std::pow( m_LayerZPos[12 + i] - m_LayerZPos[20], 3 );

      m_ExtFacCache[cnt]        = ExtFac_gamma * std::pow( m_LayerZPos[i] - m_LayerZPos[8], 2 );
      m_ExtFacCache[12 + cnt++] = ExtFac_gamma * std::pow( m_LayerZPos[12 + i] - m_LayerZPos[20], 2 );
    }

    // factor of -1 because everything was trained with magdown, so for magdown we want factor = 1, magup = -1
    m_magscalefactor = -1 * m_magFieldSvc->signedRelativeCurrent();

    return sc;
  }

  mutable Gaudi::Accumulators::SummingCounter<unsigned> m_CounterOutput{this, "Created long tracks"};
  mutable Gaudi::Accumulators::Counter<>                m_CounterAccepted{this, "Accepted input tracks"};
  mutable Gaudi::Accumulators::Counter<>                m_CounterS1{this, "Search S1"};
  mutable Gaudi::Accumulators::Counter<>                m_CounterS1UV{this, "Search S1UV"};
  mutable Gaudi::Accumulators::Counter<>                m_CounterS2{this, "Search S2"};
  mutable Gaudi::Accumulators::Counter<>                m_CounterS2UV{this, "Search S2 UV"};
  mutable Gaudi::Accumulators::Counter<>                m_CounterS3{this, "Search S3"};
  mutable Gaudi::Accumulators::Counter<>                m_CounterS3UV{this, "Search S3UV"};

  Gaudi::Property<float> p_preSelPT{this, "PreSelectionPT", 400.0f};
  Gaudi::Property<float> p_minPt{this, "MinPt", 490.f};
  // if  a track exhibits a higher PT we open a symmetric search window
  // this is to counter the possibility of a wrong charge estimtate from the UT
  Gaudi::Property<float> p_wrongSignPT{this, "WrongSignPT", 2000.f};

  // parameters for the momentum dependent upper error limit on the x-search window estimation
  Gaudi::Property<float> p_UpperLimitOffset{this, "UpperLimit_offset", 100.f};
  Gaudi::Property<float> p_UpperLimitSlope{this, "UpperLimit_slope", 2800.f};
  Gaudi::Property<float> p_UpperLimitMax{this, "UpperLimit_max", 600.f};
  Gaudi::Property<float> p_UpperLimitMin{this, "UpperLimit_min", 150.f};

  // same as above for the lower limit
  Gaudi::Property<float> p_LowerLimitOffset{this, "LowerLimit_offset", 50.f};
  Gaudi::Property<float> p_LowerLimitSlope{this, "LowerLimit_slope", 1400.f};
  Gaudi::Property<float> p_LowerLimitMax{this, "LowerLimit_max", 600.f};

  // station 3 x-doublet search window paramters
  Gaudi::Property<float> p_DW_m{this, "DoubletWindow_slope", 4.25f};
  Gaudi::Property<float> p_DW_b{this, "DoubletWindow_offset", 0.48f};
  Gaudi::Property<float> p_DW_max{this, "DoubletWindow_max", 3.f};

  // UV window station 3
  Gaudi::Property<float> p_UV3W_m{this, "UV3Window_slope", 10.0f};
  Gaudi::Property<float> p_UV3W_b{this, "UV3Window_offset", 0.75f};
  // UV window station 2
  Gaudi::Property<float> p_UV2W_m{this, "UV2Window_slope", 10.0f};
  Gaudi::Property<float> p_UV2W_b{this, "UV2Window_offset", 0.75f};
  // UV window station 1
  Gaudi::Property<float> p_UV1W_m{this, "UV1Window_slope", 7.0f};
  Gaudi::Property<float> p_UV1W_b{this, "UV1Window_offset", 1.f};

  // S2L0 extrapolation window
  Gaudi::Property<float> p_S2L0W_m{this, "S2L0Window_slope", 2.0f};
  Gaudi::Property<float> p_S2L0W_b{this, "S2L0Window_offset", 2.0f};

  // S2L3 extrapolation window
  Gaudi::Property<float> p_S2L3W_m{this, "S2L3Window_slope", 1.0f};
  Gaudi::Property<float> p_S2L3W_b{this, "S2L3Window_offset", 1.8f};

  // S1L0 extrapolation window
  Gaudi::Property<float> p_S1L0W_m{this, "S1L0Window_slope", 2.6f};
  Gaudi::Property<float> p_S1L0W_b{this, "S1L0Window_offset", 3.6f};
  // S1L3 extrapolation window
  Gaudi::Property<float> p_S1L3W_m{this, "S1L3Window_slope", 2.6f};
  Gaudi::Property<float> p_S1L3W_b{this, "S1L3Window_offset", 3.5f};

private:
  // this enables me to print things automagically space separated (less typing)
  // also when enable is set to false, all of this code is removed which is nice for me to test what this actually costs
  template <bool enable = false, typename... Args>
  void mydebug( Args&&... args ) const {
    if constexpr ( enable ) {
      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) { ( ( debug() << std::forward<Args>( args ) << " " ), ... ) << endmsg; }
    }
  }

  float m_magscalefactor{std::numeric_limits<float>::signaling_NaN()};

  std::array<float, 24> m_LayerZPos{std::numeric_limits<float>::signaling_NaN()};
  std::array<float, 24> m_dZdYFiber{std::numeric_limits<float>::signaling_NaN()};
  std::array<float, 12> m_dXdYFiber{std::numeric_limits<float>::signaling_NaN()};
  std::array<float, 24> m_ExtFacCache{std::numeric_limits<float>::signaling_NaN()};

  PrFTZoneHandler* m_zoneHandler{nullptr};
  ILHCbMagnetSvc*  m_magFieldSvc{nullptr};
};

DECLARE_COMPONENT( SciFiTrackForwarding )

TracksFT SciFiTrackForwarding::operator()( SciFiTrackForwardingHits const& hithandler, TracksUT const& tracks ) const {
  TracksFT Output{tracks.getVeloAncestors(), &tracks};

  mydebug( "LayerZPos", m_LayerZPos );

  // get buffers for the counters
  auto S1buffer              = m_CounterS1.buffer();
  auto S2buffer              = m_CounterS2.buffer();
  auto S3buffer              = m_CounterS3.buffer();
  auto S3UVbuffer            = m_CounterS3UV.buffer();
  auto S2UVbuffer            = m_CounterS2UV.buffer();
  auto S1UVbuffer            = m_CounterS1UV.buffer();
  auto CounterAcceptedBuffer = m_CounterAccepted.buffer();

  // I want this in GeV but default in LHCb is MeV so this is a compromise to have the property in MeV
  float const minPTCutValue = p_minPt / 1000.0f;

  using dType = SIMDWrapper::scalar::types;
  using F     = dType::float_v;
  using I     = dType::int_v;

  // start our loop over UT tracks
  // for ( auto const& uttrack : tracks ) {
  for ( int uttrack = 0; uttrack < tracks.size(); uttrack += dType::size ) {
    // placeholder for best candidate
    SciFiTrackForwardingCand bestcandidate{};
    mydebug( "++++++++++ Start new UT track ++++++++++++++++" );

    Vec3<F> const endv_pos = tracks.statePos<F>( uttrack );
    Vec3<F> const endv_dir = tracks.stateDir<F>( uttrack );

    // everything I need from the end velo state
    float const endv_x    = endv_pos.x.cast();
    float const endv_y    = endv_pos.y.cast();
    float const endv_tx   = endv_dir.x.cast();
    float const endv_ty   = endv_dir.y.cast();
    float const endv_qp   = tracks.stateQoP<F>( uttrack ).cast();
    float const endv_z    = endv_pos.z.cast();
    float const endv_tx2  = endv_tx * endv_tx;
    float const endv_ty2  = endv_ty * endv_ty;
    float const endv_txy2 = endv_tx2 + endv_ty2;
    float const endv_txy  = std::sqrt( endv_txy2 );
    float const endv_pq   = 1.f / endv_qp;
    float const endv_p    = std::abs( endv_pq );
    float const endv_pz   = endv_p / std::sqrt( 1.f + endv_txy2 );
    float const endv_pt   = endv_pz * endv_txy;

    mydebug( "pt: ", endv_pt, "preselpt", p_preSelPT );
    if ( endv_pt < p_preSelPT ) continue;
    CounterAcceptedBuffer += 1;

    bool const TrackGoesTroughUpperHalf = ( endv_y + ( 9400.f - endv_z ) * endv_ty ) > 0;

    // extrapolate velo track y-position into 4 Layers of last station
    // IIFE (Immediately invoked function expression) to keep yAtZ const
    // compiler explorer tested that this isn't producing overhead over simple loop
    auto const yAtZ = [&]() {
      std::array<float, 12> tmp;

      // determine if track goes through upper or lower half of detector
      int cnt = TrackGoesTroughUpperHalf ? 12 : 0;

      std::transform( m_LayerZPos.begin() + cnt, m_LayerZPos.begin() + 12 + cnt, tmp.begin(),
                      [=]( float const z ) { return endv_y + ( z - endv_z ) * endv_ty; } );
      return tmp;
    }();

    mydebug( "yatz: ", yAtZ );

    // adjust the z-position to account for the predicted y-position of the track
    auto const betterZ = [&]() {
      std::array<float, 12> tmp;
      // starting point depends on track going through upper or lower half of detector
      int cnt = TrackGoesTroughUpperHalf ? 12 : 0;

      std::transform( yAtZ.begin(), yAtZ.end(), m_LayerZPos.begin() + cnt, tmp.begin(),
                      [&]( float const y, float const z ) { return z + y * m_dZdYFiber[cnt++]; } );
      return tmp;
    }();

    mydebug( "betterz: ", TrackGoesTroughUpperHalf ? " upper half" : " lower half", betterZ );

    int const ExtFac_Offset = TrackGoesTroughUpperHalf ? 12 : 0;

    float const ExtFac_alpha_S1L0 = m_ExtFacCache[ExtFac_Offset];
    float const ExtFac_beta_S1L0  = m_ExtFacCache[ExtFac_Offset + 1];
    float const ExtFac_gamma_S1L0 = m_ExtFacCache[ExtFac_Offset + 2];
    float const ExtFac_alpha_S1L3 = m_ExtFacCache[ExtFac_Offset + 3];
    float const ExtFac_beta_S1L3  = m_ExtFacCache[ExtFac_Offset + 4];
    float const ExtFac_gamma_S1L3 = m_ExtFacCache[ExtFac_Offset + 5];
    float const ExtFac_alpha_S2L0 = m_ExtFacCache[ExtFac_Offset + 6];
    float const ExtFac_beta_S2L0  = m_ExtFacCache[ExtFac_Offset + 7];
    float const ExtFac_gamma_S2L0 = m_ExtFacCache[ExtFac_Offset + 8];
    float const ExtFac_alpha_S2L3 = m_ExtFacCache[ExtFac_Offset + 9];
    float const ExtFac_beta_S2L3  = m_ExtFacCache[ExtFac_Offset + 10];
    float const ExtFac_gamma_S2L3 = m_ExtFacCache[ExtFac_Offset + 11];

    auto const dXdYLayer = [&]() {
      std::array<float, 6> tmp;
      // starting point depends on track going through upper or lower half of detector
      int cnt = TrackGoesTroughUpperHalf ? 6 : 0;
      std::transform( tmp.begin(), tmp.end(), m_dXdYFiber.begin() + cnt, tmp.begin(),
                      [=]( float const /*unused*/, float const dxdy ) { return -dxdy; } );
      return tmp;
    }();

    mydebug( "dXdYLayer", TrackGoesTroughUpperHalf ? " upper half" : " lower half", dXdYLayer );

    // delta in Z between kink position in magnet and z position of first layer of last station
    float const InvDeltaZMagS2L0 = 1.f / ( betterZ[8] - zPosKinkMagnet );

    // extrapolate velo track x-position into magnet
    float const xAtMagnet = endv_x + ( zPosKinkMagnet - endv_z ) * endv_tx;

    // charge * magnet polarity factor needed in various polynomials below
    float const factor = std::copysign( 1.f, endv_qp ) * m_magscalefactor;

    // charge over momentum in GeV
    float const qOpGeV = endv_qp * 1000.f * m_magscalefactor;

    // extrapolate the track x-position into the scifi to determine search windows
    // common term for both extrapolations below

    float const term1 =
        toSciFiExtParams[0] + endv_tx * ( -toSciFiExtParams[1] * factor + toSciFiExtParams[2] * endv_tx ) +
        endv_ty2 * ( toSciFiExtParams[3] + endv_tx * ( toSciFiExtParams[4] * factor + toSciFiExtParams[5] * endv_tx ) );

    // prediction of tracks x position with ut momentum estimate
    float const xExt = qOpGeV * ( term1 + qOpGeV * ( toSciFiExtParams[6] * endv_tx + toSciFiExtParams[7] * qOpGeV ) );

    // 1/p, given a minPT cut and the tracks slopes
    float const minInvPGeV = factor / minPTCutValue * endv_txy / std::sqrt( 1.f + endv_txy2 );
    // given the above value of 1/p this should be the tracks x-position
    // we can use this to make our windows smaller given a minPT cut
    float const minPTBorder =
        minInvPGeV * ( term1 + minInvPGeV * ( toSciFiExtParams[6] * endv_tx + toSciFiExtParams[7] * minInvPGeV ) );

    // extrapolate the velo straight into the scifi
    float const straighExt = endv_x + ( betterZ[8] - endv_z ) * endv_tx;

    // calculate an upper error boundary on the tracks x-prediction
    float const upperError = std::clamp( p_UpperLimitOffset + p_UpperLimitSlope * std::abs( qOpGeV ),
                                         p_UpperLimitMin.value(), p_UpperLimitMax.value() );

    // calculate an lower error boundary on the tracks x-prediction
    float const lowerError =
        std::min( p_LowerLimitOffset + p_LowerLimitSlope * std::abs( qOpGeV ), p_LowerLimitMax.value() );

    // depending on the sign of q set the lower and upper error in the right direction
    // on top of that make sure that the upper error isn't larger than the minPTBorder
    // and the lower error isn't bigger than xExt which means we don't look further than the straight line prediction
    float xMin = factor > 0 ? straighExt + ( ( xExt < lowerError ) ? 0 : ( xExt - lowerError ) )
                            : straighExt + std::max( xExt - upperError, minPTBorder );

    float xMax = factor > 0 ? straighExt + std::min( xExt + upperError, minPTBorder )
                            : straighExt + ( ( xExt > -lowerError ) ? 0 : ( xExt + lowerError ) );

    if ( endv_pt > p_wrongSignPT ) {
      xMin = straighExt - std::abs( xExt ) - upperError;
      xMax = straighExt + std::abs( xExt ) + upperError;
    }

    mydebug( "==================" );
    mydebug( "qOpGeV", qOpGeV, "straight: ", straighExt, "xExt ", xExt, " dxref ", minPTBorder, "lowError", lowerError,
             "upError: ", upperError, "xmin", xMin, "xmax: ", xMax, "minInvPGeV", minInvPGeV );

    int startL0, EndL0, startL3, startL1, startL2, startS2L0, startS2L3, startS2L1, startS2L2, startS1L0, startS1L1,
        startS1L3;
    int startS3L0, startS1L2;
    // set start and end points in 1D hit array depending on if a track is in upper or lower detector half

    if ( TrackGoesTroughUpperHalf ) {
      mydebug( "Selecting upper hits" );
      // FIXME binary search is faster here but I think avx2 linear search might win this
      // in any case I need to compare a sentinel branchles binary search vs avx2 linear here
      startL0 = getUpperBoundBinary( hithandler.hits, hithandler.startS3L0Up, hithandler.startS3L3Up, xMin );
      EndL0   = getUpperBoundBinary( hithandler.hits, startL0, hithandler.startS3L3Up, xMax );

      startS3L0 = hithandler.startS3L0Up;
      startL3   = hithandler.startS3L3Up;
      startL1   = hithandler.startS3L1Up;
      startL2   = hithandler.startS3L2Up;

      startS2L0 = hithandler.startS2L0Up;
      startS2L3 = hithandler.startS2L3Up;
      startS2L1 = hithandler.startS2L1Up;
      startS2L2 = hithandler.startS2L2Up;

      startS1L0 = hithandler.startS1L0Up;
      startS1L1 = hithandler.startS1L1Up;
      startS1L2 = hithandler.startS1L2Up;
      startS1L3 = hithandler.startS1L3Up;
    } else {
      mydebug( "Selecting lower hits" );
      startL0 = getUpperBoundBinary( hithandler.hits, hithandler.startS3L0Down, hithandler.startS3L3Down, xMin );
      EndL0   = getUpperBoundBinary( hithandler.hits, startL0, hithandler.startS3L3Down, xMax );

      startS3L0 = hithandler.startS3L0Down;
      startL3   = hithandler.startS3L3Down;
      startL1   = hithandler.startS3L1Down;
      startL2   = hithandler.startS3L2Down;

      startS2L0 = hithandler.startS2L0Down;
      startS2L3 = hithandler.startS2L3Down;
      startS2L1 = hithandler.startS2L1Down;
      startS2L2 = hithandler.startS2L2Down;

      startS1L0 = 0;
      startS1L1 = hithandler.startS1L1Down;
      startS1L2 = hithandler.startS1L2Down;
      startS1L3 = hithandler.startS1L3Down;
    }

    // some variables to cache offsets
    int startoffsetS3L1{startL1};
    int startoffsetS3L2{startL2};
    int startoffsetS3L3{startL3};

    // FIXME these offsets seem to work in the tested minPt=.49 scenario
    // These are not always 100% correct though
    int startoffsetS2L0{startS2L0};
    int startoffsetS2L1{startS2L1};
    int startoffsetS2L2{startS2L2};
    int startoffsetS2L3{startS2L3};

    float const zdelta_S3_XLayers     = betterZ[11] - betterZ[8];
    float const inv_zdelta_S3_XLayers = 1.f / ( betterZ[11] - betterZ[8] );
    // for S1 and S2 it is better to calc it in the loop since we reach it less often

    // Loop over hits in first x-layer within search window
    for ( auto idxL0{startL0}; idxL0 < EndL0; ++idxL0 ) {

      mydebug( "#####################Start new hit################################" );
      // get x value of hit
      float const S3x0 = hithandler.hits[idxL0];
      // calculate a rough estimate of the expected slope
      float const tx = ( S3x0 - xAtMagnet ) * InvDeltaZMagS2L0;
      // this is a small correction mainly for lower momentum tracks for which the zkink isn't perfect
      // and they'll have some bending. This is solely empirical.
      float const x3curve =
          ( tx - endv_tx ) * ( -0.231615f + ( endv_tx * 33.1256f + tx * 10.4693f ) * ( tx - endv_tx ) );
      // piece together the x-prediction in the last x layer
      float const S3x3pred = S3x0 + tx * zdelta_S3_XLayers + x3curve;

      mydebug( "S3x0", S3x0, "lhcbid", hithandler.IDs[idxL0], "S3x3pred", S3x3pred, "tx", tx );

      // quit if we are outside of sensitive area
      if ( std::abs( S3x3pred ) > 3200.f ) continue;

      S3buffer += 1;
      // FIXME this is the most expensive search, luckily we can cache the starting point
      // however the first search is still costly, maybe we can perform the first search with an avx2 search
      // before the loop?
      auto const [selectS3L3, S3L3Delta] = SearchLayerForHitLinear( hithandler.hits, startoffsetS3L3, S3x3pred );
      startoffsetS3L3                    = selectS3L3;

      // calculate x-slope of the found doublet
      float const newtx = ( hithandler.hits[selectS3L3] - S3x0 ) * inv_zdelta_S3_XLayers;
      // deltaSlope is proportional to the momentum and used in many places to tune momentum dependant search windows
      float const deltaSlope = newtx - endv_tx;
      // the minimum value of 0.011 avoids too small search windows for tracks with P > 100 GeV
      float const absDSlope = std::max( 0.011f, std::abs( deltaSlope ) );

      mydebug( "delta x3 :", S3L3Delta, "idx", hithandler.IDs[selectS3L3] );

      // check if the x-hit is within our search window acceptance
      if ( S3L3Delta > std::min( p_DW_b + p_DW_m * absDSlope, p_DW_max.value() ) ) continue;

      // we have a doublet and are going to try and build a track
      // for that we need a couple tmp objects to keep track of stuff
      // tmpidvec will store the indices of the hit to refind it later
      std::array<int, 12> tmpidvec{selectS3L3};
      // this is a vector that will remember some values for the linear fit performed at the very end.
      // for each hit this will store 4 values, [xvalue, zdelta, curvature, residual]
      // where zdelta is the distance between the hit and the hit in station 3 layer 0
      // curvature is the non-linear part of the prediction see above for example x3curve variable
      std::array<float, 24> fitvec{S3x0, 0, 0, 0, hithandler.hits[selectS3L3], zdelta_S3_XLayers, x3curve, 0};
      // fitcnt simply keeps track of what's in the array
      int fitcnt = 8;

      float const direction = -1.f * std::copysign( 1.f, deltaSlope );

      // correct the straight line estimate of the y-position at station 3 layer 0
      float const ycorr =
          absDSlope * ( direction * deltaYParams[0] +
                        endv_ty * ( deltaYParams[1] + deltaYParams[5] * endv_ty2 +
                                    endv_tx * ( direction * ( deltaYParams[2] + deltaYParams[6] * endv_ty2 ) +
                                                endv_tx * ( deltaYParams[3] + deltaYParams[7] * endv_ty2 +
                                                            deltaYParams[4] * direction * endv_tx ) ) ) );

      // calculate the x-predictions for the U V layers in station 3
      float const S3x1pred = S3x0 + newtx * ( betterZ[9] - betterZ[8] ) + ( yAtZ[9] - ycorr ) * dXdYLayer[4];
      float const S3x2pred = S3x0 + newtx * ( betterZ[10] - betterZ[8] ) + ( yAtZ[10] - 1.03f * ycorr ) * dXdYLayer[5];
      // keep track of how many hits we find
      int numuvhits{0};
      int numxhits{2};

      S3UVbuffer += 1;

      // search for hits in layer
      auto const [selectS3L1, DeltaS3L1] = SearchLayerForHitLinear( hithandler.hits, startoffsetS3L1, S3x1pred );
      auto const [selectS3L2, DeltaS3L2] = SearchLayerForHitLinear( hithandler.hits, startoffsetS3L2, S3x2pred );

      // cache the position for next search
      startoffsetS3L1 = selectS3L1;
      startoffsetS3L2 = selectS3L2;
      mydebug( "deltas x1, x2: ", DeltaS3L1, DeltaS3L2, hithandler.IDs[selectS3L1], hithandler.IDs[selectS3L2] );

      // calculate acceptance window for uv hits based on momentum
      float const S3uvwindow = ( p_UV3W_b + p_UV3W_m * absDSlope );

      if ( DeltaS3L2 < S3uvwindow ) {
        // I found a hit in S3L2
        tmpidvec[1] = selectS3L2;
        ++numuvhits;
      };
      if ( DeltaS3L1 < S3uvwindow ) {
        // I found a hit in S3L1
        tmpidvec[1 + numuvhits] = selectS3L1;
        ++numuvhits;
      } else if ( numuvhits < 1 )
        // didn't find any uv hits let's try our luck in the next loop iteration
        continue;
      // lhcbids are in order L3 (L2) (L1) L0
      tmpidvec[1 + numuvhits] = idxL0;

      // this variable is saved for later as it is quite good for ghost rejection
      float const S3UVDelta =
          numuvhits < 2
              ? 1.0f
              : 1.0f + std::abs( hithandler.hits[selectS3L1] - S3x1pred + hithandler.hits[selectS3L2] - S3x2pred );

      /////////////////////////////////////////////////////////////////////////
      /////////////////////////////////////////////////////////////////////////
      //                      Start of search in Station 2                   //
      /////////////////////////////////////////////////////////////////////////
      /////////////////////////////////////////////////////////////////////////

      // most of what happens below is similar to station 3

      // make x-predictions
      float const S2x0curve =
          ExtFac_alpha_S2L0 * deltaSlope + ExtFac_beta_S2L0 * deltaSlope + ExtFac_gamma_S2L0 * deltaSlope * endv_ty2;

      float const S2x0pred = S3x0 + newtx * ( betterZ[4] - betterZ[8] ) + S2x0curve;

      float const S2x3curve =
          deltaSlope * ExtFac_alpha_S2L3 + deltaSlope * ExtFac_beta_S2L3 + deltaSlope * ExtFac_gamma_S2L3 * endv_ty2;

      float const S2x3pred = S3x0 + newtx * ( betterZ[7] - betterZ[8] ) + S2x3curve;

      mydebug( "S2X0, S2X3", S2x0pred, S2x3pred, "newtx", newtx, "dZsS3L0", betterZ[4] - betterZ[8], "dZsS2L3",
               betterZ[7] - betterZ[8] );

      // S2Doublet will stay true if x-hits are found in both stations
      bool S2Doublet = true;
      S2buffer += 1;

      // get best hit candidate
      auto const [selectS2L3, DeltaS2L3] = SearchLayerForHitLinearABS( hithandler.hits, startoffsetS2L3, S2x3pred );
      // cache position for next search
      startoffsetS2L3 = selectS2L3;
      mydebug( "selectS2L3", selectS2L3, " delta S2L3: ", DeltaS2L3, hithandler.IDs[selectS2L3] );

      // did we find a hit?
      if ( DeltaS2L3 < ( p_S2L3W_b + p_S2L3W_m * absDSlope ) ) {
        tmpidvec[numuvhits + numxhits] = selectS2L3;
        ++numxhits;
        // fill fit vec with x-hit information
        fillFitVec( fitvec, fitcnt, hithandler.hits[selectS2L3], betterZ[7] - betterZ[8], S2x3curve,
                    hithandler.hits[selectS2L3] - S2x3pred );
      } else {
        // we didn't find a hit and thus also not a doublet
        S2Doublet = false;
        // do we even really want to continue with this track or is it crap?
        if ( numxhits + numuvhits < 4 ) continue;
      }

      // get hit candidate for S2L0
      auto const [selectS2L0, DeltaS2L0] = SearchLayerForHitLinearABS( hithandler.hits, startoffsetS2L0, S2x0pred );
      // cache position for next search
      startoffsetS2L0 = selectS2L0;
      mydebug( "selectS2L0", selectS2L0, " delta S2L0: ", DeltaS2L0, hithandler.IDs[selectS2L0] );

      // did we find a hit?
      if ( DeltaS2L0 < ( p_S2L0W_b + p_S2L0W_m * absDSlope ) ) {
        ++numxhits;
        fillFitVec( fitvec, fitcnt, hithandler.hits[selectS2L0], betterZ[4] - betterZ[8], S2x0curve,
                    hithandler.hits[selectS2L0] - S2x0pred );
      } else {
        S2Doublet = false;
        if ( numxhits + numuvhits < 5 ) continue;
      }

      mydebug( "Start Station 2 UV Work" );
      // default value in case we don't enter the uv search
      float S2UVDelta = 1.0f;

      // we only check for uv hits if we found a doublet
      // this choice was made since a doublet and it's slope allow for small search windows
      // FIXME we could potentially allow this search in other/all scenarios but this need more tweaking
      // Note that this search will only help with ghost rejection, not help with efficiencies
      // Thus as long as ghosts aren't a problem there isn't really a big reason to loosen this requirement
      if ( S2Doublet ) {
        S2UVbuffer += 1;
        float const S2Tx = ( hithandler.hits[selectS2L3] - hithandler.hits[selectS2L0] ) / ( betterZ[7] - betterZ[4] );

        float const S2x1pred = hithandler.hits[selectS2L0] + S2Tx * ( betterZ[5] - betterZ[4] ) +
                               ( yAtZ[5] - .83f * ycorr ) * dXdYLayer[2];
        float const S2x2pred = hithandler.hits[selectS2L0] + S2Tx * ( betterZ[6] - betterZ[4] ) +
                               ( yAtZ[6] - .85f * ycorr ) * dXdYLayer[3];

        mydebug( S2Tx, betterZ[7] - betterZ[4], hithandler.hits[selectS2L0], hithandler.hits[selectS2L3], S2x1pred,
                 S2x2pred );
        mydebug( hithandler.hits[startS2L1 + 1], hithandler.hits[startS2L2 - 2] );
        mydebug( LHCb::LHCbID( hithandler.IDs[startS2L1 + 1] ), LHCb::LHCbID( hithandler.IDs[startS2L2 - 2] ) );
        mydebug( hithandler.hits[startS2L2 + 1], hithandler.hits[startS3L0 - 2] );
        mydebug( LHCb::LHCbID( hithandler.IDs[startS2L2 + 1] ), LHCb::LHCbID( hithandler.IDs[startS3L0 - 2] ) );

        auto const [selectS2L1, DeltaS2L1] = SearchLayerForHitLinearABS( hithandler.hits, startoffsetS2L1, S2x1pred );
        startoffsetS2L1                    = selectS2L1;

        auto const [selectS2L2, DeltaS2L2] = SearchLayerForHitLinearABS( hithandler.hits, startoffsetS2L2, S2x2pred );
        startoffsetS2L2                    = selectS2L2;

        float const S2uvwindow = ( p_UV2W_b + p_UV2W_m * absDSlope );
        if ( DeltaS2L1 > S2uvwindow and DeltaS2L2 > S2uvwindow ) { continue; }

        if ( DeltaS2L2 < S2uvwindow ) {
          tmpidvec[numuvhits + numxhits - 1] = selectS2L2;
          ++numuvhits;
        }

        if ( DeltaS2L1 < S2uvwindow ) {
          tmpidvec[numuvhits + numxhits - 1] = selectS2L1;
          ++numuvhits;
        }

        if ( DeltaS2L1 < S2uvwindow and DeltaS2L2 < S2uvwindow ) {
          S2UVDelta += std::abs( hithandler.hits[selectS2L1] + hithandler.hits[selectS2L2] - S2x1pred - S2x2pred );
        }
      } /// end of UV work

      // this fill is postponed simply for reasons of ordering
      tmpidvec[numuvhits + numxhits - 1] = selectS2L0;

      /////////////////////////////////////////////////////////////////////////
      /////////////////////////////////////////////////////////////////////////
      //                      Start of search in Station 2                   //
      /////////////////////////////////////////////////////////////////////////
      /////////////////////////////////////////////////////////////////////////

      mydebug( "Start Station 1 Work" );

      S1buffer += 1;

      float const S1x3curve =
          deltaSlope * ExtFac_alpha_S1L3 + deltaSlope * ExtFac_beta_S1L3 + deltaSlope * ExtFac_gamma_S1L3 * endv_ty2;
      float const S1x3pred = S3x0 + newtx * ( betterZ[3] - betterZ[8] ) + S1x3curve;

      auto const [selectS1L3, DeltaS1L3] = SearchLayerForHitBinary( hithandler.hits, startS1L3, startS1L1, S1x3pred );

      bool S1Doublet = true;
      mydebug( "selectS1L3", selectS1L3 );

      mydebug( "delta S1L3: ", DeltaS1L3, hithandler.IDs[selectS1L3] );

      if ( DeltaS1L3 < ( p_S1L3W_b + p_S1L3W_m * absDSlope ) ) {
        tmpidvec[numuvhits + numxhits] = selectS1L3;
        ++numxhits;
        fillFitVec( fitvec, fitcnt, hithandler.hits[selectS1L3], betterZ[3] - betterZ[8], S1x3curve,
                    hithandler.hits[selectS1L3] - S1x3pred );
      } else {
        S1Doublet = false;
        if ( numxhits < 4 or numuvhits < 3 ) continue;
      }

      float const S1x0curve =
          ExtFac_alpha_S1L0 * deltaSlope + ExtFac_beta_S1L0 * deltaSlope + deltaSlope * ExtFac_gamma_S1L0 * endv_ty2;

      float const S1x0pred = S3x0 + newtx * ( betterZ[0] - betterZ[8] ) + S1x0curve;

      auto const [selectS1L0, DeltaS1L0] = SearchLayerForHitBinary( hithandler.hits, startS1L0, startS1L3, S1x0pred );

      mydebug( "selectS1L0", selectS1L0, "delta S1L0: ", DeltaS1L0, hithandler.IDs[selectS1L0] );

      if ( DeltaS1L0 < ( p_S1L0W_b + p_S1L0W_m * absDSlope ) ) {
        ++numxhits;
        fillFitVec( fitvec, fitcnt, hithandler.hits[selectS1L0], betterZ[0] - betterZ[8], S1x0curve,
                    hithandler.hits[selectS1L0] - S1x0pred );
      } else {
        S1Doublet = false;
        if ( numxhits < 5 or numuvhits < 3 ) continue;
      }

      float S1UVDelta = 1.0f;

      if ( S1Doublet ) {
        S1UVbuffer += 1;
        float const S1Tx = ( hithandler.hits[selectS1L3] - hithandler.hits[selectS1L0] ) / ( betterZ[3] - betterZ[0] );

        float const S1x1pred = hithandler.hits[selectS1L0] + S1Tx * ( betterZ[1] - betterZ[0] ) +
                               ( yAtZ[1] - .62f * ycorr ) * dXdYLayer[0];

        float const S1x2pred = hithandler.hits[selectS1L0] + S1Tx * ( betterZ[2] - betterZ[0] ) +
                               ( yAtZ[2] - .63f * ycorr ) * dXdYLayer[1];

        auto const [selectS1L1, DeltaS1L1] = SearchLayerForHitBinary( hithandler.hits, startS1L1, startS1L2, S1x1pred );
        auto const [selectS1L2, DeltaS1L2] = SearchLayerForHitBinary( hithandler.hits, startS1L2, startS2L0, S1x2pred );
        float const S1uvwindow             = ( p_UV1W_b + p_UV1W_m * absDSlope );

        if ( DeltaS1L1 > S1uvwindow and std::abs( DeltaS1L2 ) > S1uvwindow ) { continue; }
        if ( DeltaS1L2 < S1uvwindow ) {
          tmpidvec[numuvhits + numxhits - 1] = selectS1L2;
          ++numuvhits;
        }
        if ( DeltaS1L1 < S1uvwindow ) {
          tmpidvec[numuvhits + numxhits - 1] = selectS1L1;
          ++numuvhits;
        }
        if ( DeltaS1L1 < S1uvwindow and DeltaS1L2 < S1uvwindow )
          S1UVDelta += std::abs( hithandler.hits[selectS1L1] + hithandler.hits[selectS1L2] - S1x1pred - S1x2pred );
      }
      tmpidvec[numuvhits + numxhits - 1] = selectS1L0;

      mydebug( "End Station 1 Work" );

      /////////////////////////////////////////////////////////////////////////
      /////////////////////////////////////////////////////////////////////////
      //                      Start of finalization procedure                //
      /////////////////////////////////////////////////////////////////////////
      /////////////////////////////////////////////////////////////////////////

      // quick linear fit in x0 and tx, curvature terms remain fixed
      float s0  = 0;
      float sz  = 0;
      float sz2 = 0;
      float sd  = 0;
      float sdz = 0;

      for ( int idx{0}; idx < fitcnt; idx += 4 ) {
        // for now the weight is just 1
        s0 += 1;
        sz += fitvec[idx + 1];
        sz2 += fitvec[idx + 1] * fitvec[idx + 1];
        sd += fitvec[idx + 3];
        sdz += fitvec[idx + 3] * fitvec[idx + 1];
      }

      float const den     = 1.f / ( sz * sz - 6 * sz2 );
      float const xoff    = ( sdz * sz - sd * sz2 ) * den;
      float const txoff   = ( sd * sz - 6 * sdz ) * den;
      float const finaltx = newtx + txoff;
      float const newx0   = S3x0 + xoff;

      // calculate a chi2 after the fit
      float chi2 = ( fitvec[0] - newx0 ) * ( fitvec[0] - newx0 );
      for ( int idx{4}; idx < fitcnt; idx += 4 ) {
        // for now the weight is just 1
        float const diff = fitvec[idx] - ( newx0 + fitvec[idx + 1] * finaltx + fitvec[idx + 2] );
        chi2 += diff * diff;
      }

      // determine momentum * charge estimate, polynomial was fitted on MC
      const float finaltx2 = finaltx * finaltx;
      const float coef =
          ( MomentumParams[0] + finaltx2 * ( MomentumParams[1] + MomentumParams[2] * finaltx2 ) +
            MomentumParams[3] * finaltx * endv_tx + endv_ty2 * ( MomentumParams[4] + MomentumParams[5] * endv_ty2 ) +
            MomentumParams[6] * endv_tx2 );

      float const candPQ = m_magscalefactor * coef / ( finaltx - endv_tx ) + factor * MomentumParams[7];

      mydebug( finaltx, endv_tx, endv_ty2, factor, coef, candPQ );

      // the condition evaluates to true if the track is a charge flipped one
      float deltaMom = ( endv_pt > p_wrongSignPT and ( factor * ( S3x0 - straighExt ) < 0 ) )
                           ? 3.f * std::abs( endv_qp * ( endv_pq + candPQ ) )
                           : std::abs( endv_qp * ( endv_pq - candPQ ) );

      // counteract worse momentum resolution at higher momenutm by scaling down the observed delta
      if ( std::abs( candPQ ) > 40000 ) deltaMom *= 40000.f / std::abs( candPQ );

      // For now a linear discriminant seems to do well enough. But if need be this could be substituted by a neural net
      // for better rejection of ghosts
      float const quality =
          LDAParams[0] * vdt::fast_logf( 1.f + deltaMom ) + LDAParams[1] * vdt::fast_logf( 1.f + chi2 ) +
          LDAParams[2] * vdt::fast_logf( std::abs( candPQ ) * endv_txy / std::sqrt( 1 + endv_txy2 ) ) +
          LDAParams[3] * vdt::fast_logf( S3UVDelta ) + LDAParams[4] * vdt::fast_logf( S2UVDelta ) +
          LDAParams[5] * vdt::fast_logf( S1UVDelta ) + LDAParams[6] * static_cast<float>( numuvhits ) +
          LDAParams[7] * static_cast<float>( numxhits );

      mydebug( "+++++++++Trackcandidate++++++++++++++++" );
      mydebug( candPQ, finaltx, endv_pq );
      mydebug( "deltaMom", deltaMom, "chi2", chi2, "pt", std::abs( candPQ ) * endv_txy / std::sqrt( 1 + endv_txy2 ),
               "S3UVDelta", S3UVDelta );
      mydebug( "final quality", quality, "worst candidate", bestcandidate.quality );

      if ( quality < bestcandidate.quality )
        bestcandidate = {tmpidvec, candPQ, finaltx, newx0, quality, numuvhits + numxhits};

    } // loop over possible station three doublets

    mydebug( "Search done!" );

    // safe the bestcandidate if we built one
    if ( bestcandidate.ids[0] != maxint ) {
      int i    = Output.size();
      int mask = true;

      Output.compressstore_trackVP<I>( i, mask, tracks.trackVP<I>( uttrack ) );
      Output.compressstore_trackUT<I>( i, mask, uttrack );

      float const qop = 1.f / bestcandidate.PQ;
      Output.compressstore_stateQoP<F>( i, mask, qop );

      int const offset = 12 - bestcandidate.numHits;
      int       n_hits = 0;
      for ( auto idx{bestcandidate.ids.rbegin() + offset}; idx != bestcandidate.ids.rend(); ++idx, ++n_hits ) {
        Output.compressstore_hit<I>( i, n_hits, mask, hithandler.IDs[*idx] );
      }
      Output.compressstore_nHits<I>( i, mask, bestcandidate.numHits );

      // AtT State
      float const endT_z = m_LayerZPos[8];
      float const endT_x = bestcandidate.newx0;
      auto        endT_y = endv_pos.y + endv_dir.y * ( endT_z - endv_pos.z );

      auto endT_pos = Vec3<F>( endT_x, endT_y, endT_z );
      auto endT_dir = Vec3<F>( bestcandidate.finaltx, endv_dir.y, 1.f );

      Output.compressstore_statePos<F>( i, mask, endT_pos );
      Output.compressstore_stateDir<F>( i, mask, endT_dir );

      Output.size() += dType::popcount( mask );
    }

  } // end loop over UT tracks

  m_CounterOutput += Output.size();
  return Output;
}
