/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// RichUtils
#include "RichUtils/RichSIMDTypes.h"

// local utils
#include "../Common.h"

// STL
#include <algorithm>
#include <array>
#include <iostream>
#include <memory>
#include <random>
#include <string>
#include <typeinfo>
#include <vector>

static unsigned short int gid{0};

class Foo {
public:
  Foo() : id( gid++ ) {}

public:
  unsigned short int id{0};
};

using FloatV = Rich::SIMD::FPF;
using IntV   = Rich::SIMD::UInt32;

using FooPtrArray = std::array<const Foo*, FloatV::Size>;

template <typename DATA>
unsigned long long int __attribute__( ( noinline ) ) comparePtrs( const DATA& V ) {
  unsigned long long int best_dur{99999999999999999};
  timespec               start, end;

  unsigned long long int sum( 0 );

  const unsigned int nTests = 1000;
  for ( unsigned int i = 0; i < nTests; ++i ) {
    // start clock
    clock_gettime( CLOCK_MONOTONIC_RAW, &start );

    // test
    for ( const auto& d : V ) {
      if ( d.first != d.second ) { ++sum; }
    }

    // stop clock
    clock_gettime( CLOCK_MONOTONIC_RAW, &end );

    // get best time
    const auto duration = time_diff( &start, &end );
    if ( duration < best_dur ) { best_dur = duration; }
  }

#ifndef __clang__
  asm volatile( "" : "+x"( sum ) );
#else
  // clang does not seem to like the above
  std::cout << "Sum = " << sum << std::endl;
#endif

  return best_dur;
}

int main( int /*argc*/, char** /*argv*/ ) {

  // test CPU supports compilation level
  if ( !checkCPU() ) { return 77; }

  // make owning container of objects
  std::array<Foo, FloatV::Size> foos;

  // make arrays of pointers to compare
  std::vector<std::pair<FooPtrArray, FooPtrArray>> V;

  // number of elements to test
  const std::size_t nElems = 1e4;
  std::cout << "Using " << nElems << " test arrays" << std::endl;

  // fill containers
  V.reserve( nElems );
  for ( std::size_t e = 0; e < nElems; ++e ) {
    V.emplace_back();
    auto& a = V.back().first;
    auto& b = V.back().second;
    for ( std::size_t i = 0; i < FloatV::Size; ++i ) { a[i] = b[i] = &foos[i]; }
    std::random_shuffle( a.begin(), a.end() );
    std::random_shuffle( b.begin(), b.end() );
  }

  // compare
  auto timePtr = comparePtrs( V );
  std::cout << "Pointer compare time = " << timePtr << std::endl;

  // make sure we are not optimized away
  asm volatile( "" : "+x"( timePtr ) );

  return 0;
}
