/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "RichTrackRadiatorMaterial.h"

using namespace Rich::Future::Rec::Moni;

//-----------------------------------------------------------------------------
// Implementation file for class : TrackRadiatorMaterial
//
// 2016-12-06 : Chris Jones
//-----------------------------------------------------------------------------

TrackRadiatorMaterial::TrackRadiatorMaterial( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator, //
                {KeyValue{"TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default}} ) {
  // print some stats on the final plots
  // setProperty ( "HistoPrint", true );
  // change some defaults.
  setProperty( "NBins1DHistos", 100 );
  setProperty( "NBins2DHistos", 100 );
}

//-----------------------------------------------------------------------------

StatusCode TrackRadiatorMaterial::initialize() {
  auto sc = Consumer::initialize();
  if ( !sc ) return sc;

  // services
  m_transSvc = svc<ITransportSvc>( "TransportSvc", true );

  return sc;
}

//-----------------------------------------------------------------------------

StatusCode TrackRadiatorMaterial::prebookHistograms() {

  RADIATOR_GLOBAL_POSITIONS_X;
  RADIATOR_GLOBAL_POSITIONS_Y;

  // Loop over radiators
  for ( const auto rad : Rich::radiators() ) {
    if ( m_rads[rad] ) {

      // max path length
      const auto minL = ( Rich::Rich2Gas != rad ? 800.0 : 1400.0 );
      const auto maxL = ( Rich::Rich2Gas != rad ? 1100.0 : 3200.0 );

      richHisto1D( HID( "EffL", rad ), "Effective length", 0, 0.3, nBins1D() );
      richHisto1D( HID( "EffLOvPathL", rad ), "Effective length / unit pathlength", 0, 0.0001, nBins1D() );
      richHisto1D( HID( "PathL", rad ), "Track pathlength", minL, maxL, nBins1D() );
      richHisto2D( HID( "EffLVPathL", rad ), "Effective length in rad units V Segment pathlength", minL, maxL,
                   nBins2D(), 0, 0.3, nBins2D() );

      richProfile2D( HID( "EffLVEntryXY", rad ), "Effective length V Entry (x,y)", -xRadEntGlo[rad], xRadEntGlo[rad],
                     nBins1D(), -yRadEntGlo[rad], yRadEntGlo[rad], nBins1D() );

      richProfile2D( HID( "EffLVExitXY", rad ), "Effective length V Exit (x,y)", -xRadExitGlo[rad], xRadExitGlo[rad],
                     nBins1D(), -yRadExitGlo[rad], yRadExitGlo[rad], nBins1D() );

      richProfile2D( HID( "EffLOvPathLVEntryXY", rad ), "Effective length  / unit pathlength V Entry (x,y)",
                     -xRadEntGlo[rad], xRadEntGlo[rad], nBins1D(), -yRadEntGlo[rad], yRadEntGlo[rad], nBins1D() );

      richProfile2D( HID( "EffLOvPathLVExitXY", rad ), "Effective length  / unit pathlength V Exit (x,y)",
                     -xRadExitGlo[rad], xRadExitGlo[rad], nBins1D(), -yRadExitGlo[rad], yRadExitGlo[rad], nBins1D() );
    }
  }

  return StatusCode::SUCCESS;
}

//-----------------------------------------------------------------------------

void TrackRadiatorMaterial::operator()( const LHCb::RichTrackSegment::Vector& segments ) const {

  // Create accelerator cache for the transport service
  auto tsCache = m_transSvc->createCache();

  // loop over segments
  for ( const auto& seg : segments ) {

    // Which radiator
    const auto rad = seg.radiator();
    // which rich
    const auto rich = seg.rich();

    // radiator points
    // move the entry/exit points by +-10mm to move away from mirror structures etc.
    const auto entP = seg.entryPoint() + Gaudi::XYZVector( 0, 0, 10 );
    const auto extP = seg.exitPoint() + Gaudi::XYZVector( 0, 0, -10 );

    // select segments away from edges etc.
    if ( ( Rich::Rich1 == rich && ( extP.rho() < 70 || entP.rho() < 70 ) ) ||
         ( Rich::Rich2 == rich && ( extP.rho() < 300 || entP.rho() < 300 || //
                                    fabs( entP.x() ) > 2200 || fabs( extP.x() ) > 3200 ) ) ) {
      continue;
    }

    // path length (using the exact points used below)
    const auto length = std::sqrt( ( extP - entP ).mag2() );

    // get the radiation length
    try {
      const auto effL    = m_transSvc->distanceInRadUnits_r( entP, extP, tsCache, 0, nullptr );
      const auto effLOvL = ( length > 0 ? effL / length : 0.0 );

      // fill plots
      richHisto1D( HID( "EffL", rad ) )->fill( effL );
      richHisto1D( HID( "EffLOvPathL", rad ) )->fill( effLOvL );
      richHisto2D( HID( "EffLVPathL", rad ) )->fill( length, effL );
      richHisto1D( HID( "PathL", rad ) )->fill( length );
      richProfile2D( HID( "EffLVEntryXY", rad ) )->fill( entP.x(), entP.y(), effL );
      richProfile2D( HID( "EffLVExitXY", rad ) )->fill( extP.x(), extP.y(), effL );
      richProfile2D( HID( "EffLOvPathLVEntryXY", rad ) )->fill( entP.x(), entP.y(), effLOvL );
      richProfile2D( HID( "EffLOvPathLVExitXY", rad ) )->fill( extP.x(), extP.y(), effLOvL );
    } catch ( const TransportSvcException& excpt ) {
      ++m_radLwarn;
      _ri_debug << excpt.message() << endmsg;
    }
  }
}

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TrackRadiatorMaterial )

//-----------------------------------------------------------------------------
