/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichRecHistoAlgBase.cpp
 *
 *  Implementation file for RICH reconstruction monitor
 *  algorithm base class : Rich::Rec::HistoAlgBase
 *
 *  @author Chris Jones    Christopher.Rob.Jones@cern.ch
 *  @date   2005/01/13
 */
//-----------------------------------------------------------------------------

// local
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// ============================================================================
// Force creation of templated class
#include "RichRecBase.icpp"
template class Rich::Future::Rec::CommonBase<Rich::Future::HistoAlgBase>;
// ============================================================================

// ============================================================================
// Standard constructor
// ============================================================================
Rich::Future::Rec::HistoAlgBase::HistoAlgBase( const std::string& name, ISvcLocator* pSvcLocator )
    : Rich::Future::HistoAlgBase( name, pSvcLocator ) //
    , Rich::Future::Rec::CommonBase<Rich::Future::HistoAlgBase>( this ) {}
// ============================================================================

// ============================================================================
// Initialise
// ============================================================================
StatusCode Rich::Future::Rec::HistoAlgBase::initialize() {
  // Initialise base class
  auto sc = Rich::Future::HistoAlgBase::initialize();
  // Common initialisation
  return ( sc.isSuccess() ? initialiseRichReco() : sc );
}
// ============================================================================

// ============================================================================
// Main execute method
// ============================================================================
StatusCode Rich::Future::Rec::HistoAlgBase::execute() {
  // All algorithms should re-implement this method
  error() << "Default Rich::RecHistoAlgBase::execute() called !!" << endmsg;
  return StatusCode::FAILURE;
}
// ============================================================================

// ============================================================================
// Finalize
// ============================================================================
StatusCode Rich::Future::Rec::HistoAlgBase::finalize() {
  // Common finalisation
  const auto sc = finaliseRichReco();
  // Finalize base class
  return ( sc.isSuccess() ? Rich::Future::HistoAlgBase::finalize() : sc );
}
// ============================================================================
