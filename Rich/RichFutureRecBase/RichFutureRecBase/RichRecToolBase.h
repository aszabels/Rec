/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichRecToolBase.h
 *
 * Header file for reconstruction tool base class : RichRecToolBase
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date   2002-07-26
 */
//-----------------------------------------------------------------------------

#pragma once

// Base classes
#include "RichFutureKernel/RichToolBase.h"
#include "RichFutureRecBase/RichRecBase.h"

namespace Rich::Future::Rec {

  //-----------------------------------------------------------------------------
  /** @class ToolBase RichRecToolBase.h RichRecBase/RichRecToolBase.h
   *
   *  Abstract base class for RICH reconstruction tools providing
   *  some basic functionality.
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   2002-07-26
   */
  //-----------------------------------------------------------------------------

  class ToolBase : public Rich::Future::ToolBase, public Rich::Future::Rec::CommonBase<Rich::Future::ToolBase> {

  public:
    /// Standard constructor
    ToolBase( const std::string& type, const std::string& name, const IInterface* parent );

    // Initialize method
    virtual StatusCode initialize() override;

    // Finalize method
    virtual StatusCode finalize() override;
  };

} // namespace Rich::Future::Rec
