/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "RichDetectablePhotonYields.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Future::Rec;

//=============================================================================

DetectablePhotonYields::DetectablePhotonYields( const std::string& name, ISvcLocator* pSvcLocator )
    : MultiTransformer( name, pSvcLocator,
                        // inputs
                        {KeyValue{"TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default},
                         KeyValue{"EmittedSpectraLocation", PhotonSpectraLocation::Emitted},
                         KeyValue{"MassHypothesisRingsLocation", MassHypoRingsLocation::Emitted}},
                        // outputs
                        {KeyValue{"DetectablePhotonYieldLocation", PhotonYieldsLocation::Detectable},
                         KeyValue{"DetectablePhotonSpectraLocation", PhotonSpectraLocation::Detectable}} ) {
  m_riches.fill( nullptr );
  m_qWinZSize.fill( 0 );
  // debug
  // setProperty( "OutputLevel", MSG::VERBOSE );
}

//=============================================================================

StatusCode DetectablePhotonYields::initialize() {
  // Sets up various tools and services
  auto sc = MultiTransformer::initialize();
  if ( !sc ) return sc;

  // Rich1 and Rich2
  m_riches[Rich::Rich1] = getDet<DeRich1>( DeRichLocations::Rich1 );
  m_riches[Rich::Rich2] = getDet<DeRich2>( DeRichLocations::Rich2 );

  // Do we have HPDs or PMTs
  const bool PmtActivate = m_riches[Rich::Rich1]->RichPhotoDetConfig() == Rich::PMTConfig;

  // Quartz window eff
  const auto qEff = m_riches[Rich::Rich1]->param<double>( "HPDQuartzWindowEff" );

  // Digitisation pedestal loss
  const auto pLos = ( PmtActivate && m_riches[Rich::Rich1]->exists( "PMTPedestalDigiEff" )
                          ? m_riches[Rich::Rich1]->param<double>( "PMTPedestalDigiEff" )
                          : m_riches[Rich::Rich1]->param<double>( "HPDPedestalDigiEff" ) );

  // store cached value
  m_qEffPedLoss = qEff * pLos;

  // Quartz window params
  m_qWinZSize[Rich::Rich1] = m_riches[Rich::Rich1]->param<double>( "Rich1GasQuartzWindowThickness" );
  m_qWinZSize[Rich::Rich2] = m_riches[Rich::Rich2]->param<double>( "Rich2GasQuartzWindowThickness" );

  // return
  return sc;
}

//=============================================================================

OutData DetectablePhotonYields::operator()( const LHCb::RichTrackSegment::Vector& segments,       //
                                            const PhotonSpectra::Vector&          emittedSpectra, //
                                            const MassHypoRingsVector&            massRings       //
                                            ) const {

  // Scalar type to work with
  using ScType = PhotonYields::Type;

  // make the data to return
  OutData data;
  auto&   yieldV   = std::get<PhotonYields::Vector>( data );
  auto&   spectraV = std::get<PhotonSpectra::Vector>( data );
  // reserve sizes
  yieldV.reserve( segments.size() );
  spectraV.reserve( segments.size() );

  // local counts
  Count<const DeRichPD, 40>       pdCount;
  Count<const DeRichSphMirror, 4> primMirrCount;
  Count<const DeRichSphMirror, 6> secMirrCount;

  // Loop over input data
  for ( auto&& [segment, emitSpectra, rings] : Ranges::ConstZip( segments, emittedSpectra, massRings ) ) {

    // Create the detectable photon spectra, using the same energy range
    // as the emitted spectra
    auto& detSpectra = spectraV.emplace_back( emitSpectra.minEnergy(), emitSpectra.maxEnergy() );

    // create the yield data
    auto& yields = yieldV.emplace_back();

    // which RICH
    const auto rich = segment.rich();

    // Loop over real PID types
    for ( const auto id : activeParticlesNoBT() ) {

      // the signal
      ScType signal = 0;

      // Skip rings with no hits
      //_ri_debug << "  -> " << id << " #Rings=" << rings[id].size() << endmsg;
      if ( LIKELY( !rings[id].empty() ) ) {

        // Collect the signal info for this ring
        std::uint32_t totalInPD{0};
        pdCount.reset();
        primMirrCount.reset();
        secMirrCount.reset();
        for ( const auto& P : rings[id] ) {
          if ( LIKELY( RayTracedCKRingPoint::InHPDTube == P.acceptance() ) ) {
            // Mirrors
            const auto pM = P.primaryMirror();
            const auto sM = P.secondaryMirror();
            // PD
            const auto pd = P.photonDetector();
            // check
            assert( pM && sM && pd );
            // Count accepted points
            ++totalInPD;
            // Count PDs hit by this ring
            pdCount.count( pd );
            // Count primary mirrors
            primMirrCount.count( pM );
            // Count secondary mirrors
            secMirrCount.count( sM );
          }
        }

        // Any hits in acceptance ?
        if ( LIKELY( totalInPD > 0 ) ) {

          // loop over the energy bins
          for ( std::size_t iEnBin = 0; iEnBin < emitSpectra.energyBins(); ++iEnBin ) {

            // bin energy ( in eV )
            const auto energy = emitSpectra.binEnergy( iEnBin ) * Gaudi::Units::eV;
            if ( LIKELY( energy > 0 ) ) {

              // start with emitted signal
              auto sig = ( emitSpectra.energyDist( id ) )[iEnBin];
              if ( LIKELY( sig > 0 ) ) {

                // Get weighted average PD Q.E.
                ScType pdQEEff( 0 );
                if ( LIKELY( !pdCount.empty() ) ) {
                  for ( const auto& PD : pdCount ) {
                    // add up Q.E. eff
                    pdQEEff += ( ScType )( PD.second ) * ( ScType )( *( PD.first->pdQuantumEff() ) )[energy];
                  }
                  // normalise the result (and scale from % to fraction)
                  pdQEEff /= ( ScType )( 100 * totalInPD );
                } else {
                  pdQEEff = 1.0;
                  ++m_warnNoPDs;
                }

                // Weighted primary mirror reflectivity
                ScType primMirrRefl( 0 );
                if ( LIKELY( !primMirrCount.empty() ) ) {
                  for ( const auto& PM : primMirrCount ) {
                    // add up mirror refl.
                    primMirrRefl += ( ScType )( PM.second ) * ( ScType )( *( PM.first->reflectivity() ) )[energy];
                  }
                  // normalise the result
                  primMirrRefl /= ( ScType )( totalInPD );
                } else {
                  primMirrRefl = 1.0;
                  ++m_warnNoPrimMirrs;
                }

                // Weighted secondary mirror reflectivity
                ScType secMirrRefl( 0 );
                if ( LIKELY( !secMirrCount.empty() ) ) {
                  for ( const auto& SM : secMirrCount ) {
                    // add up mirror refl.
                    secMirrRefl += ( ScType )( SM.second ) * ( ScType )( *( SM.first->reflectivity() ) )[energy];
                  }
                  // normalise the result
                  secMirrRefl /= ( ScType )( totalInPD );
                } else {
                  secMirrRefl = 1.0;
                  ++m_warnNoSecMirrs;
                }

                // The total efficiency
                const auto totEff = m_qEffPedLoss * pdQEEff * primMirrRefl * secMirrRefl;

                // Scale the distribution in this bin by the above efficiencies
                sig *= totEff;

                // The Quartz window efficiency
                sig *= Rich::Maths::fast_exp( -m_qWinZSize[rich] /
                                              ( ScType )( *( m_riches[rich]->gasWinAbsLength() ) )[energy] );

                // if we still have some signal, save the values
                if ( LIKELY( sig > 0 ) ) {
                  // Save to the output spectra for this bin
                  ( detSpectra.energyDist( id ) )[iEnBin] = sig;
                  // update the overal detectable signal
                  signal += sig;
                }

              } // emitted signal in energy bin > 0

            } // energy > 0

          } // loop over energy bins

        } // >0 PDs

      } // Not below threshold

      // save the yield for this hypo
      yields.setData( id, signal );

    } // loop over PID types
  }

  // return the new data
  return data;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( DetectablePhotonYields )

//=============================================================================
