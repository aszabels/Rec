/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <array>
#include <cassert>
#include <cstdint>
#include <tuple>
#include <vector>

// Gaudi
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/PhysicalConstants.h"

// Base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Kernel
#include "Kernel/FastAllocVector.h"
#include "Kernel/RichDetectorType.h"

// Event Model
#include "RichFutureRecEvent/RichRecMassHypoRings.h"
#include "RichFutureRecEvent/RichRecPhotonYields.h"

// Utils
#include "RichUtils/FastMaths.h"
#include "RichUtils/RichMap.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/StlArray.h"
#include "RichUtils/ZipRange.h"

// RichDet
#include "RichDet/DeRich1.h"
#include "RichDet/DeRich2.h"
#include "RichDet/DeRichPD.h"
#include "RichDet/DeRichSphMirror.h"

namespace Rich::Future::Rec {

  // Use the functional framework
  using namespace Gaudi::Functional;

  namespace {
    /// Output data type
    using OutData = std::tuple<PhotonYields::Vector, PhotonSpectra::Vector>;
  } // namespace

  /** @class DetectablePhotonYields RichDetectablePhotonYields.h
   *
   *  Computes the emitted photon yield data from Track Segments.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */
  class DetectablePhotonYields final : public MultiTransformer<OutData( const LHCb::RichTrackSegment::Vector&, //
                                                                        const PhotonSpectra::Vector&,          //
                                                                        const MassHypoRingsVector& ),
                                                               Traits::BaseClass_t<AlgBase>> {

  public:
    /// Standard constructor
    DetectablePhotonYields( const std::string& name, ISvcLocator* pSvcLocator );

    /// Initialization after creation
    StatusCode initialize() override;

  public:
    /// Algorithm execution via transform
    OutData operator()( const LHCb::RichTrackSegment::Vector& segments,       //
                        const PhotonSpectra::Vector&          emittedSpectra, //
                        const MassHypoRingsVector&            massRings       //
                        ) const override;

  private:
    // utility types

    template <typename TYPE>
    using CountEntry = std::pair<TYPE*, std::uint32_t>;

    /// Utility class to count entry for a given entity.
    template <typename TYPE, std::size_t N>
    class Count final : public LHCb::Boost::Small::Vector<CountEntry<TYPE>, N> {

    public:
      // definitions

      /// The Data type
      using Data = CountEntry<TYPE>;

    public:
      /// Count the given object
      inline void count( TYPE* p ) noexcept {
        // Is this the same object as last time ?
        if ( p == m_last ) {
          ++( m_data->second );
        } else {
          // need to do a search to try and find the entry for this object
          auto it = std::find_if( this->begin(), this->end(), [&p]( const auto& i ) { return i.first == p; } );
          // if not preset add a new count of one
          if ( it == this->end() ) {
            m_data = &( this->emplace_back( p, 1 ) );
          } else {
            // increment existing count
            ++( it->second );
            m_data = &*it;
          }
          // update 'last' pointer cache
          m_last = p;
        }
      }

      /// Reset
      inline void reset() noexcept {
        // clear the vector
        this->clear();
        // reset the cache pointers
        m_last = nullptr;
        m_data = nullptr;
      }

    private:
      // data

      /// The last object added to the count
      TYPE* m_last = nullptr;

      /// Pointer to the data count for the last object added
      Data* m_data = nullptr;
    };

  private:
    // data

    /// Pointers to RICHes
    DetectorArray<const DeRich*> m_riches = {{}};

    /// Cached value storing product of quartz window eff. and digitisation pedestal loss
    float m_qEffPedLoss{0};

    /// Thickness of windows in each RICH
    DetectorArray<float> m_qWinZSize = {{}};

  private:
    // messaging

    /// No PDs found
    mutable WarningCounter m_warnNoPDs{this, "No PDs found -> Assuming Av. PD Q.E. of 1"};

    /// No primary mirrors found
    mutable WarningCounter m_warnNoPrimMirrs{this, "No primary mirrors found -> Assuming Av. reflectivity of 1"};

    /// No secondary mirrors found
    mutable WarningCounter m_warnNoSecMirrs{this, "No secondary mirrors found -> Assuming Av. reflectivity of 1"};
  };

} // namespace Rich::Future::Rec
