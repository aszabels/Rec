/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "RichCreateScalarPhotons.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Future::Rec;

//=============================================================================

CreateScalarPhotons::CreateScalarPhotons( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator,
                   // input
                   KeyValue{"InputPhotonsLocation", SIMDCherenkovPhotonLocation::Default},
                   // output
                   KeyValue{"OutputPhotonsLocation", CherenkovPhotonLocation::Default} ) {}

OutData CreateScalarPhotons::operator()( const SIMDCherenkovPhoton::Vector& simdPhotons ) const {

  // Create output contain and reserve size based on # and SIMD vector size
  OutData scalarPhotons;
  scalarPhotons.reserve( simdPhotons.size() * SIMDFP::Size );

  // loop over SIMD photons and for scalars for valid entries
  for ( const auto& phot : simdPhotons ) {
    // loop of scalar entries
    for ( std::size_t i = 0; i < SIMDFP::Size; ++i ) {
      if ( phot.validityMask()[i] ) { scalarPhotons.emplace_back( phot.scalarPhoton( i ) ); }
    }
  }

  // return photons
  return scalarPhotons;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( CreateScalarPhotons )

//=============================================================================
