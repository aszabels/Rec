/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef _ITsaSeedAddHits_H
#define _ITsaSeedAddHits_H

#include "GaudiKernel/IAlgTool.h"

#include "TsaKernel/SeedHit.h"
#include "TsaKernel/SeedTrack.h"

static const InterfaceID IID_ITsaSeedAddHits( "ITsaSeedAddHits", 0, 0 );

#include <vector>

namespace Tf {
  namespace Tsa {

    /** @class ITsaSeedAddHits ITsaSeedAddHits.h
     *
     *  Interface to Tsa Seed Hit Adder (!!)
     *
     *  @author M.Needham
     *  @date   12/11/2006
     */

    class ITsaSeedAddHits : virtual public IAlgTool {
    public:
      /// Retrieve interface ID
      static const InterfaceID& interfaceID() { return IID_ITsaSeedAddHits; }

      virtual StatusCode execute( SeedTracks* seeds, SeedHits* hits ) = 0;
    };

  } // namespace Tsa
} // namespace Tf

#endif
