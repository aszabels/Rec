/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PATTTSTATIONHITMANAGER_H
#define PATTTSTATIONHITMANAGER_H 1
// Include files
#include "PatKernel/PatTTHit.h"
#include "TfKernel/TTStationHitManager.h"

/** @class PatTTStationHitManager PatTTStationHitManager.h
 *
 *  TT station hit manager for Pat.
 *
 *  @author S. Hansmann-Menzemer, W. Hulsbergen, C. Jones, K. Rinnert
 *  @date   2007-06-01 Initial version
 */

struct PatTTStationHitManager : Tf::TTStationHitManager<PatTTHit> {
  using Tf::TTStationHitManager<PatTTHit>::TTStationHitManager;
};

#endif // PATTSTATIONHITMANAGER_H
